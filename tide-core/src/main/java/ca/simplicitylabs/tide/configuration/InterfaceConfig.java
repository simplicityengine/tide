package ca.simplicitylabs.tide.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

import ca.simplicitylabs.tide.App;
import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.api.APIHandler;
import ca.simplicitylabs.tide.enumeration.ProtocolType;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.NotFound;
import ca.simplicitylabs.tide.orchestration.APIFlow;
import jauter.MethodlessRouter;

@Reference("interfaces")
public class InterfaceConfig extends Config {
	private ProtocolType protocol;
	private String host;
	private Integer port;
	private SSLEntry ssl;
	private transient MethodlessRouter<Class<? extends APIFlow>> router = new MethodlessRouter<Class<? extends APIFlow>>();
	private transient APIHandler handler = new APIHandler(router);
	private List<FlowEntry> flows = new ArrayList<FlowEntry>();
	
	public InterfaceConfig() {
		super();
		try {
			Manifest manifest = AppUtils.manifest(InterfaceConfig.class);
			groupId = manifest.getMainAttributes().getValue("groupId");
			artifactId = manifest.getMainAttributes().getValue("artifactId");
			version = manifest.getMainAttributes().getValue("version");
			
		} catch (APIError e) {
			error(e);
			groupId = "ca.simplicitylabs.tide";
			artifactId = "tide-core";
			version = "1.0.0";
		}
	}
	
	public InterfaceConfig(String host, int port, ProtocolType protocol, SSLEntry ssl) {
		this();
		this.host = host;
		this.port = port;
		this.protocol = protocol;
		this.ssl = ssl;
	}
	
	@Override
	public boolean equals(Config config){
		try{
			InterfaceConfig api = (InterfaceConfig) config;
			
			if(api.host() != null && api.port() != null){
				/*if host does not match*/
				if(!host.equalsIgnoreCase( api.host()))
					return super.equals(config);
				
				/*if port does not match*/
				if(!port.equals(api.port()))
					return super.equals(config);
				
				/*if both port and host match, then consider this the same config*/
				return true;
			}
			else
				return super.equals(config);
		}
		catch(Exception ex){
			return super.equals(config);
		}
	}
	
	@Override
	public boolean equals(Manifest manifest){
		for(FlowEntry flow : flows){
			if(flow.equals(manifest))
				return true;
		}
		return false;
	}
	
	public ProtocolType protocol() {
		return protocol;
	}

	public String host() {
		return host;
	}


	public Integer port() {
		return port;
	}
	
	public MethodlessRouter<Class<? extends APIFlow>> router(){
		return router;
	}
	
	public APIHandler handler(){
		return handler;
	}
	
	public SSLEntry ssl(){
		return ssl;
	}
	
	public void add(FlowEntry flow){
		flows.add(flow);
	}
	
	public FlowEntry flow(Manifest manifest) throws APIError{
		for(FlowEntry flow : flows){
			if(flow.equals(manifest))
				return flow;
		}
		
		
		throw new NotFound("Flow Entry Not Found!");
	}
	

	@Override
	public void added(){	
		try {
			App.add(this);
		} catch (APIError e) {
			error(e);
		}
	}
	
	@Override
	public void dropped(){
		try {
			App.drop(this);
		} catch (APIError e) {
			error(e);
		}
	}

	@Override
	public boolean valid(){
		boolean result = super.valid();
		if(!result)
			return false;
		
		if(protocol == null)
			return false;
		
		if(host == null)
			return false;
		
		if(port == null)
			return false;
		
		return true;
		
	}
	
	
}
