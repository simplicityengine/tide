package ca.simplicitylabs.tide.configuration;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64;

import ca.simplicitylabs.tide.api.LooseTrustManager;
import ca.simplicitylabs.tide.enumeration.SerialFormat;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.serialization.Serial;

public class SSLEntry extends SelfLogger implements Serial {
	private static final String CERT_BEGIN = "-----BEGIN CERTIFICATE-----";
	private static final String CERT_END = "-----END CERTIFICATE-----";
	@SuppressWarnings("unused")
	private static final String KEY_BEGIN = "-----BEGIN PRIVATE KEY-----";
	@SuppressWarnings("unused")
	private static final String KEY_END = "-----END PRIVATE KEY-----";
	
	private transient SSLContext context;

	private String certificatePath;
	
	private String keyPath;
	
	public SSLEntry(){
		super();
	}
	
	public SSLEntry(String certificatePath, String keyPath){
		this();
		this.certificatePath = certificatePath;
		this.keyPath = keyPath;
	}
	public void setContext(SSLContext context){
		this.context = context;
	}
	public SSLContext getContext() {
		return context;
	}

	@Override
	public <T> void serialized(SerialFormat type, T raw) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * @author Joseph Eniojukan
	 * @param type indicates the serialization algorithm to be used (example: <b>JSON</b>,
	 * <b>XML</b>, <b>SQL</b>)
	 * @see <a href="https://stackoverflow.com/questions/12501117/programmatically-obtain-keystore-from-pem">
	 * Stack Overflow: Programmatically Obtain Keystore from PEM</a>
	 */
	@Override
	public <T> void deserialized(SerialFormat type, T raw) {
		try{
			String password = "changeit";
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			setContext(SSLContext.getInstance("TLS"));
			KeyStore keystore = KeyStore.getInstance("JKS");
			keystore.load(null);
			Map<String, byte[]> pems = new HashMap<String, byte[]>();
			
			
			List<byte[]> certificates = listCerts();
			
			
			for(int i = 0; i < certificates.size(); i++){
				byte[] cert = certificates.get(i);
				String alias = String.format("intermediate-%s", i);
				
				if(i == certificates.size() - 1)
					alias = "root";
				
				else if( i == 0 )
					alias = "issuer";
				
				pems.put(alias, cert);
			}
			
			byte[] key = getKey();
			
			pems.put("private", key);
			
			Set<String> aliases = pems.keySet();
			List<Certificate> chain = new ArrayList<Certificate>();
			for(String alias : aliases){
				byte[] pem = pems.get(alias);
				ByteArrayInputStream bstream = new ByteArrayInputStream(pem);
				if(alias.equalsIgnoreCase("private")){
					byte[] decoded = Base64.decode(pem);
					PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
					KeyFactory factory = KeyFactory.getInstance("RSA");
					Key privKey = factory.generatePrivate(spec);
					keystore.setKeyEntry(alias, privKey, password.toCharArray(), chain.toArray(new Certificate[chain.size()]));
				}
				
				else{
					Certificate cert = cf.generateCertificate(bstream);
					keystore.setCertificateEntry(alias, cert);
					chain.add(cert);
				}
			}
			kmf.init(keystore, password.toCharArray());
			
			context.init(kmf.getKeyManagers(), new TrustManager[] { new LooseTrustManager() }, null);
		}
		catch(Exception e){
			error(e);
		}
	}
	
	private List<byte[]> listCerts() throws FileNotFoundException, IOException{
		List<byte[]> certificates = new ArrayList<byte[]>();
		String pems = IOUtils.toString(new FileInputStream(certificatePath), "UTF-8");
		String [] tokens = pems.split(CERT_END);
		
		for(String token : tokens){
			if(token.startsWith(CERT_BEGIN)){
				token = String.format("%s%s\n", token, CERT_END);
				byte[] certificate = token.getBytes();
				certificates.add(certificate);
			}
		}
		
		return certificates;
	}
	
	private byte[] getKey() throws FileNotFoundException, IOException, NoSuchAlgorithmException{
		String content = IOUtils.toString(new FileInputStream(keyPath), "UTF-8");
		content = content.replace(KEY_BEGIN, "");
		content = content.replace(KEY_END, "");
		return content.getBytes();
	}
}
