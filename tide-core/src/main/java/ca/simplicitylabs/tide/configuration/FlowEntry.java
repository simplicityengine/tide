package ca.simplicitylabs.tide.configuration;

import java.util.jar.Manifest;

import ca.simplicitylabs.tide.api.POM;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class FlowEntry extends SelfLogger implements POM {
	private String groupId;
	private String artifactId;
	private String version = "";
	private String domain = "";
	private String prefix = "";
	
	public FlowEntry(){
		
	}
	
	public FlowEntry(String groupId, String artifactId, String version){
		this();
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
	}
	
	public FlowEntry(Manifest manifest){
		this();
		this.groupId = manifest.getMainAttributes().getValue("groupId");
		this.artifactId = manifest.getMainAttributes().getValue("artifactId");
		this.version = manifest.getMainAttributes().getValue("version");
		this.domain = manifest.getMainAttributes().getValue("domain");
	}
	
	@Override
	public String groupId() {
		return groupId;
	}

	@Override
	public String artifactId() {
		return artifactId;
	}

	@Override
	public String version() {
		return version;
	}

	@Override
	public String domain() {
		return domain;
	}
	
	public String prefix(){
		String result = prefix;
		result = result.replace("{artifactId}", artifactId);
		result = result.replace("{version}", version);
		result = result.replace("{domain}", domain);
		return result;
	}
	
	public boolean equals(Manifest manifest){
		try{
			String groupId = manifest.getMainAttributes().getValue("groupId");
			String artifactId = manifest.getMainAttributes().getValue("artifactId");
			String version = manifest.getMainAttributes().getValue("version");
			
			if(!groupId.equalsIgnoreCase(groupId()))
				return false;
			
			if(!artifactId.equalsIgnoreCase(artifactId()))
				return false;
			
			if(ok(version())){
				return version().equalsIgnoreCase(version);
			}
			
			return true;
		}
		catch(Exception ex){
			return false;
		}
	}
	
	private boolean ok(String value){
		if(value == null)
			return false;
		
		return !value.trim().isEmpty();
	}
}