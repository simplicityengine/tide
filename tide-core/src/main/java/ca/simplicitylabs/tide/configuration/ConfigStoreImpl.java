package ca.simplicitylabs.tide.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ca.simplicitylabs.tide.App;
import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.Conflict;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.exception.NotFound;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.serialization.GsonSerializer;

public class ConfigStoreImpl extends TimerTask implements ConfigStore{
	private static final String REGEX_CONFIG_NAMES = "interfaces|databases|configurations";
	private static GsonBuilder builder = new GsonBuilder().setPrettyPrinting().serializeNulls();
	private static String path;
	private static JsonObject json = new JsonObject();
	private static SelfLogger log = new SelfLogger(ConfigStore.class);
	private static Map<String, JsonArray> configurations = new HashMap<String, JsonArray>();
	private static List<Class<? extends Config>> types = new ArrayList<Class<? extends Config>>();
	private static AtomicBoolean lock = new AtomicBoolean(false);
	private static Timer timer = new Timer();
	
	public ConfigStoreImpl() throws APIError  {
		if(path == null){
			Properties props = App.properties();
			path = props.getProperty("configstore.path", "/var/lib/tide/store/config.json");
			read();
			
			// TODO: make configurable
			timer.schedule(this, 60000, 60000);
		}
	}
	
	@Override
	public void run() {
		try {
			save();
		} catch (APIError ex) {
			log.error(ex);
			log.error("cancelling timer!!!");
			timer.cancel();
			timer.purge();
		}
	}
	
	@Override
	public GsonBuilder builder(){
		return builder;
	}
	
	public Gson gson(){
		return builder.create();
	}
	
	@Override
	public List <Class<? extends Config>>  types(){
		List <Class<? extends Config>> list = new ArrayList<Class<? extends Config>>();
		synchronized(types){
			for(Class<? extends Config> type : types){
				list.add(type);
			}
		}
		return list;
	}
	
	@Override
	public String getPath() {
		return path;
	}
	
	@Override
	public void setPath(String path) throws APIError {
		ConfigStoreImpl.path = path;
		read();
	}
	
	@Override
	public void register(Class<? extends Config> type) throws APIError {
		try{
			synchronized(types){
				if(!types.contains(type)){
					types.add(type);
					GsonSerializer<Config> adapter = new GsonSerializer<Config>(Config.class);
					builder.registerTypeAdapter(type, adapter);
					Config.registerTypeAdapter(type, adapter);
				}
			}			
			
			String name = reference(type);
			
			synchronized(configurations){
				Gson gson = gson();
				if(configurations.containsKey(name)){
					JsonArray array = configurations.get(name);
					for(JsonElement elem : array){
						Config config = gson.fromJson(elem, type);
						if(config.valid())
							config.added();
					}
				}
			}
		}
		catch(Exception ex){
			log.error(ex);
			throw new Internal(ex);
		}
	}

	@Override
	public void unregister(Class<? extends Config> cls) throws APIError {
	}

	@Override
	public void add(Config config) throws APIError {
		String name = reference(config);
		if(!has(config)){
			synchronized(configurations){
				
				JsonArray list = configurations.get(name);
				
				if(list == null){
					list = new JsonArray();
					configurations.put(name, list);
				}
				config.generateUUID();
				list.add(gson().toJsonTree(config));
				config.added();
				save();
			}
			
		}
		else
			throw new Conflict("Configuration %s already exists!", config.name());
		
	}

	@Override
	public void drop(Config config) throws APIError {
		
		String name = reference(config);
		if(has(config)){
			synchronized(configurations){
				Gson gson = gson();
				JsonArray array = configurations.get(name);
				if(array != null){
					int size = array.size();
					for(int i = size - 1; i >= 0; i--){
						JsonElement elem = array.get(i);
						Config item = gson.fromJson(elem, config.getClass());
						if(item.equals(config)){
							array.remove(i);
							item.dropped();
							save();
							return;
						}
					}
				}
			}
		}
		throw new NotFound();
	}
	@Override
	public void replace(Config config) throws APIError{
		if(has(config)){
			String name = reference(config.getClass());
			synchronized(configurations){
				JsonArray array = configurations.get(name);
				Gson gson = gson();
				for(JsonElement elem : array){
					Config item = gson.fromJson(elem, config.getClass());
					if(item.equals(config)){
						overwrite(config, item, true);
						break;
					}
				}
			}
			
		}
		else
			throw new NotFound();
	}
	@Override
	public void modify(Config config) throws APIError{
		if(has(config)){
			String name = reference(config.getClass());

			synchronized(configurations){
				JsonArray array = configurations.get(name);
				Gson gson = gson();
				for(JsonElement elem : array){
					Config item = gson.fromJson(elem, config.getClass());
					if(item.equals(config)){
						overwrite(config, item, false);
						save();
						break;
					}
				}
			}
			
		}
		else
			throw new NotFound();
	}
	
	private void overwrite(Config reference, Config target, boolean nullify) throws APIError{
		List<Field> fields = AppUtils.fields(target.getClass(), Config.class);
		for(Field field : fields){
			try{
				String name = field.getName();
				Object value = field.get(reference);
				Class<?> type = field.getType();
				
				if(name.equalsIgnoreCase("uuid"))
					continue;
				
				if(!nullify && value == null)
					continue;
				
				if(AppUtils.isPrimitive(type) || AppUtils.isIterable(type)){
					log.info("name: %s", name);
					field.set(target, value);
				}
				
				else{
					Object from = value;
					Object to = field.get(target);
					AppUtils.copy(from, to, nullify);
				}
				
			}
			catch(Exception ex){
				
			}
		}
	}
	
	@Override
	public boolean has(Config config) throws APIError {
		String name = reference(config.getClass());
		
		synchronized(configurations){
			if(configurations.containsKey(name)){
				Gson gson = gson();
				JsonArray array = configurations.get(name);
				for(JsonElement elem : array){
					Config item = gson.fromJson(elem, config.getClass());
					if(item.equals(config))
						return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public <T extends Config> List<T> list(Manifest manifest, Class<T> type) throws APIError {
		String name = reference(type);
		List<T> list = new ArrayList<T>();
		if(configurations.containsKey(name)){
			JsonArray array = configurations.get(name);
			synchronized(array){
				Gson gson = gson();
				for(JsonElement elem : array){
					Config config = gson.fromJson(elem, type);
					
					if(config.equals(manifest)){
						list.add((T)config);
					}
				}
			}
		}
		
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Config> List<T> list(Class<T> type) throws APIError{
		String name = reference(type);
		List<T> list = new ArrayList<T>();
		if(configurations.containsKey(name)){
			JsonArray array = configurations.get(name);
			synchronized(array){
				Gson gson = gson();
				for(JsonElement elem : array){
					Config config = gson.fromJson(elem, type);
					list.add((T)config);
				}
			}
		}
		return list;
	}
	
	/**
	 * This method reads the file containing the persisted information.
	 * It is only called once by the App during startup.
	 * @throws APIError
	 * @author Joseph Eniojukan
	 */
	private static void read() throws APIError{
		
		try {
			File file = new File(path);
			if(file.exists()){
				InputStream stream = new FileInputStream(file);
				String content = IOUtils.toString(stream, "UTF-8");
				Gson gson = builder.create();
				JsonElement element = gson.fromJson(content, JsonElement.class);
				
				if(element.isJsonObject())
					json = (JsonObject) element;
	
			}
			else{
				json = new JsonObject();
			}
			
			Set<String> keys = json.keySet();
			for(String key : keys){
				JsonElement elem = json.get(key);
				if(elem.isJsonArray()){
					configurations.put(key, elem.getAsJsonArray());
				}
			}
			
		}
		
		catch(UnsupportedCharsetException e){
			log.error(e);
			throw new Internal("Data Store had Character Sets that are not Supported!");
		}
		
		catch(UnsupportedEncodingException e){
			log.error(e);
			throw new Internal("UFT-8 Encoding not support on System!");
		}
		
		catch (FileNotFoundException e) {
			log.error(e);
			throw new Internal("File on Data Store Path was not Found!");
		} 
		
		catch (IOException e) {
			log.error(e);
			throw new Internal("I/O error on Data Store!");
		}
		
	}
	
	private static void save() throws APIError{
		try {
			if(!lock.get()){
				lock.set(true);
				File file = new File(path);
				Gson gson = builder.create();
				
				synchronized(configurations){
					String content = gson.toJson(configurations);
					FileUtils.write(file, content, "UTF-8");
					log.debug("contents saved to file");
				}
			}
			
			
		}
		
		catch(NullPointerException  e){
			log.error(e);
			throw new Internal("Data Store had Character Sets that are not Supported!");
		} 
		
		catch (IOException e) {
			log.error(e);
			throw new Internal("Data Store had Character Sets that are not Supported!");
		}
		finally{
			lock.set(false);
		}
	}
	
	private static String reference(Config config) throws APIError{
		return reference(config.getClass());
	}
	private static String reference(Class<? extends Config> type) throws APIError{
		try{
			String name = "configurations";
			if(type.isAnnotationPresent(Reference.class)){
				Reference data = type.getAnnotation(Reference.class);
				name = data.value();
			}
			
			// TODO: make configurable
			if(!name.matches(REGEX_CONFIG_NAMES))
				name = "configurations";
			
			return name;
		}
		catch(Exception ex){
			log.error(ex);
			throw new Internal(ex);
		}
	}
	

}
