package ca.simplicitylabs.tide.lcm;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import ca.simplicitylabs.tide.App;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class LifeCycleManager extends TimerTask {
	private LifeCycleLoader loader;
	private static SelfLogger logger = new SelfLogger(LifeCycleManager.class);
	private Timer timer = new Timer();
	public LifeCycleManager(){
		
	}
	
	public LifeCycleManager(LifeCycleLoader loader){
		this.loader = loader;
		//TODO: make configurable
		timer.schedule(this, 1000, 10000);
	}
	
	@Override
	public void run() {
		scan();
	}
	
	private void scan(){
		// TODO: make configurable
		logger.debug("starting...");
		File folder = new File("/var/lib/tide/deploy");
		File[] files = folder.listFiles();
		boolean processed = false;
		for(File file : files){
			try {
				String name = file.getName();
				if(name.endsWith(".jar")){
					logger.debug("found jar: %s", name);
					JarMarker marker = new JarMarker(file, loader.getParent());
					if(loader.process(marker))
						processed = true;
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		
		try{
			if(processed)
				App.reloadInterfaces();
			}
		catch(Exception ex){
			logger.error(ex);
		}
			
	}

}
