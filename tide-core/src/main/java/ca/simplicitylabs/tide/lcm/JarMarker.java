package ca.simplicitylabs.tide.lcm;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import ca.simplicitylabs.tide.App;
import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.API;
import ca.simplicitylabs.tide.api.POM;
import ca.simplicitylabs.tide.configuration.ConfigStore;
import ca.simplicitylabs.tide.configuration.FlowEntry;
import ca.simplicitylabs.tide.configuration.InterfaceConfig;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.orchestration.APIFlow;
import io.undertow.util.PathTemplate;
import jauter.MethodlessRouter;
import jauter.Routed;

public class JarMarker extends SelfLogger implements POM, Comparable<JarMarker>{
	private Manifest manifest;
	private JarFile  jar;
	private URL url;
	private long timestamp;
	private long chksum;
	private boolean active = false;
	private List<InterfaceConfig> configurations = new ArrayList<InterfaceConfig>();
	private URLClassLoader loader;
	private ClassLoader bootloader;

	private List<String> paths = new ArrayList<String>();
	private List<String> classes = new ArrayList<String>();
	private static ConfigStore store;
	
	public JarMarker(){
		
	}
	
	public JarMarker(File file, ClassLoader bootloader) throws Exception{
		
		try{
			trace("starting....");
			url = new URL(String.format("jar:file://%s!/", file.getCanonicalPath()));
			loader = new URLClassLoader(new URL[]{url}, bootloader);
			trace("url: %s", url);
			jar = new JarFile(file);
			trace("jar loaded");
			manifest = jar.getManifest();
			trace("manifest retrieved");
			this.bootloader = bootloader;			
			trace("class loader setup");
			setTimestamp(file.lastModified());
			trace("current timestamp: %s", timestamp);
			this.setChksum(AppUtils.checksum(file));
			trace("current checksum: %s", chksum);
			trace("finished");
		}
		catch( Exception ex){
			
			error(ex);
			throw ex;
		}
		finally{
			
		}
	}
	
	public static void use(ConfigStore store){
		JarMarker.store = store;
	}
	
	public boolean allowed(Class<?> cls){
		return allowed(cls.getCanonicalName());
	}
	public boolean allowed(String classPath){
		return classes.contains(classPath);
	}
	
	private void loadInterfaces() throws APIError{
		List<InterfaceConfig> configs = App.interfaces();
		configurations.clear();
		for(InterfaceConfig config : configs){
			configurations.add(config);
		}
	}
	
	private void loadFlows() throws APIError {
		try{
		Enumeration<?> entries = jar.entries();

		while(entries.hasMoreElements()){
			JarEntry entry = (JarEntry) entries.nextElement();
			String name = entry.getName();
			if(name.endsWith(".class")){
				trace("Adding class: %s", name);
				
				// block any class attempting to insert in the tide package name
				if(name.toLowerCase().startsWith("ca.simplicitylabs.tide")){
					error("The following jar is attempting to hijack the tide namespace:\n%s", url);
					continue;
				}
				
				Class<?> cls = loadClass(name);
				classes.add(cls.getCanonicalName());
				if(cls.isAnnotationPresent(API.class) && APIFlow.class.isAssignableFrom(cls)){
					trace("registering api: %s", cls.getSimpleName());
	    			String[] chain = cls.getAnnotation(API.class).value();
	    			synchronized(configurations){
	    				for(InterfaceConfig config : configurations){
	    					try{
		    					for(String link : chain){
		    						MethodlessRouter<Class<? extends APIFlow>> router = config.router();
		    						FlowEntry flowEntry = config.flow(manifest);
		    						String prefix = flowEntry.prefix();
		    						String path = String.format("/%s/%s", prefix, link);
		    						URI uri = AppUtils.normalize(new URI(path));
		    						trace("uri: %s", uri);
				    				router.pattern(uri.toString(), (Class<? extends APIFlow>) cls);
				    				paths.add(uri.toString());
				    			}
	    					}
	    					catch(Exception ex){
	    						warn(ex.toString());
	    					}
	    				}
		    			
	    			}	
	    		}
				
			}
		}
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public void init() {
		for (String path : paths) {
			for (InterfaceConfig config : configurations) {
				try {
					MethodlessRouter<Class<? extends APIFlow>> router = config.router();
					Routed<Class<? extends APIFlow>> route = router.route(path);
					Class<? extends APIFlow> target = route.target();
					AppUtils.activate(target);
				} catch (Exception ex) {
					error(ex);
				}
			}
		}
	}
	
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		String className = name.replaceAll("\\.class$", "").replace('/', '.');
		try{
			//Class<?> cls = loader.loadClass(className);
			Class<?> cls = AppUtils.implementation(Class.forName(className, true, loader), true, bootloader);
			trace("loaded: %s", className);
			return cls;
		}
		catch(Exception ex){
			return bootloader.loadClass(className);
		}
	}
	
	public Enumeration<URL> getResources(String name) throws IOException{
		return new LifeCycleEnumerator<URL>(url);
	}
	
	public Manifest getManifest() {
		return manifest;
	}
	public void setManifest(Manifest manifest) {
		this.manifest = manifest;
	}

	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getChksum() {
		return chksum;
	}
	public void setChksum(long chksum) {
		this.chksum = chksum;
	}
	private String attribute(String key){
		try{
			String value = manifest.getMainAttributes().getValue(key);
			return value;
		}
		catch(Exception e){
			warn(e);
		}
		return null;
	}
	
	@Override
	public String groupId(){
		return attribute("groupId");
	}
	
	@Override
	public String artifactId(){
		return attribute("artifactId");
	}
	
	@Override
	public String version(){
		return attribute("version");
	}
	
	@Override
	public String domain(){
		return attribute("domain");
	}
	
	public String key(){
		return String.format("%s.%s", groupId(), artifactId());
	}
	
	public boolean isActive(){
		return active;
	}
	
	public void activate() throws IOException, ClassNotFoundException, APIError{
		active = true;
		loadInterfaces();
		loadFlows();
		init();
	}
	
	public void deactivate(){
		active = false;
		synchronized(configurations){
			for(InterfaceConfig config : configurations){
				MethodlessRouter<Class<? extends APIFlow>> router = config.router();
				for(String path : paths){
					Routed<Class<? extends APIFlow>> route = router.route(path);
					router.removePath(path);
					
					try {
						Class<? extends APIFlow> target = route.target();
						AppUtils.deactivate(target);
					} catch (APIError e) {
						error(e);
					}
				}
			}
			
		}
	}
	
	public boolean isModified(JarMarker marker){
		if(equals(marker))
			if(marker.getChksum() != getChksum())
				if(marker.getTimestamp() > getTimestamp())
					return true;
		return false;
	}
	
	public boolean equals(JarMarker marker){
		return compareTo(marker) == 0;
	}
	
	@Override
	public int compareTo(JarMarker marker){
		return compareTo(marker.key());
	}
	public int compareTo(String string){
		return key().compareTo(string);
	}
	
	@Override
	public String toString(){
		return String.format("Marker [name: %s, version: %s, checksum: %s, timestamp: %s]", artifactId(), version(), getChksum(), timestamp);
	}
}
