package ca.simplicitylabs.tide.lcm;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.orchestration.APIFlow;
import jauter.MethodlessRouter;

public class LifeCycleLoader extends ClassLoader {
	private List<JarMarker> markers = new ArrayList<JarMarker>();
	private static SelfLogger log = new SelfLogger(LifeCycleLoader.class);
	private ClassLoader bootloader;
	public LifeCycleLoader(ClassLoader parent){
		super(parent);
		bootloader = parent;
	}
	
	@Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class <?> cls = null;
		for(JarMarker marker : markers){
			try{
				cls = marker.loadClass(name);
				break;
			}
			catch(ClassNotFoundException ex){
			}
			
		}
		if(cls == null)
			cls = bootloader.loadClass(name);
		
		log.debug(name);
        return AppUtils.implementation(cls, true, bootloader);
    }
	
	
	public boolean process(JarMarker reference) throws ClassNotFoundException, IOException, APIError{
		log.trace("starting...");
		boolean found = false;
		boolean updated = false;
		
		synchronized(markers){
			int size = markers.size();
			for( int i = size - 1; i >= 0; i-- ){
				JarMarker marker = markers.get(i);
				log.trace("checking %s", marker);
				if(marker.equals(reference))
					found = true;
				
				if(marker.isModified(reference)){
					log.info("change detected:\nold %s\nnew %s", marker, reference);
					marker.deactivate();
					markers.remove(i);
					markers.add(i, reference);
					updated = true;
					break;
				}
			}
			if(!found){
				log.info("adding %s", reference);
				markers.add(reference);
				updated = true;
			}
		}
		
		if(updated){
			reference.activate();
		}
		
		return updated;
		
	}
	
	public Enumeration<URL> getResources(String name) throws IOException{
		// TODO: sandbox other JarMarkers
		synchronized(markers){
			log.trace("checking %d markers", markers.size());
			for(JarMarker marker : markers){
				Enumeration<URL> urls = marker.getResources(name);
				log.trace("%s has elements: %s", name, urls.hasMoreElements());
				if(urls.hasMoreElements())
					return urls;
			}
		}
		log.info("name: %s", name);
		Enumeration<URL> urls = bootloader.getResources(name);
		return urls;
			
	}
 
}
