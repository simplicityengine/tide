package ca.simplicitylabs.tide.lcm;
import java.util.Enumeration;

public class LifeCycleEnumerator<E> implements Enumeration<E> {
	private E entry;
	private boolean unlisted = true;
	public LifeCycleEnumerator() {
		// TODO Auto-generated constructor stub
	}
	
	public LifeCycleEnumerator(E entry) {
		this.entry = entry;
	}

	@Override
	public boolean hasMoreElements() {
		return unlisted;
	}

	@Override
	public E nextElement() {
		if(unlisted){
			unlisted = false;
			return entry;
		}
		return null;
	}

}
