package ca.simplicitylabs.tide.api;

import java.net.InetSocketAddress;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.NotFound;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.orchestration.APIFlow;
import io.undertow.io.Sender;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import jauter.MethodlessRouter;
import jauter.Routed;
public class APIHandler extends SelfLogger implements HttpHandler {
	protected MethodlessRouter<Class<?  extends APIFlow> > router;
	public APIHandler(MethodlessRouter<Class<?  extends APIFlow> > router){
		this.router = router;
	}
	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		APIFlow flow = null;
		try{
			String path = exchange.getRequestPath();
			HttpString method = exchange.getRequestMethod();
			InetSocketAddress from = exchange.getSourceAddress();
			InetSocketAddress to = exchange.getDestinationAddress();
			
			debug("%s %s [from: %s:%d, to: %s:%d]", method, path, from.getAddress().getHostAddress(), from.getPort(), to.getAddress().getHostAddress(), to.getPort());
			Routed<Class<? extends APIFlow>> routed = router.route(path);
			trace("route acquired: %s", routed);
			
			if(routed == null)
				throw new NotFound("No Service Defined on %s", path);
			
			trace("route found");
			flow = (APIFlow) routed.instanceFromTarget();
			routed.params();
			flow.handle(exchange, routed.params());
			close(flow);
			trace("completed");
		}
		catch(APIError error){
			handleError(error, exchange);
			close(flow);
		}
		catch(Exception ex){
			
			APIError aex = AppUtils.normalize(ex);
			error(aex);
			handleError(aex, exchange);
			close(flow);
		}
		
	}
	
	protected void handleError(APIError error, HttpServerExchange exchange){
		exchange.setStatusCode(error.getCode());
		Sender sender = exchange.getResponseSender();
		if(sender != null)
			sender.send(error.getMessage());
	}
	
	private void close(APIFlow flow){
		if(flow != null)
			flow.close();
	}

}
