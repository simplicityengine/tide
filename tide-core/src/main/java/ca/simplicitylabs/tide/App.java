package ca.simplicitylabs.tide;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLContextSpi;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.reflections.Reflections;

import ca.simplicitylabs.tide.annotation.API;
import ca.simplicitylabs.tide.api.APIHandler;
import ca.simplicitylabs.tide.api.LooseTrustManager;
import ca.simplicitylabs.tide.configuration.InterfaceConfig;
import ca.simplicitylabs.tide.configuration.SSLEntry;
import ca.simplicitylabs.tide.enumeration.ProtocolType;
import ca.simplicitylabs.tide.configuration.Config;
import ca.simplicitylabs.tide.configuration.ConfigStore;
import ca.simplicitylabs.tide.configuration.FlowEntry;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.Conflict;
import ca.simplicitylabs.tide.historization.FileLogger;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.lcm.JarMarker;
import ca.simplicitylabs.tide.lcm.LifeCycleLoader;
import ca.simplicitylabs.tide.lcm.LifeCycleManager;
import ca.simplicitylabs.tide.orchestration.APIFlow;
import ca.simplicitylabs.tide.orchestration.ConfigFlow;
import ca.simplicitylabs.tide.serialization.GsonSerializer;
import io.undertow.Undertow;
import io.undertow.Undertow.Builder;
import io.undertow.server.handlers.BlockingHandler;
import jauter.MethodlessRouter;
import jauter.NonorderedRouter;
import jauter.Pattern;

import static ca.simplicitylabs.tide.AppUtils.activeLoader;
import static ca.simplicitylabs.tide.AppUtils.activeThread;
import static ca.simplicitylabs.tide.enumeration.SerialFormat.JSON;
import static ca.simplicitylabs.tide.enumeration.ProtocolType.HTTP;
import static ca.simplicitylabs.tide.enumeration.ProtocolType.HTTPS;

@SuppressWarnings("unused")
public class App
{
	private static Undertow server;
	private static Properties properties = new Properties();
	private static SelfLogger log;
	private static ClassLoader original;
	private static String serverHost;
	private static Integer serverPort;
	private static Integer serverThreads = 64;
	private static String certificatePath;
	private static String keyPath;	
	private static String username;
	private static String password;
	private static String bearer;
	private static LifeCycleManager manager;
	
	private static ConfigStore store;
	private static List<InterfaceConfig> interfaces = new ArrayList<InterfaceConfig>();
	
    public static void main( String[] args ) 
    {
    	try{
    		setupLoggers();
    		
    		log.info("setting up security providers");
    		Security.addProvider(new BouncyCastleProvider());    		
    		
	    	log.info("setting up properties");
	    	setupProperties();
	    	
	    	log.info("setting up configuration store");
	    	store = AppUtils.instantiateWithErrors(ConfigStore.class);
	    	
	    	log.info("setting up class loader");
	    	LifeCycleLoader loader = new LifeCycleLoader(activeLoader());
	    	activeThread().setContextClassLoader(loader);
	    	
	    	
	    	log.info("setting up configuration store");
	    	store = AppUtils.instantiateWithErrors(ConfigStore.class);
	    	
	    	log.info("setting up admin interface");
	    	setupAdminInterface();
	    	
	    	log.info("registering configuration types");
	    	registerConfigTypes();
	    	
	    	log.info("setting up interfaces");
	    	reloadInterfaces();
	    	
	    	JarMarker.use(store);
	    	manager = new LifeCycleManager(loader);
	    	
    	}
    	catch(Exception ex){
    		log.error(ex);
    	}
    }
    
    private static void setupProperties() throws FileNotFoundException, IOException, APIError{
    	// TODO: make configurable
    	String configPath = System.getProperty("config.properties");
    	
    	// TODO: lock system property using Java Security Manager
    	properties.load(new FileInputStream(configPath));
    	setServerHost(properties.getProperty("server.host"));
    	setServerPort(properties.getProperty("server.port"));
    	setServerThreads(properties.getProperty("server.threads"));
    	setCertificatePath(properties.getProperty("certificate.path"));
    	setKeyPath(properties.getProperty("key.path"));
    	setUsername(properties.getProperty("admin.username"));
    	setPassword(properties.getProperty("admin.password"));
    	setBearer(properties.getProperty("admin.bearer"));
    }
    
    @SuppressWarnings("unchecked")
    private static void setupAdminInterface() throws APIError{
		ConfigFlow.use(store);		
    	
    	Manifest manifest = AppUtils.manifest(App.class);
    	FlowEntry flowEntry = new FlowEntry(manifest);
		SSLEntry sslEntry = new SSLEntry(certificatePath, keyPath);
		sslEntry.deserialized(JSON, null);
		InterfaceConfig config = new InterfaceConfig(serverHost, serverPort, HTTPS, sslEntry);
		config.add(flowEntry);
    	
		Reflections reflections = new Reflections("ca.simplicitylabs.tide.orchestration");
		Set<Class<?>> classess = reflections.getTypesAnnotatedWith(API.class);
		for(Class<?> cls : classess){
			if(APIFlow.class.isAssignableFrom(cls)){
				Class<? extends APIFlow> flowClass = (Class<? extends APIFlow>) cls;
				try{
					APIFlow flow = flowClass.newInstance();
					API api = flowClass.getAnnotation(API.class);
					String[] paths = api.value();
					
					for(String path : paths){
						MethodlessRouter<Class<? extends APIFlow>> router = config.router();
						router.pattern(path, flowClass);
					}
				}
				catch(Exception ex){
					log.error(ex);
				}
			}
		}
		
		add(config);
	}
    
    @SuppressWarnings("unchecked")
	private static void registerConfigTypes() throws APIError{
    	Reflections reflections = new Reflections("ca.simplicitylabs.tide.configuration");
    	Set<Class<? extends Config>> types = reflections.getSubTypesOf(Config.class);
    	
    	for(Class<? extends Config> type : types){
    		Config.registerTypeAdapter(type, new GsonSerializer<Config>((Class<Config>) type));
    		store.register(type);
    		log.info("registering: %s", type.getSimpleName());
    	}
    	
    }
    
    private static void setupLoggers(){
    	// TODO: use reflection
		SelfLogger.addType(FileLogger.class);
		log = new SelfLogger(App.class);
		log.info("setting up loggers");
    }

	private static void setKeyPath(String property) {
		keyPath = property;
		
	}

	private static void setCertificatePath(String property) {
		certificatePath= property;
	}

	private static void setServerPort(String property) {
		serverPort = Integer.valueOf(property);
	}
	
	private static void setServerThreads(String property){
		if(AppUtils.isInteger(property)){
			int threads = Integer.valueOf(property);
			threads = Math.max(threads, 32);
			threads = Math.min(threads, 2048);
			serverThreads = threads;
		}
	}

	private static void setServerHost(String property) {
		serverHost = property;
	}
	
	private static void setUsername(String value){
		username = scramble(value);
	}
	
	private static void setPassword(String value) throws APIError{
		value = scramble(value);
		String encrypted = AppUtils.encryptMD5(value);
		password = encrypted;
	}
	
	private static void setBearer(String value){
		bearer = scramble(value);
	}
	
	/**
	 * This method prevents un-initialized values of administrator username and passwords to be
	 * exploited as a security breach.
	 * @param value input administrator <b>username</b>, <b>password</b> or <b>bearer</b> value
	 * @return a random UUID string
	 */
	private static String scramble(String value){
		UUID uuid = UUID.randomUUID();
		String alternate = uuid.toString();
		
		if(value == null)
			return alternate;
		
		if(value.trim().isEmpty())
			return alternate;
		
		if(value.matches("\\$\\{[^\\{\\}]*\\}"))
			return alternate;
		
		return value;
	}
	public static String username(){
		// TODO: stack check security
		return username;
	}
	public static String password(){
		// TODO: stack check security
		return password;
	}
	public static String bearer(){
		// TODO: stack check security
		return bearer;
	}
	
	public static Properties properties(){
		return properties;
	}
	
	public static void add(InterfaceConfig config) throws APIError{
		synchronized(interfaces){
			for(InterfaceConfig inter : interfaces){
				if(inter.equals(config))
					throw new Conflict("Interface already Exists!");
			}
			interfaces.add(config);
		}
	}
	
	public static void drop(InterfaceConfig config) throws APIError{
		synchronized(interfaces){
			for(int i = interfaces.size() - 1; i >= 0; i--){
				InterfaceConfig inter = interfaces.get(i);
				if(inter.equals(config)){
					interfaces.remove(i);
				}
			}
		}
	}
	
	public static List<InterfaceConfig> interfaces(){
		List<InterfaceConfig> list = new ArrayList<InterfaceConfig>();
		synchronized(interfaces){
			for(int i = 1; i < interfaces.size(); i++){
				InterfaceConfig item = interfaces.get(i);
				list.add(item);
			}
		}
		
		
		return list;
	}
	
	public static synchronized void reloadInterfaces() throws APIError{
		try{
			if(server != null)
				server.stop();
			server = null;
			log.info("server stopped");
			// TODO: make configurable
			Builder builder = Undertow.builder();			
			builder.setIoThreads(serverThreads);
			
			synchronized(interfaces){
				for(InterfaceConfig inter : interfaces){
					String host = inter.host();
					int port = inter.port();
					ProtocolType protocol = inter.protocol();
					BlockingHandler handler = new BlockingHandler(inter.handler());
					MethodlessRouter<Class<? extends APIFlow>> router = inter.router();
					switch(protocol){
						case HTTPS:
							SSLContext context = inter.ssl().getContext();
							builder.addHttpsListener(port, host, context, handler);
							break;
						case HTTP:
							builder.addHttpListener(port, host, handler);
							break;
						default:
							break;
					}
				}
			}
			
			server = builder.build();
			server.start();
			log.info("server started");
		}
		
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
		
	}
}
