package ca.simplicitylabs.tide.encryption;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.historization.SelfLogger;
public class AESEncryptionImpl extends SelfLogger implements AESEncryption{
	private MessageDigest digest;
	private SecretKey key;
	private Cipher cipher;
	
	public AESEncryptionImpl() throws APIError {
		try {
			digest = MessageDigest.getInstance("SHA-1");
			cipher = Cipher.getInstance("AES");

		} catch (NoSuchAlgorithmException|NoSuchPaddingException e) {
			error(e);
			throw new Internal("Encryption Algorithm Missing from System");
		} 
	}
	
	@Override
	public void setSecret(String value) throws APIError{
		try {
			byte [] secret = value.getBytes("UTF-8");
			secret = digest.digest(secret);
			secret = Arrays.copyOf(secret, 16);
			key = new SecretKeySpec(secret, "AES");
		} catch (UnsupportedEncodingException e) {
			error(e);
			throw new Internal("String Encoding not Supported!");
		}
	}
	
	@Override
	public String encrypt(String value) throws APIError{
		try{
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] bytes = value.getBytes("UTF-8");
			byte[] encrypted = cipher.doFinal(bytes);
			
			return AppUtils.hexString(encrypted);
		}
		catch( Exception ex){
			error(ex);
			throw new Internal("Encryption key is Invalid");
		}
	}
	
	public String decrypt(String value) throws APIError{
		try{
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] bytes = value.getBytes("UTF-8");
			byte[] decrypted = cipher.doFinal(bytes);
			
			return new String(decrypted);
		}
		catch( Exception ex){
			error(ex);
			throw new Internal("Encryption key is Invalid");
		}
	}
}
