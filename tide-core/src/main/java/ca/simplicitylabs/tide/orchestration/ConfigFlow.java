package ca.simplicitylabs.tide.orchestration;
import java.util.List;

import com.google.gson.GsonBuilder;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.API;
import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.configuration.Config;
import ca.simplicitylabs.tide.configuration.ConfigStore;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.NotFound;


@API({"/configuration/:type"})
public class ConfigFlow extends CoreFlow {
	private static ConfigStore store;
	public ConfigFlow() {
		super();
	}
	
	
	@Override
	public void post() throws APIError{
		authenticate();
		Class<? extends Config> type = getType();
		Config config = payload(type);
		store.add(config);
		respond(200, config);
	}
	
	@Override
	public void get() throws APIError{
		authenticate();
		Class<? extends Config> type = getType();
		List<? extends Config> list = store.list(type);
		Config search = query(type, Config.class);

		if(search.hasUUID()){
			for(Config config : list){
				if(config.equals(search)){
					respond(200, config);
					return;
				}
			}
			throw new NotFound("unable to find configuration: %s", search.uuid());
		}
		respond(200, list, this.resourceString("type"));
	}
	
	@Override
	public void patch() throws APIError{
		authenticate();
		Class<? extends Config> type = getType();
		Config search = query(type, Config.class);
		Config config = payload(type);
		if(!search.hasUUID())
			throw new BadRequest("uuid is missing from the query parameters");
		
		config.uuid(search.uuid());
		store.modify(config);
		respond(200);
		return;
	}
	
	@Override
	public void delete() throws APIError{
		authenticate();
		Class<? extends Config> type = getType();
		Config search = query(type, Config.class);
		if(!search.hasUUID())
			throw new BadRequest("uuid is missing from the query parameters");
		
		store.drop(search);
		respond(204);
		return;
			
	}
	
	@Override
	public GsonBuilder builder(){
		return store.builder();
	}
	
	private Class<? extends Config> getType() throws APIError{
		try{
			String name = resourceString("type");
			List<Class<? extends Config>> types = store.types();
			for(Class<? extends Config> type : types){
				String reference = reference(type);
				if(reference != null){
					if(reference.equals(name))
						return type;
				}
			}
			throw new NotFound("There is no reference to the %s configuration", name);
		}
		catch(Exception ex){
			APIError aex = AppUtils.normalize(ex);
			error(aex);
			throw aex;
		}
	}
	
	private String reference(Class <? extends Config> cls){
		if(cls.isAnnotationPresent(Reference.class)){
			Reference ref = cls.getAnnotation(Reference.class);
			return ref.value();
		}
		return null;
	}
	
	public static void use(ConfigStore store){
		ConfigFlow.store = store;
	}

}
