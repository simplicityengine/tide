package ca.simplicitylabs.tide.orchestration;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import ca.simplicitylabs.tide.App;
import ca.simplicitylabs.tide.authentication.BasicAuth;
import ca.simplicitylabs.tide.configuration.Config;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.exception.UnAuthorized;
import ca.simplicitylabs.tide.serialization.GsonSerializer;
import ca.simplicitylabs.tide.serialization.Serial;

public abstract class CoreFlow extends APIFlow {
	private static GsonBuilder builder = new GsonBuilder()
			.registerTypeAdapter(Config.class, new GsonSerializer<Serial>(Serial.class));

	public CoreFlow() {
	}
	
	protected String username(){
		return App.username();
	}
	
	protected String password(){
		return App.password();
	}
	
	protected String bearer(){
		return App.bearer();
	}
	
	protected GsonBuilder builder(){
		return builder;
	}
	protected Gson gson(){
		return builder().create();
	}
	
	public void authenticate() throws APIError{
		BasicAuth cred = new BasicAuth(exchange);
		if(!ok(username()) || !ok(password()))
			throw new Internal("admin credentials not setup properly");
		
		if(!username().equals(cred.getUsername()))
			throw new UnAuthorized("Username is invalid");
		
		if(!password().equals(cred.getMd5Password()))
			throw new UnAuthorized("Password does not match");
	}
	
	public <T extends Config> T payload(Class<T> cls) throws APIError{
		try{
			Gson gson = gson();
			String content = payload();
			return (T) gson.fromJson(content, cls);
		}
		catch(JsonSyntaxException ex){
			throw new BadRequest(ex);
		}
		catch(Exception ex){
			throw new Internal(ex);
		}
	}
	
	public void respond(int code, Config config){
		header("Content-Type", "application/json");
		Gson gson = gson();
		String content = gson.toJson(config);
		respond(code, content);
	}
	
	public void respond(int code, List<? extends Config> configurations, String name){
		header("Content-Type", "application/json");
		Gson gson = gson();
		JsonArray array = (JsonArray) gson.toJsonTree(configurations);
		JsonObject response = new JsonObject();
		response.add(name, array);
		String content = gson.toJson(response);
		respond(code, content);
	}
	
	private boolean ok(String value){
		if(value == null)
			return false;
		
		return !value.trim().isEmpty();
	}
	
	@Override
	public void handleError(APIError error) throws APIError{
		handleJsonError(error);
	}

}
