package ca.simplicitylabs.tide;

import org.junit.Test;

import ca.simplicitylabs.tide.historization.SelfLogger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;

public class AppUtilsUnit extends SelfLogger{
	Map<String, String> nouns = new HashMap<String, String>();
	public AppUtilsUnit() {
		nouns.put("name", "names");
		nouns.put("aircraft", "aircraft");
		nouns.put("currency", "currencies");
	}
	
	@Test
	public void singular(){
		
		Set<String> singulars = nouns.keySet();
		
		for(String singular : singulars){
			String plural = nouns.get(singular);
			String test = AppUtils.singular(plural);
			print("plural: %s, singular: %s, test: %s", plural, singular, test);
			Assert.assertTrue(test.equals(singular));
		}
	}

}
