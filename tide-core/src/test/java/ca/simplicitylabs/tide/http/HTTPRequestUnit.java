package ca.simplicitylabs.tide.http;

import org.junit.Test;
import org.junit.Assert;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class HTTPRequestUnit extends HTTPRequest {

	
	@Test
	public void urlResources() throws APIError{
		url("https://www.simplicitylabs.ca:443/:country/:state/:city");
		resource("country", "canada");
		resource("state", "ontario");
		resource("city", "toronto");
		String url = url();
		print(url);
		Assert.assertTrue(url.equals("https://www.simplicitylabs.ca:443/canada/ontario/toronto"));
	}
	
	@Test
	public void testBasicAuth() throws APIError {
		basicAuth("username", "password");
		String authorization = basicAuthHeader();
		print(authorization);
		Assert.assertTrue(authorization.equals("Basic dXNlcm5hbWU6cGFzc3dvcmQ="));
	}
	
	@Test
	public void testPayload() {
		parameter("country", "canada");
		parameter("state", "ontario");
		parameter("city", "toronto");
		String payload = payload(null);
		print(payload);
	}
}
