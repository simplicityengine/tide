package ca.simplicitylabs.tide.enumeration;

public enum ComparisonType {
 EQ("="), NEQ("!="), GT(">"), LT("<"), GTEQ(">="), LTEQ("<="),
 
 //SQL comparisons
 LIKE("LIKE"), STARTSWITH("LIKE"), ENDSWITH("LIKE"),
 UNLIKE("LIKE"), NOTSTARTSWITH("NOT LIKE"), NOTENDSWITH("NOT LIKE"), IS("IS"), NOT("NOT"), ISNOT("IS NOT");
 
	private String _value;
	
	ComparisonType(String value){
		_value = value;
	}
	
	public String toString(){
		return _value;
	}
}
