package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;

public class DeleteClause extends SQLClause {
	public DeleteClause(){
		super();
	}
	
	public DeleteClause(SQLClause ...clauses){
		use(clauses);
	}
	
	public FromClause from(){
		return getClause(FromClause.class);
	}
	
	public WhereClause where(){
		return getClause(WhereClause.class);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		FromClause from = from();
		WhereClause where = where();
		
		if(from != null){
			Definable definable = from.getDefinable();
			if(definable != null){
				sb.append(String.format("DELETE FROM %s", definable.tableName()));
				
				if(where != null)
					sb.append(" ").append(where);
			}
			
		}
		
		return sb.toString();
	}
}
