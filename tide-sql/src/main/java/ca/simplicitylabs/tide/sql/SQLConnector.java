package ca.simplicitylabs.tide.sql;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.Conflict;
import ca.simplicitylabs.tide.exception.Forbidden;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.exception.NotFound;
import static ca.simplicitylabs.tide.enumeration.SerialFormat.SQL;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.sql.clauses.AlterTableClause;
import ca.simplicitylabs.tide.sql.clauses.AndClause;
import ca.simplicitylabs.tide.sql.clauses.DeleteClause;
import ca.simplicitylabs.tide.sql.clauses.FromClause;
import ca.simplicitylabs.tide.sql.clauses.InsertClause;
import ca.simplicitylabs.tide.sql.clauses.SQLClause;
import ca.simplicitylabs.tide.sql.clauses.SelectClause;
import ca.simplicitylabs.tide.sql.clauses.UpdateClause;
import ca.simplicitylabs.tide.sql.clauses.WhereClause;

public class SQLConnector extends SelfLogger {
	private int fetchSize = 0;
	private boolean transparency = false;
	private Connection connection;
	public SQLConnector() {
		
	}
	
	public SQLConnector(Connection connection){
		this.connection = connection;
	}
	
	public SQLConnector(Connection connection, int fetchSize){
		this(connection);
		setFetchSize(fetchSize);
	}
	
	public boolean opened(){
		return !closed();
	}
	
	public boolean closed(){
		try {
			return connection.isClosed();
		} 
		catch (SQLException e) {
			warn(e);
			return false;
		}
	}
	
	public boolean valid(){
		return valid(5);
	}
	
	/**
	 * this method checks if the active connection is still valid
	 * @param timeout number of seconds to wait for response from SQL server.
	 * @return
	 */
	public boolean valid(int timeout){
		try {
			return connection.isValid(timeout);
		} 
		catch (SQLException e) {
			warn(e);
			return false;
		}
	}
	
	public ResultSet query(SQLClause clause) throws APIError {
		if(clause instanceof SelectClause)
			return query(clause.toString());	
		else
			throw new Internal("Database Error: Unsupported SQL Clause");
	}
	
	public ResultSet query(String clause) throws APIError {
		Statement stmt = null;
		ResultSet rs = null;
		try{
			stmt = connection.createStatement();
			stmt.setFetchSize(fetchSize);
			rs = stmt.executeQuery(clause);
			
			return rs;
			
		}
		catch(SQLException e){
			close(rs);
			close(stmt);
			String message = String.format("[%d] (%s) %s", e.getErrorCode(), e.getSQLState(), e.getMessage());
			throw new Internal(msg(message, "Database Error During Query"));
		}
		catch(Exception ex){
			close(rs);
			close(stmt);
			throw AppUtils.normalize(ex);
		}
	}
	
	public Statement execute(SQLClause clause) throws APIError {
		if(clause instanceof UpdateClause)
			return update(clause.toString());
		
		else if(clause instanceof AlterTableClause || clause instanceof InsertClause)
			return execute(clause.toString());
		
		else
			throw new Internal("Database Error: Unsupported SQL Clause");
	}
	
	public Statement execute(String clause) throws APIError {
		return execute(clause, false);
	}
	
	public Statement update(String clause) throws APIError {
		return execute(clause, true);
	}
	
	private Statement execute(String clause, boolean update) throws APIError {
		Statement stmt = null;
		try{
			stmt = connection.createStatement();
			stmt.setFetchSize(fetchSize);
			
			if(update)
				stmt.executeUpdate(clause);
			
			else
				stmt.execute(clause);
			
			return stmt;
			
		}
		catch(SQLException e){
			close(stmt);
			String message = String.format("[%d] (%s) %s", e.getErrorCode(), e.getSQLState(), e.getMessage());
			throw new Internal(msg(message, "Database Error During Execution"));
		}
		catch(Exception ex){
			close(stmt);
			throw AppUtils.normalize(ex);
		}
	}

	public <T extends Definable> List<T> select(SelectClause clause, Class<T> cls) throws APIError {
		List<T> list = new ArrayList<T>();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			stmt = connection.createStatement();
			stmt.setFetchSize(fetchSize);
			rs = stmt.executeQuery(clause.toString());
			while(rs.next()){
				T definable = cls.newInstance();
				
				if(clause.expanded())
					definable.expand(rs);
				else
					definable.load(rs);
				
				list.add(definable);
			}
		} 
		catch(SQLException e){
			String message = String.format("[%d] (%s) %s", e.getErrorCode(), e.getSQLState(), e.getMessage());
			throw new Internal(msg(message, "Database Error During Query"));
		}
		catch (InstantiationException | IllegalAccessException e) {
			// TODO: scan SQL exception to map to an APIError
			throw new Internal("Database or other Internal Error was encounterd");
		}
		finally{
			close(rs);
			close(stmt);
		}
		
		return list;
	}
	
	public SQLResponse insert(InsertClause clause) throws APIError {
		SQLResponse resp =  mutate(clause);
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS id");
			
			int id = (rs.next() ? rs.getInt("id") : 0);
			
			if(id <= 0)
				throw new BadRequest("Creation Unsuccessful");
			
			resp.id(id);
			
			
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		} 
		finally{
			close(rs);
			close(stmt);
		}
		return resp;
	}

	
	public SQLResponse update(UpdateClause clause) throws APIError {
		return mutate(clause);
	}
	
	public SQLResponse delete(DeleteClause clause) throws APIError {
		return mutate(clause);
	}

	
	public SQLResponse alter(AlterTableClause clause) throws APIError {
		return mutate(clause);
	}

	@SuppressWarnings("unchecked")
	public void render(Definable... definables) throws APIError {
		if(definables == null)
			return;
		
		if(definables.length <= 0)
			return;
		
		Definable definable = definables[0];
		
		// return if only 1 definable argument is present
		if(!definable.hasId() && definables.length <= 1)
			return;
		
		// avoid re-rendering a definable that has been already rendered
		if(definable.isRendered())
			return;

		Definable signature =  definable.principalSignature("id");
		Class<? extends Definable> cls =  definable.getClass();
		AndClause and = new AndClause(signature);
		WhereClause where = new WhereClause(and);
		FromClause from = new FromClause(signature);
		SelectClause select = new SelectClause(from, where);
		select.limit(1);
		
		for(int i = 1; i < definables.length; i++){
			Definable foreign = definables[i];
			and.foreignValue("id", foreign);
		}

		List<Definable> list = (List<Definable>) select(select, cls);
		
		if(list.size() <= 0)
			return;
		
		definable.render();
		Definable result = list.get(0);
	
		List<Field> fields = definable.getFields();
		for(Field field : fields){
			try {
				boolean external = false;
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					if(column.ignore())
						continue;
					
					external = column.external();
				}
				//String key = field.getName();
				Object value = field.get(result);
				Class <?> type = field.getType();
				
				if(value instanceof Definable){
					Definable child = (Definable)value;
					if(external)
						render(child, definable);
					else
						render(child);	
				}
				// handle External Columns
				else if( Definable.class.isAssignableFrom(type)){
					Class<? extends Definable> childClass = (Class<? extends Definable>) type;
					
					if(external){
						if(value == null)
							value = childClass.newInstance();
						
						Definable child = (Definable) value;
						child.principal(true);
						render(child, definable);
						
						if(!child.hasId())
							value = null;
					}
				}
				
				else if(List.class.isAssignableFrom(type)){
					ParameterizedType generic = (ParameterizedType) field.getGenericType();
					Class<?> genericType = (Class<?>) generic.getActualTypeArguments()[0];
					if(Definable.class.isAssignableFrom(genericType)){
						Class<? extends Definable> itemType = (Class<? extends Definable>) genericType;
						Definable item = itemType.newInstance();
						item.principal(false);
						AndClause a = new AndClause(item);
						WhereClause w = new WhereClause(a);
						FromClause f = new FromClause(item);
						SelectClause s = new SelectClause(w,f);
						a.foreignValue("id", definable);
						
						trace(s);
						List<Definable> children = (List<Definable>) select(s, itemType);
						for(Definable child : children){
							render(child);
						}
						
						if(!children.isEmpty())
							value = children;
					}
				}
				
				field.set(definable, value);
				
			} catch (Exception e) {
				warn(e);
			}
		}
		definable.deserialized(SQL, result);
	}
	
	public void renderID(Definable definable,  Definable ...foreigns) throws APIError {
		AndClause and = new AndClause(definable);
		WhereClause where = new WhereClause(and);
		FromClause from = new FromClause(definable);
		SelectClause select = new SelectClause(where, from);
		if(foreigns != null){
			for(Definable foreign : foreigns)
				and.foreignValue("id", foreign);
		}
		select.limit(1);
		trace(select);
		List<? extends Definable> list =  select(select, definable.getClass());
		
		for(Definable item : list){
			definable.setId(item.getId());
		}
	}
	
	public void expand(Definable definable,  Definable ...foreigns) throws APIError {
		try{
			AndClause and = new AndClause(definable.principalSignature("id"));
			WhereClause where = new WhereClause(and);
			SelectClause select = new SelectClause(where);
			for(Definable foreign : foreigns)
				and.foreignValue("id", foreign);
			
			select.limit(1);
			select.expand(definable.getClass());
			trace(select);
			ResultSet rs = query(select);
			
			if(rs.next())
				definable.expand(rs);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public void expandLists(Definable definable, String... filters) throws APIError{
		expandLists(definable, true, true, filters);
	}
	
	public void expandLists(Definable definable, boolean isolatePrincipal, String... filters) throws APIError{
		expandLists(definable, isolatePrincipal, true, filters);
	}
	
	public void expandLists(Definable definable, boolean isolatePrincipal, boolean deep, String... filters) throws APIError{
		try{
			List<Field> fields = definable.getFields();
			for(Field field : fields){
				try{
					boolean ignore = false;
					boolean external = false;
					boolean iterable = false;
					
					String name = field.getName();
					Class<?> type = field.getType();
					Object value = field.get(definable);
					
					if(filters.length > 0){
						boolean found = false;
						for(String filter : filters){
							if(name.equals(filter)){
								found = true;
								break;
							}
						}
						
						if(!found)
							continue;
					}
					
					if(field.isAnnotationPresent(Column.class)){
						Column column = field.getAnnotation(Column.class);
						ignore = column.ignore();
						external = column.external();
					}
					
					if(ignore)
						continue;
					
					if(List.class.isAssignableFrom(type)){
						type = AppUtils.genericType(field);
						iterable = true;
					}
					
					if(Definable.class.isAssignableFrom(type)){
						Definable child = (Definable) type.newInstance();
						
						if(iterable){
							if(isolatePrincipal)
								child.principal(false);
							
							AndClause and = new AndClause(child);
							WhereClause where = new WhereClause(and);
							SelectClause select = new SelectClause(where);
							select.expand(child.getClass());
							and.foreignValue("id", definable);
							
							trace(select);
							List<? extends Definable> list = select(select, child.getClass());
							
							if(deep){
								for(Definable item : list)
									expandLists(item, isolatePrincipal);
							}
							
							
							if(!list.isEmpty())
								field.set(definable, list);
						}
						else{
							if(value != null && deep){
								child = (Definable) value;
								if(ok(child))
									expandLists(child, isolatePrincipal);
							}
						}
					}
				}
				catch(Exception ex){
					warn(ex);
				}
			}
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	private boolean ok(Definable definable){
		if(definable != null)
			return definable.hasId();
		
		return false;
	}
	
	public boolean exists(Definable definable,  Definable ...foreigns) throws APIError {
		Definable search = definable.principalSignature("id");
		AndClause and = new AndClause(search);
		WhereClause where = new WhereClause(and);
		FromClause from = new FromClause(search);
		SelectClause select = new SelectClause(where, from);
		if(foreigns != null){
			for(Definable foreign : foreigns)
				and.foreignValue("id", foreign);
		}
		select.limit(1);
		trace(select);
		List<? extends Definable> list = select(select, search.getClass());
		
		return !list.isEmpty();
	}
	
	public void add(Definable definable, Definable... foreigns) throws APIError {
		List<Definable> externals = definable.externals();
		
		AlterTableClause alter = new AlterTableClause(definable);
		InsertClause insert = new InsertClause(definable);
		
		for(Definable foreign : foreigns)
			insert.foreignValue("id", foreign);
		
		debug(insert);
		alter.autoIncrement(1);
		alter(alter);
		SQLResponse response = insert(insert);
		
		definable.setId(response.id());
		
		if(!definable.hasId())
			throw new Internal("%s was not successfully inserted into the database!", definable.getClass().getSimpleName());
		
		for(Definable external : externals){
			
			// skip read-only externals
			if(external.readonly())
				continue;
			
			external.setId(null);
			external.principal(true);
			AlterTableClause a = new AlterTableClause(external);
			InsertClause i = new InsertClause(external);
			a.autoIncrement(1);
			i.foreignValue("id", definable);
			debug(i);
			alter(a);
			insert(i);
		}
		// TODO: add lists
	}
	
	public void modify(Definable definable, Definable... foreigns) throws APIError {
		// TODO: make recursive
		// TODO: make threadsafe (can be risky)
		try{
			if(!definable.hasId())
				throw new BadRequest("%s ID is Required!", definable.getClass().getSimpleName());
			
			available(definable, foreigns);
			Definable search = definable.principalSignature("id");
			List<Definable> externals = definable.externals(true);
			
			
			
			if(definable.hasColumns("id")){
				// make sure to not update the id
				definable.setId(null);
				
				// build and execute query
				WhereClause where = new WhereClause(search);
				UpdateClause update = new UpdateClause(definable, where);
				for(Definable foreign : foreigns)
					where.foreignValue("id", foreign);
				
				debug(update);
				update(update);
				
				// reset the profile id to its original value
				definable.setId(search.getId());
			}
			
			for(Definable external : externals){
				// skip read-only externals
				if(external.readonly())
					continue;
				
				external.principal(true);
				Definable identifier = external.signature("id");
				boolean present = ( identifier.hasId() ? exists(identifier, definable) : false );
				
				// make sure to not update the id
				external.setId(null);
				// remove existing principal
				clearPrincipal(external, search);
				if(present){
					WhereClause where = new WhereClause(identifier);
					UpdateClause update = new UpdateClause(external, where);
					where.foreignValue("id", search);
					debug(update);
					update(update);
				}
				
				else{
					AlterTableClause alter = new AlterTableClause(identifier);
					InsertClause insert = new InsertClause(external);
					insert.foreignValue("id", search);
					alter.autoIncrement(1);
					
					trace(alter);
					alter(alter);
					debug(insert);
					insert(insert);
				}
				
				
				// reset the profile id to its original value
				external.setId(identifier.getId());
			}
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
		
	}
	
	private void clearPrincipal(Definable external, Definable foreign) throws APIError{
		if(external.principal()){
			Definable clearer = external.principalSignature();
			clearer.principal(false);
			AndClause and = new AndClause(external.signature());
			WhereClause where = new WhereClause(and);
			UpdateClause update = new UpdateClause(clearer, where);
			and.foreignValue("id", foreign);
			trace(update);
			update(update);
		}
	}
	public void setExternalIds(Definable definable) throws APIError{
		setExternalIds(definable, true);
	}
	public void setExternalIds(Definable definable, boolean overwrite) throws APIError{
		List<Definable> externals = definable.externals();
		for(Definable external : externals){
			Definable search = external.principalSignature("id");
			search.principal(true);
			renderID(search, definable);
			
			if(external.hasId()){
				if(overwrite)
					external.setId(search.getId());
			}
			else{
				external.setId(search.getId());
			}
			
		}
	}
	
	public void drop(Definable definable, Definable... foreigns) throws APIError{
		if(!definable.hasId())
			throw new BadRequest("id required");
		
		if(!definable.isRendered()){
			available(definable, foreigns);
			render(definable);
		}
		
		List<Definable> externals = definable.externals();
		List<List<Definable>> lists = definable.lists();
		
		for(Definable external : externals){
			// skip read-only externals
			if(external.readonly())
				continue;
			
			external.principal(true);
			drop(external);
		}
		
		for(List<Definable> list : lists){
			for(Definable item : list){
				item.principal(false);
				drop(item);
			}
		}
		
		Definable search = (Definable) definable.principalSignature("id");
		WhereClause where = new WhereClause(search);
		FromClause from = new FromClause(search);
		DeleteClause delete = new DeleteClause(from, where);
		
		for(Definable foreign : foreigns){
			from.use(foreign);
			where.foreignValue("id", foreign);
		}
		debug(delete);
		SQLResponse r = delete(delete);
		
		if(!r.isSuccessful())
			throw new NotFound("%s (id: %d) was not found!", definable.getClass().getSimpleName(), definable.getId());
	}
	
	private SQLResponse mutate(SQLClause clause) throws APIError{
		Statement stmt = null;
		try {
			// TODO: make configurable
			stmt = connection.createStatement();
			String sql = clause.toString();
			int count = 0;
			boolean success = false;

			if(clause instanceof UpdateClause)
				stmt.executeUpdate(sql);
			
			else
				stmt.execute(sql);

			
			count = stmt.getUpdateCount();
			success = (count > 0);
			
			return new SQLResponse(count, success);
		} catch (SQLException e) {
			int code = e.getErrorCode();
			String state = e.getSQLState();
			String message = String.format("[%d] (%s) %s", code, state, e.getMessage());
			
			switch(code){
				case 1062:
				case 1644: 
					throw new Conflict(msg(message, "Already Exists!"));
				
				case 1364:
					throw new BadRequest(msg(message, "Bad Format"));
					
				case 1452:
					throw new BadRequest(msg(message, "Non Existing Reference"));
			}
			throw new Internal(msg(message, "Database or other Internal Error was encounterd"));
		}
		finally{
			close(stmt);
		}
	}
	
	private String msg(String input, String fallback){
		return (transparency ? input : fallback);
	}
	
	private boolean available(Definable definable, Definable... foreigns) throws APIError{
		Definable search = definable.signature("id");
		if(exists(search, foreigns))
			return true;
		
		if(foreigns.length > 0){
			String delimiter = "";
			StringBuilder sb = new StringBuilder();
			for(Definable foreign : foreigns){
				sb.append(delimiter).append(String.format("%s (id: %d)", foreign.getClass().getSimpleName(), foreign.getId()));
				delimiter = " and ";
			}
			sb.append(String.format(" has no access to %s (id: %d)", search.getClass().getSimpleName(), search.getId()));
			throw new Forbidden(sb.toString());
		}
		
		throw new NotFound("%s (id: %d) was not found in the Database", search.getClass().getSimpleName(), search.getId());
	}
	
	public void close() throws APIError {
		try {
			trace("closing connection");
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			error(e);
			throw new Internal("SQL Connection Error");
		}
	}
	
	public void close(Statement statement){
		try{
			if(statement != null)
				statement.close();
		}
		catch(Exception ex){
			warn(ex);
		}
	}
	
	public void close(ResultSet rs){
		try{
			if(rs != null)
				rs.close();
		}
		catch(Exception ex){
			warn(ex);
		}
	}
	
	public boolean isAutoCommit() throws APIError{
		try {
			return connection.getAutoCommit();
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		}
	}
	public void setAutoCommit(boolean autocommit) throws APIError{
		
		try {
			connection.setAutoCommit(autocommit);
			
			execute(String.format("SET autocommit=%d", autocommit ? 1 : 0));
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public void commit() throws APIError{
		try {
			connection.commit();
		} catch (SQLException e) {
			// TODO: add filtering for transparency
			throw AppUtils.normalize(e);
		}
	}
	
	public void rollback() throws APIError{
		try {
			connection.rollback();
		} catch (SQLException e) {
			// TODO: add filtering for transparency
			throw AppUtils.normalize(e);
		}
	}
	private APIError err(SQLException ex){
		return err(ex,Internal.class);
	}
	private <T extends APIError> T err(SQLException ex, Class<T> type){
		try{
			int code = ex.getErrorCode();
			String state = ex.getSQLState();
			String message = String.format("[%d] (%s) %s", code, state, ex.getMessage());
			T error = type.newInstance();
			error.setMessage(message);
			return error;
		}
		catch(Exception e){
			warn(e);
		}
		return null;
	}
	
	
	public boolean isTransparent(){
		return transparency;
	}
	
	public void setTransparency(boolean value){
		this.transparency = value;
	}
	
	public int getFetchSize() {
		// TODO Auto-generated method stub
		return fetchSize;
	}
	
	public void setFetchSize(int value) {
		this.fetchSize = value;
	}

}
