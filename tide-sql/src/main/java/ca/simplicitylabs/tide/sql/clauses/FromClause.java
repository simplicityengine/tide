package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;

public class FromClause extends SQLClause {
	public FromClause(){
		
	}
	public FromClause(Definable ...definables){
		for(Definable definable : definables){
			use(definable);
		}
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		if(!definables.isEmpty() ){
			int size = definables.size();
			sb.append("FROM ");
			for(int i = 0; i < size; i++){
				Definable definable = definables.get(i);
				String delimiter = (i > 0 ? ", " : "");
				String name = definable.tableLabel();
				sb.append(delimiter).append(name);
			}
		}
		
		return sb.toString();
	}
}
