package ca.simplicitylabs.tide.sql.clauses;

import java.util.List;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public class WhereClause extends SQLClause {
	public WhereClause(){
		
	}
	
	public WhereClause(SQLClause ...clauses){
		use(clauses);
	}
	
	public WhereClause(Definable ...definable){
		use(definable);
	}
	
	public void use(SQLClause ...clauses){
		SQLClause main = null;
		if(!this.clauses.isEmpty())
			main = this.clauses.get(0);
		
		if(main == null){
			main = new AndClause();
			super.use(main);
		}
		
		for(SQLClause clause : clauses){
			if(clause instanceof LogicalClause)
				main.use(clause);
		}
	}
	
	@Override
	public void use(Definable ...models){
		AndClause and = new AndClause(models[0]);
		use(and);
	}
	
	@Override
	public List<Definable> getForeignValues(String key){
		for(SQLClause clause : clauses)
			return clause.getForeignValues(key);
		
		return super.getForeignValues(key);
	}
	
	@Override
	public <T extends Definable> T getForeignValue(String key, Class<T> cls){
		for(SQLClause clause : clauses)
			return clause.getForeignValue(key, cls);
		
		return super.getForeignValue(key, cls);
	}
	
	@Override
	public void foreignValue(String key, Definable definable){
		for(SQLClause clause : clauses)
			clause.foreignValue(key, definable);
	}
	
	@Override
	public void foreignValue(Definable definable){
		for(SQLClause clause : clauses)
			clause.foreignValue(definable);
	}
	
	@Override
	public List<Definable> getForeignKeys(String key){
		for(SQLClause clause : clauses)
			return clause.getForeignKeys(key);
		
		return super.getForeignKeys(key);
	}
	
	@Override
	public <T extends Definable> T getForeignKey(String key, Class<T> cls){
		for(SQLClause clause : clauses)
			return clause.getForeignKey(key, cls);
		
		return super.getForeignKey(key, cls);
	}
	
	@Override
	public void foreignKey(String key, Class<? extends Definable> type) throws APIError{
		for(SQLClause clause : clauses){
			clause.foreignKey(key, type);
			break;
		}
	}
	
	@Override
	public void foreignKey(String key, Definable definable){
		for(SQLClause clause : clauses)
			clause.foreignKey(key, definable);
	}
	
	@Override
	public void foreignKey(Definable definable){
		for(SQLClause clause : clauses)
			clause.foreignKey(definable);
	}
	
	public boolean valid(){
		for(SQLClause clause : clauses){
			if(clause.valid())
				return true;
		}
		return false;
	}
	
	@Override
	public String toString(){
		if(valid()){
			if(clauses.size() > 0){
				SQLClause clause = clauses.get(0);
				
				if(clause.valid())
					return String.format("WHERE %s", clause);
			}
		}
		
		return "";
	}
}
