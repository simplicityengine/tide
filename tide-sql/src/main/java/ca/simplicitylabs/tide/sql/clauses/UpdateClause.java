package ca.simplicitylabs.tide.sql.clauses;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.sql.SQLOption;
import ca.simplicitylabs.tide.utilization.SQLUtils;

public class UpdateClause extends SQLClause {
	public UpdateClause(){
	}
	
	public UpdateClause(Definable definable){
		super();
		use(definable);
	}
	
	public UpdateClause(Definable definable, WhereClause where){
		use(definable);
		use(where);
	}
	
	public WhereClause where(){
		for(SQLClause clause : clauses){
			if(clause instanceof WhereClause)
				return (WhereClause) clause;
		}
		return null;
	}
	/**
	 * @category Tidy Method
	 * @param definable: i
	 **/
	@Override
	public void use(Definable ...models){
		Definable definable = models[0];
		super.use(definable);
		SQLOption option = new SQLOption();
		option.enumerable(true);
		
		definable.setId(null);
		
		/* This ensures that the id field in an update
		 * clause never gets put into the SET operation.
		 * this option is not nullable so it will not be
		 * used when the id is set to null.  This complies
		 * with the Tidy database setup standards*/
		
		try {
			configure(definable.getField("id"), option);
		} catch (NoSuchFieldException e) {
			error(e);
		}
	}
	
	@Override
	public boolean valid(){
		Definable definable = getDefinable();
		WhereClause where = where();
		
		if(definable != null && where != null){
			if(where.valid())
				return definable.hasPrimitives("id");
		}
		
		return false;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		Definable definable = getDefinable();
		WhereClause where = where();
		String table = definable.tableName();
		List<Field> fields = getFields();
		String delimiter = "";
		String equate = "%s = %s";
		
		sb.append("SET ");
		for(Field field : fields){
			// skip read-only fields
			if(definable.readonly(field))
				continue;
			
			String name = definable.columnName(field);
			String value = sqlValue(field, definable, false);
			sb.append(delimiter);
			sb.append(String.format(equate, name, value));
			delimiter = ", ";
		}
		
		if(foreignValues.size() > 0){
			Set<String> keys = foreignValues.keySet();
			for(String key : keys){
				List<Definable> foreigns = getForeignValues(key);
				SQLOption option = getConfiguration(key);
				for(Definable foreign : foreigns){
					String label = foreign.columnLabel(key);
					String name = String.format("%s.%s", table, label);
					Object value = foreign.getValue(key);
					if(value instanceof Definable)
						value = ((Definable) value).getId();
					
					// skip if value is a null, but not nullable
					if(value == null && !option.nullable())
						continue;
					
					value = sqlValue(key, value);
					
					sb.append(delimiter);
					sb.append(String.format(equate, name, value));
					delimiter = ", ";
				}
				
			}
		}
		
		if(map.size() > 0){
			Set<String> keys = map.keySet();
			for(String key : keys){
				Object value = avp(key);
				if(!key.contains("."))
					key = safeName(key);
				
				if(value instanceof Definable){
					Definable child = (Definable) value;
					value = child.getId();
				}
				else if(value instanceof String){
					value = SQLUtils.escape((String) value);
				}
				sb.append(delimiter);
				sb.append(String.format(equate, key, value));
				delimiter = ", ";
			}
		}
		
		
		
		return String.format("UPDATE %s %s %s", table, sb, where);
	}
}
