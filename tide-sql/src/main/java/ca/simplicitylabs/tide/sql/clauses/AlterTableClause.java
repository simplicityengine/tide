package ca.simplicitylabs.tide.sql.clauses;

import java.util.Iterator;

import ca.simplicitylabs.tide.definition.Definable;

public class AlterTableClause extends SQLClause {
	
	public AlterTableClause(){
		
	}
	public AlterTableClause(Definable definable){
		use(definable);
	}
	public AlterTableClause(Class<? extends Definable> cls) throws InstantiationException, IllegalAccessException{
		this(cls.newInstance());
	}
	
	public Integer autoIncrement(){
		Object value = map.get("AUTO_INCREMENT");
		
		if(value instanceof Integer){
			return (Integer) value;
		}
		return null;
	}
	
	public void autoIncrement(Integer value){
		if(value == null)
			map.remove("AUTO_INCREMENT");
		else
			map.put("AUTO_INCREMENT", value);
	}
	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		Definable definable = this.getDefinable();
		
		if(map.size() > 0 && definable != null){
			sb.append("ALTER TABLE ").append(definable.tableName());
			
			Iterator<String> keys = map.keySet().iterator();
			String delimiter = "";
			while(keys.hasNext()){
				String key = keys.next();
				Object value = map.get(key);
				sb.append(delimiter).append(" ");
				sb.append(key).append("=").append(value);
				delimiter = ",";
			}
		}
		return sb.toString();
	}
}
