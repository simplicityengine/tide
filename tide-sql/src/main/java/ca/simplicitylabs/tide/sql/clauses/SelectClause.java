package ca.simplicitylabs.tide.sql.clauses;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.sql.SQLOption;

public class SelectClause extends SQLClause{
	private int limit = 0;
	private boolean distinct = false;
	private String distinctName;
	private String distinctLabel;
	private boolean expanded = false;
	
	public SelectClause(){
		super();
	}
	
	public SelectClause(SQLClause ...clauses){
		use(clauses);
	}
	
	public SelectClause selector(Definable definable){
		if(definable != null){
			List<Field> fields = definable.getColumns(true);
			for(Field field : fields)
				selector(field.getName(), definable);
		} 
		return this;
	}

	public <T extends Definable> SelectClause selector(String key, Class<T> cls){
		try {
			Definable definable = cls.newInstance();
			return selector(key, definable);
		} catch (Exception e) {
			error(e);
		}
		
		return this;
	}
	
	public <T extends Definable> SelectClause selector(String key, Definable definable){
		try {
			String name = definable.columnName(key);
			String label = ( expanded() ? String.format("%s_%s", AppUtils.singular(definable.tableName()), definable.annotatedName(key)) : definable.columnLabel(key) );
			rename(name, label);
			return this;
		} catch (Exception e) {
			error(e);
		}
		
		return this;
	}
	
	public <T extends Definable> SelectClause selector(Class<T> cls, String... keys){
		try {
			Definable definable = cls.newInstance();
			if(keys.length > 0){
				for(String key : keys){
					String name = definable.columnName(key);
					String label = definable.columnLabel(key);
					rename(name, label);
				}
			}
			else
				return selector(definable);
			
		}
		catch (InstantiationException | IllegalAccessException e) {
			error(e);
		}
		return this;
	}
	
	
	
	public FromClause from(){
		return getClause(FromClause.class);
	}
	
	public WhereClause where(){
		return getClause(WhereClause.class);
	}
	
	public OrderClause order(){
		return getClause(OrderClause.class);
	}
	
	public List<JoinClause> joins(){
		List<JoinClause> list = new ArrayList<JoinClause>();
		for(SQLClause clause : clauses){
			if(clause instanceof JoinClause)
				list.add((JoinClause) clause);
		}
		
		return list;
	}
	
	public Integer limit(){
		return limit;
	}
	
	public void limit(int value){
		limit = value;
	}
	
	public boolean expanded(){
		return expanded;
	}
	
	public boolean distinct(){
		return distinct;
	}
	
	public void distinct(String key, Class<? extends Definable> type) throws APIError{
		try{
			Definable definable = type.newInstance();
			distinct(key, definable);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public void distinct(String key, Definable definable){
		distinct = true;
		distinctName = definable.columnName(key);
		distinctLabel = definable.columnLabel(key);
	}
	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		FromClause from = from();
		WhereClause where = where();
		OrderClause order = order();
		
		
		if(from != null){
			List<Definable> models = from.getDefinables();
			if(!labels.isEmpty()){
				Set<String> set = labels.keySet();
				Iterator<String> iter = set.iterator();
				sb.append("SELECT ");
				String delimiter = "";
				if(distinct()){
					sb.append(String.format("DISTINCT(%s) AS %s", distinctName, safeName(distinctLabel)));
					delimiter = ", ";
				}
				
				while(iter.hasNext()){
					String key = iter.next();
					String column = key;				
					String label = labels.get(key);
					if(distinct()){
						if(key.equalsIgnoreCase(distinctName) || label.equalsIgnoreCase(distinctLabel))
							continue;
					}
					
					sb.append(delimiter);
					sb.append(String.format("%s AS `%s`", column, label));
					if(delimiter.isEmpty())
						delimiter = ", ";
				}
			}
			else if(!models.isEmpty()){

				sb.append("SELECT ");
				String delimiter = "";
				if(distinct()){
					sb.append(String.format("DISTINCT(%s) AS %sd", distinctName, safeName(distinctLabel)));
					delimiter = ", ";
				}
				
				for(Definable definable : models){
					List<Field> fields = definable.getColumns(true);
					for(Field field: fields){
						String key = definable.columnName(field);
						String label = definable.columnLabel(field);
						
						if(distinct()){
							if(key.equalsIgnoreCase(distinctName) || label.equalsIgnoreCase(distinctLabel))
								continue;
						}
						
						String column = key;
						
						sb.append(delimiter);
						sb.append(String.format("%s AS %s", column, label));
						if(delimiter.isEmpty())
							delimiter = ", ";
					}
				}
			}
			sb.append(" ");
			sb.append(from);
			
			List<JoinClause> joins = joins();
			
			for(JoinClause join : joins){
				if(join.valid())
					sb.append(" ").append(join);
			}
			
			if(where != null){
				if(where.valid())
					sb.append(" ").append(where);
			}
			
			if(order != null){
				if(order.valid()){
					sb.append(" ").append(order);
				}
			}
			
			if(limit > 0 ){
				sb.append(" ").append("LIMIT ").append(limit);
			}
		}
		
		return sb.toString();
	}
	
	public <T extends Definable> void expand(Class<T> type) throws APIError{
		try{
			expanded = true;
			Definable definable = type.newInstance();
			if(from() == null)
				use(new FromClause(definable));
			
			expandDive(definable);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	private void expandDive(Definable definable) throws APIError{
		expandDive(definable, true);
	}
	private void expandDive(Definable definable, boolean next) throws APIError{
		try{
			selector(definable);
			if(!next)
				return;
			
			
			List<Field> fields = definable.getFields();
			
			for(Field field : fields){
				String key = field.getName();
				Class<?> type = field.getType();
				if(Definable.class.isAssignableFrom(type)){
					boolean expand = true;
					boolean ignore = false;
					boolean external = false;
					if(field.isAnnotationPresent(Column.class)){
						Column column = field.getAnnotation(Column.class);
						expand = column.expand();
						ignore = column.ignore();
						external = column.external();
						if(!column.value().trim().isEmpty())
							key = column.value();
					}
					
					if(!ignore){
						Definable child = (Definable) type.newInstance();
						String label = String.format("%s_%s", AppUtils.singular(definable.tableName()), AppUtils.plural(key));
						child.label(label);
						if(external)
							child.principal(true);
						
						AndClause and = new AndClause(child);
						JoinClause join = new LeftJoinClause(child, and);
						use(join);
						
						if(external)
							and.associate(definable.columnName("id"), String.format("%s.%s", child.tableName(), definable.columnLabel("id")));
						else
							and.associate(child.columnName("id"), definable.columnName(key));
						
						expandDive(child, expand);
					}
				}
			}
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
}
