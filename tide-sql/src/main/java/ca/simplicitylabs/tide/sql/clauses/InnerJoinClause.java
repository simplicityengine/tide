package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public class InnerJoinClause extends JoinClause {

	public InnerJoinClause() {
		super();
	}

	public InnerJoinClause(Definable definable) throws APIError {
		super(definable);
	}

	public InnerJoinClause(LogicalClause... clauses) {
		super(clauses);
	}
	
	public InnerJoinClause(Definable definable, LogicalClause...clauses){
		super(definable, clauses);
	}

	@Override
	public String prefix() {
		return "INNER";
	}

}
