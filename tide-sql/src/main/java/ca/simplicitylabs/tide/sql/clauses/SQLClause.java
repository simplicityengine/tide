package ca.simplicitylabs.tide.sql.clauses;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.collection.MultiMap;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.enumeration.ComparisonType;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.sql.SQLOption;
import ca.simplicitylabs.tide.utilization.SQLUtils;

public abstract class SQLClause extends SelfLogger {
	protected Map<String, Object> map = new HashMap<String, Object>();
	protected MultiMap<String, Definable> foreignKeys = new MultiMap<String, Definable>();
	protected MultiMap<String, Definable> foreignValues = new MultiMap<String, Definable>();
	protected Map<String, String> labels = new HashMap<String, String>();
	protected Map<String, SQLOption> options = new HashMap<String, SQLOption>();
	protected List<Definable> definables = new ArrayList<Definable>();
	protected Map<String, String> associations = new HashMap<String, String>();
	protected List<SQLClause> clauses = new ArrayList<SQLClause>();
	
	private SQLOption defaultOption = new SQLOption();
	
	public SQLClause(){
		
	}
	
	public SQLClause(Definable definable){
		this();
		use(definable);
	}
	
	public SQLClause(Definable definable, SQLOption option){
		this(definable);
		configure(option);
	}
	
	/**
	 * This method puts a safety on the column name
	 * @param name
	 * @return
	 */
	public String safeName(String name){
		return String.format("`%s`", name);
	}
		
	public void rename(Field field, String name){
		rename(field.getName(), name);
	}
	
	public void rename(String key, String name){
		labels.put(key, name);
	}
	
	public String getName(Field field, Definable definable){
		String name = definable.columnName(field);
		
		
		if(labels.containsKey(field.getName())){
			name = labels.get(field.getName());
		}
		
		return name;
	}
	
	public void configure(SQLOption option){
		defaultOption = option;
	}
	
	public void configure(Field field, SQLOption option){
		configure(field.getName(), option);
	}
	
	public void configure(String key, SQLOption option){
		options.put(key, option);
	}
	
	public void configure(Field field, String label, SQLOption option){
		configure(field.getName(), label, option);
	}
	
	public void configure(String key, String label, SQLOption option){
		labels.put(key, label);
		options.put(key, option);
	}
	
	public boolean isConfigured(Field field){
		return isConfigured(field.getName());
	}
	public boolean isConfigured(String key){
		return options.containsKey(key);
	}
	
	public SQLOption getConfiguration(){
		return defaultOption;
	}
	
	public SQLOption getConfiguration(Field field){
		return getConfiguration(field.getName());
	}
	public SQLOption getConfiguration(String key){
		
		if(options.containsKey(key))
			return options.get(key);
		else
			return defaultOption;
	}
	
	
	public void associate(String left, String right){
		associations.put(left, right);
	}
	
	public Object avp(String key){
		return map.get(key);
	}
	
	public Object avp(String key, Object value){
		return map.put(key, value);
	}
	
	public List<Definable> getForeignValues(String key){
		return foreignValues.list(key);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Definable> T getForeignValue(String key, Class<T> cls){
		List<Definable> definables = getForeignValues(key);
		for(Definable definable : definables){
			if(cls.equals(definable.getClass()))
				return (T) definable;
		}
		return null;
	}
	
	public void foreignValue(String key, Definable definable){
		foreignValues.put(key, definable);
	}
	
	public void foreignValue(Definable definable){
		foreignValues.put("id", definable);
	}
	
	public List<Definable> getForeignKeys(String key){
		return foreignKeys.list(key);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Definable> T getForeignKey(String key, Class<T> cls){
		List<Definable> definables = getForeignKeys(key);
		for(Definable definable : definables){
			if(cls.equals(definable.getClass()))
				return (T) definable;
		}
		return null;
	}
	
	public void foreignKey(String key, Class<? extends Definable> type) throws APIError{
		try {
			foreignKeys.put(key, type.newInstance());
		} catch (Exception e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public void foreignKey(String key, Definable definable){
		foreignKeys.put(key, definable);
	}
	
	public void foreignKey(Definable definable){
		foreignKeys.put("id", definable);
	}
	
	public void foreign(String key, Definable definable){
		foreignKey(key, definable);
		foreignValue(key, definable);
	}
	
	public void foreign(Definable definable){
		foreignKey("id", definable);
		foreignValue("id", definable);
	}
	
	public Object getValue(Field field){
		return getValue(field, getDefinable());
	}
	
	public Object getValue(Field field, Definable definable){
		
		try {
			if (definable != null)
				return definable.getValue(field);
		} catch (IllegalArgumentException e) {

		}
		
		return null;
	}
	
	public Definable getDefinable(){
		if(!definables.isEmpty())
			return definables.get(0);
		
		return null;
	}
	
	public boolean primitive(Field field){
		Object value = getValue(field);
		
		if(value == null)
			return true;
		
		return AppUtils.isPrimitive(field.getType());
	}
	
	public void use(SQLClause ...clauses){
		for(SQLClause clause : clauses){
			this.clauses.add(clause);
		}
	}
	
	public void use(Definable ...definables){
		for(Definable definable : definables){
			boolean found = false;
			
			for(Definable item : this.definables){
				if(item.getClass().equals(definable.getClass())){
					found = true;
					break;
				}
			}
			
			if(!found)
				this.definables.add(definable);
		}
			
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends SQLClause> T getClause(Class<T> cls){
		for(SQLClause clause : clauses){
			if(cls.equals(clause.getClass()))
				return (T) clause;
		}
		return null;
	}
	
	
	
	
	protected <T extends Definable> String columnLabel(String key, T definable){
		String singular = AppUtils.singular(definable.tableName());
		return String.format("%s_%s", singular, key);
	}
	
	public List<Definable> getDefinables(){
		return definables;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends SQLClause> List<T> clauses(Class<T> type){
		List<T> list = new ArrayList<T>();
		for(SQLClause clause : clauses){
			if(type.equals(clause.getClass()))
				list.add((T)clause);
		}
		return list;
	}
	
	public List<Field> getFields(){
		return getFields(getDefinable());
	}
	
	public List<Field> getFields(Definable definable){
		List<Field> outbound = new ArrayList<Field>();;
		if(definable != null){
			List<Field> inbound = definable.getColumns(true);
			for(Field field : inbound){
				SQLOption option = getConfiguration(field);
				Object value = getValue(field);
				Class<?> type = field.getType();
				boolean include = true;
				if(!option.nullable() && value == null)
					include = false;
				
				else if(value instanceof String){
					String string = (String) value;
					if(!option.voidable() && string.trim().isEmpty())
						include = false;
				}
				
				else if(!primitive(field) && !Definable.class.isAssignableFrom(type)){
					include = false;
				}
				
				if(include){
					outbound.add(field);
				}
			}
		}
		
		
		return outbound;
	}
	
	public boolean valid(){
		for(SQLClause clause : clauses){
			if(!clause.valid())
				return false;
		}
		return true;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
	protected String sqlValue(Field field){
		return sqlValue(field, false);
	}
	
	protected String sqlValue(Field field, boolean isCompare){
		return sqlValue(field, getDefinable(), isCompare);
	}
	
	protected String sqlValue(Field field, Definable definable){
		return sqlValue(field, definable, false);
	}
	
	protected String sqlValue(Field field, Definable definable, boolean isCompare){
		if(field.isAnnotationPresent(Column.class)){
			Class<?> type = field.getType();
			Column column = field.getAnnotation(Column.class);
			SQLOption option = null;
			if(isConfigured(field)){
				option = getConfiguration(field);
			}
			else{
				option = new SQLOption();
				configure(field, option);
			}
			
			if(Date.class.isAssignableFrom(type)){
				if(!column.timezone().trim().isEmpty()){
					if(!option.timezoned())
						option.timezone(column.timezone());
				}
				
				if(column.formats().length > 0){
					if(!option.formatted())
						option.format(column.formats()[0]);
				}
				
				//TODO: add epoch
			}
			else if(AppUtils.isString(type)){
				option.md5(column.md5());
			}
		}
		
		
		Object value = getValue(field, definable);
		return sqlValue(field.getName(), value, isCompare);
	}
	protected String sqlValue(String key, Object value){
		return sqlValue(key, value, false);
	}
	protected String sqlValue(String key, Object value, boolean isCompare){
		SQLOption option = getConfiguration(key);
		ComparisonType compare = option.compare();
		
		// handle UUID
		if(value instanceof UUID){
			UUID uuid = (UUID) value;
			value = uuid.toString();
		}
		
		if(value == null){
			return "NULL";
		}
		
		else if(value instanceof String){
			String prefix = "";
			String suffix = "";
			String string = (String) value;
			if(option.md5()){
				try{
					string = AppUtils.encryptMD5(string);
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			
			if( isNumeric(string) || isBoolean(string) ){
				if(option.enumerable())
					return string;
			}
			
			
			// escape for SQL
			string = SQLUtils.escape(string);
			
			if(isCompare){
				switch(compare){
					case STARTSWITH:
					case NOTSTARTSWITH:
						suffix = "%";
						break;
						
					case ENDSWITH:
					case NOTENDSWITH:
						prefix = "%";
						break;
						
					case LIKE:
					case UNLIKE:
						prefix = "%";
						suffix = "%";
						break;
				}
			}
				
			
			return String.format("'%s%s%s'", prefix, string, suffix);
		}
		else if ( value instanceof Boolean){
			boolean bool = (Boolean) value;
			value = (bool ? 1 : 0);
		}
		
		else if ( value instanceof Date ){
			Date date = (Date) value;
			String format = "yyyy-MM-dd HH:mm:ss.SSS";
			
			if(option.formatted())
				format = option.format();
			
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			
			if(option.timezoned()){
				TimeZone timezone = TimeZone.getTimeZone(option.timezone());
				sdf.setTimeZone(timezone);
			}
			
			return String.format("'%s'", sdf.format(date));
		}
		
		else if( value instanceof Definable){
			Definable child = (Definable) value;
			return String.valueOf(child.getId());
		}
		return String.valueOf(value);
	}
	
	protected boolean isNumeric(String value){
		try{
			Double.valueOf(value);
		}
		catch(Exception ex){
			return false;
		}
		
		
		return true;
	}
	
	protected boolean isBoolean(String value){
		return value.trim().toLowerCase().matches("^true$|^false$");
	}
}
