package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public class RightJoinClause extends JoinClause {

	public RightJoinClause() {
		super();
	}

	public RightJoinClause(Definable definable) throws APIError {
		super(definable);
	}

	public RightJoinClause(LogicalClause... clauses) {
		super(clauses);
	}
	
	public RightJoinClause(Definable definable, LogicalClause...clauses){
		super(definable, clauses);
	}

	@Override
	public String prefix() {
		return "RIGHT";
	}

}
