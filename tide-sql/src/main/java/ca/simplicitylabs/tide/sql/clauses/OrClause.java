package ca.simplicitylabs.tide.sql.clauses;
import static ca.simplicitylabs.tide.enumeration.LogicType.OR;

import ca.simplicitylabs.tide.definition.Definable;

public class OrClause extends LogicalClause {
	public OrClause(){
		super(OR);
	}
	
	public OrClause(Definable definable){
		super(OR);
		use(definable);
	}
	
	public OrClause(SQLClause ...clauses){
		super(OR);
		use(clauses);
	}
}
