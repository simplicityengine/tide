package ca.simplicitylabs.tide.sql.clauses;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.sql.SQLOption;

public class InsertClause extends SQLClause {
	public InsertClause(){
		
	}
	public InsertClause(Definable definable){
		use(definable);
	}
	
	public InsertClause(Definable definable, SQLOption option){
		use(definable);
		configure(option);
	}
	
	@Override
	public boolean valid(){
		Definable definable = this.getDefinable();
		boolean result = false;
		
		// TODO: make this check more robust (i.e. check fields)
		if(definable != null){
			result = true;
		}
		
		if(!result)
			result = foreignValues.size() > 0;
		
		return result;
	}
	
	@Override
	public String toString(){
		if(valid()){
			Definable definable = this.getDefinable();
			String table = definable.tableName();
			StringBuilder names = new StringBuilder();
			StringBuilder values = new StringBuilder();
			List<Field> fields = getFields();
			String delimiter = "";
			
			for(Field field : fields){
				// skip read-only fields
				if(definable.readonly(field))
					continue;
				
				String name = safeName(definable.annotatedName(field));
				String value = sqlValue(field);
				names.append(delimiter).append(name);
				values.append(delimiter).append(value);
				delimiter = ", ";
			}
			
			if(foreignValues.size() > 0){
				Set<String> keys = foreignValues.keySet();
				for(String key : keys){
					List<Definable> foreigns = getForeignValues(key);
					SQLOption option = getConfiguration(key);
					for(Definable foreign : foreigns){
						try{
							
							String name = safeName(foreign.columnLabel(key));
							Object value = foreign.getValue(key);
							
							if(value instanceof Definable)
								value = ((Definable) value).getId();
							
							// skip if value is a null, but not nullable
							if(value == null && !option.nullable())
								continue;
							
							value = sqlValue(key, value);
							
							names.append(delimiter).append(name);
							values.append(delimiter).append(value);
							delimiter = ", ";
						}
						catch(Exception ex){
							warn(ex);
						}
					}
				}
			}
			
			// TODO: add map
			
			return String.format("INSERT INTO %s ( %s ) VALUES ( %s )", table, names, values);
		}
		return "";
	}
	
	
}
