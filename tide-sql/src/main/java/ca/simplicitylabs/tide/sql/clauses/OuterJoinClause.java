package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public class OuterJoinClause extends JoinClause {

	public OuterJoinClause() {
		super();
	}

	public OuterJoinClause(Definable definable) throws APIError {
		super(definable);
	}

	public OuterJoinClause(LogicalClause... clauses) {
		super(clauses);
	}
	
	public OuterJoinClause(Definable definable, LogicalClause...clauses){
		super(definable, clauses);
	}

	@Override
	public String prefix() {
		return "OUTER";
	}

}
