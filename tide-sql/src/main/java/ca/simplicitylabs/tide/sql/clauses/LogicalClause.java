package ca.simplicitylabs.tide.sql.clauses;

import static ca.simplicitylabs.tide.enumeration.LogicType.*;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.enumeration.ComparisonType;
import ca.simplicitylabs.tide.enumeration.LogicType;
import ca.simplicitylabs.tide.sql.SQLOption;

public class LogicalClause extends SQLClause {
	private LogicType type = AND;
	
	public LogicalClause(){
		super();
	}
	
	public LogicalClause(LogicType type){
		super();
		this.type = type;
	}
	
	public void use(Definable definable){
		super.use(definable);
	}
	
	@Override
	public String toString(){
		if(!valid())
			return "";
		
		StringBuilder sb = new StringBuilder();
		Definable definable = getDefinable();
		String delimiter = "";
		
		if(definable != null){
			String template = "%s %s %s";
			List<Field> fields = getFields();
			for(int i = 0; i < fields.size(); i++){
				Field field = fields.get(i);
				SQLOption option = getConfiguration(field);
				String name = getName(field, definable);
				
				ComparisonType compare = option.compare();
				
				// special case for null values
				Object value = definable.getValue(field);
				if(value == null){
					switch(compare){
						case EQ: compare = ComparisonType.IS; break;
						default: compare = ComparisonType.ISNOT; break;
					}
				}
				
				//String sqlvalue = (option.isLink() ? getName(field, definable): sqlValue(field, true));
				String sqlvalue = sqlValue(field, true);
				
				
				sb.append(delimiter);
				sb.append(String.format(template, name, compare, sqlvalue));
				delimiter = String.format(" %s ", type);
			}
		}
		
		if(clauses.size() > 0){
			String template = "( %s )";
			for(int i = 0; i < clauses.size(); i++){
				SQLClause clause = clauses.get(i);
				if(clause.valid()){
					sb.append(delimiter);
					sb.append(String.format(template, clause));
					delimiter = String.format(" %s ", type);
				}
			}
		}
		
		if(foreignValues.size() > 0 && definable != null){
			String template = "%s %s %s";
			Set<String> keys = foreignValues.keySet();
			for(String key : keys){
				List<Definable> foreigns = getForeignValues(key);
				SQLOption option = getConfiguration(key);
				ComparisonType compare = option.compare();				
				
				for(Definable foreign : foreigns){
					String table = definable.tableName();
					String label = foreign.columnLabel(key);
					String left = String.format("%s.%s", table, label);
					Object right = foreign.getValue(key);
					if(right instanceof Definable)
						right = ((Definable) right).getId();
					
					// skip if value is a null, but not nullable
					if(right == null && !option.nullable())
						continue;
					
					right = sqlValue(key, right);
					
					// special case for null values
					if(right == null){
						switch(compare){
							case EQ: compare = ComparisonType.IS; break;
							default: compare = ComparisonType.ISNOT; break;
						}
					}
					
					sb.append(delimiter);
					sb.append(String.format(template, left, compare, right));
					delimiter = String.format(" %s ", type);
				}
				
			}
		}
		
		if(foreignKeys.size() > 0 && definable != null ){
			String template = "%s %s %s";
			Set<String> keys = foreignKeys.keySet();
			for(String key : keys){
				List<Definable> foreigns = getForeignKeys(key);
				SQLOption option = getConfiguration(key);
				ComparisonType compare = option.compare();
				for(Definable foreign : foreigns){
					String left = foreign.columnName(key);
					String right = String.format("%s.%s", definable.tableName(), foreign.columnLabel(key));
					sb.append(delimiter);
					sb.append(String.format(template, left, compare, right));
					delimiter = String.format(" %s ", type);
				}
				
			}
		}
		
		if(associations.size() > 0){
			String template = "%s %s %s";
			Set<String> keys = associations.keySet();
			for(String left : keys){
				SQLOption option = getConfiguration(left);
				ComparisonType compare = option.compare();
				String right = associations.get(left);
				sb.append(delimiter);
				sb.append(String.format(template, left, compare, right));
				delimiter = String.format(" %s ", type);
			}
		}
		
		// TODO: normalize with associations above
		if(map.size() > 0){
			String template = "%s %s %s";
			Set<String> keys = map.keySet();
			
			
			for(String left : keys){
				SQLOption option = getConfiguration(left);
				ComparisonType compare = option.compare();
				Object right = map.get(left);
				if(right == null){
					switch(compare){
						case EQ: compare = ComparisonType.IS; break;
						default: compare = ComparisonType.ISNOT; break;
					}
					right = "NULL";
				}
				else if (AppUtils.isString(right)){
					right = ((String) right).replace("'", "''");
					right = String.format("'%s'", right);
				}
				right = sqlValue(left, right);
				sb.append(delimiter);
				sb.append(String.format(template, left, compare, right));
				delimiter = String.format(" %s ", type);
			}
		}
		
		
		return sb.toString();
	}
	
	@Override
	public boolean valid(){
		// TODO: handle case with Map only
		Definable definition = getDefinable();

		if(definition != null){
			if(!getFields().isEmpty())
				return true;
			
			if(!foreignValues.isEmpty())
				return true;
			
			if(!foreignKeys.isEmpty())
				return true;
		}
		
		
		
		if(!map.isEmpty())
			return true;
		
		if(!associations.isEmpty())
			return true;
		
		for(SQLClause clause : clauses){
			if(clause.valid())
				return true;
		}
		
		
		
		return false;
	}
}
