package ca.simplicitylabs.tide.sql;

import ca.simplicitylabs.tide.historization.SelfLogger;

public class SQLResponse extends SelfLogger {
	private int count = 0;
	private Long increment = null;
	private boolean successful = false;
	
	public SQLResponse(){
		
	}
	
	public SQLResponse(int count, boolean successful){
		this.count = count;
		this.successful = successful;
	}
	
	public int getCount(){
		return count;
	}
	
	public boolean isSuccessful(){
		return successful;
	}
	
	public Long id(){
		return increment;
	}
	
	public void id(long value){
		this.increment = value;
		if(value <= 0)
			successful = false;
	}
}
