package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public class LeftJoinClause extends JoinClause {

	public LeftJoinClause() {
		super();
	}

	public LeftJoinClause(Definable definable) throws APIError {
		super(definable);
	}

	public LeftJoinClause(LogicalClause... clauses) {
		super(clauses);
	}
	
	public LeftJoinClause(Definable definable, LogicalClause...clauses){
		super(definable, clauses);
	}

	@Override
	public String prefix() {
		return "LEFT";
	}

}
