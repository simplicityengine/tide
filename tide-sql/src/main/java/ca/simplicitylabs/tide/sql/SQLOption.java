package ca.simplicitylabs.tide.sql;

import static ca.simplicitylabs.tide.enumeration.ComparisonType.*;

import ca.simplicitylabs.tide.enumeration.ComparisonType;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class SQLOption extends SelfLogger {
	private boolean annul= false;
	private boolean empty = false;
	private boolean enumerate = false;
	private boolean link = false;
	private boolean epoch = false;
	private ComparisonType comparison = EQ;
	private String timezone;
	private String format;
	
	private boolean md5 = false;
	
	public SQLOption enumerable(boolean value){
		enumerate = value;
		return this;
	}
	
	public boolean enumerable(){
		return enumerate;
	}
	
	public SQLOption voidable(boolean value){
		empty = value;
		return this;
	}
	
	public boolean voidable(){
		return empty;
	}
	
	public SQLOption nullable(boolean value){
		annul = value;
		return this;
	}
	
	public boolean nullable(){
		return annul;
	}
	
	public SQLOption linkable(boolean value){
		link = value;
		return this;
	}
	
	public boolean linkable(){
		return link;
	}
	
	public boolean epoch(){
		return epoch;
	}
	
	public void epoch(boolean value){
		this.epoch = value;
	}
	
	public SQLOption compare(ComparisonType type){
		comparison = type;
		return this;
	}
	
	public ComparisonType compare(){
		return comparison;
	}
	
	public boolean timezoned(){
		return timezone != null;
	}
	
	public String timezone(){
		return timezone;
	}
	
	public SQLOption timezone(String timezone){
		this.timezone = timezone;
		return this;
	}
	
	public boolean formatted(){
		return format != null;
	}
	
	public String format(){
		return format;
	}
	
	public SQLOption format(String format){
		this.format = format;
		return this;
	}
	
	public boolean md5(){
		return md5;
	}
	
	public SQLOption md5(boolean value){
		this.md5=value;
		return this;
	}
	
	public void clear(){
		annul = false;
		empty = false;
		enumerate = false;
	}
}
