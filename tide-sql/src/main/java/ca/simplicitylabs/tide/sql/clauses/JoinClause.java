package ca.simplicitylabs.tide.sql.clauses;

import java.util.ArrayList;
import java.util.List;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;

public abstract class JoinClause extends SQLClause {

	public JoinClause() {
	}

	public JoinClause(Definable definable) throws APIError {
		use(definable);
		use(new AndClause(definable));
	}
	
	public JoinClause(LogicalClause ...clauses) {
		if(clauses.length == 1){
			use(clauses[0]);
		}
		else if(clauses.length > 1){
			AndClause and = new AndClause();
			for(LogicalClause clause : clauses){
				and.use(clause);
			}
			use(and);
		}
		
	}
	public JoinClause(Definable definable, LogicalClause...clauses){
		this(clauses);
		use(definable);
	}
	
	public abstract String prefix();
	
	@Override
	public void use(SQLClause... clauses){
		SQLClause logical = null;
		if(!this.clauses.isEmpty())
			logical = this.clauses.get(0);
		
		if(clauses.length == 1 && logical == null){
			SQLClause clause = clauses[0];
			if(clause instanceof LogicalClause)
				super.use(clause);
		}
		
		else if(clauses.length > 0 && logical != null){
			for(SQLClause clause : clauses){
				if(clause instanceof LogicalClause)
					logical.use(clause);
			}
		}
		
		
	}
	
	@Override
	public void use(Definable... definables){
		if(definables.length > 0){
			Definable definable = definables[0];
			this.definables.clear();
			super.use(definable);
		}
	}
	
	@Override
	public void foreignValue(String key, Definable definable){
		for(SQLClause clause : clauses){
			clause.foreignValue(key, definable);
			break;
		}
	}
	
	@Override
	public void foreignValue(Definable definable){
		for(SQLClause clause : clauses){
			clause.foreignValue(definable);
			break;
		}
	}
	
	@Override
	public List<Definable> getForeignKeys(String key){
		for(SQLClause clause : clauses){
			return clause.getForeignKeys(key);
		}
		return new ArrayList<Definable>();
	}
	
	@Override
	public void foreignKey(String key, Class<? extends Definable> type) throws APIError{
		for(SQLClause clause : clauses){
			clause.foreignKey(key, type);
			break;
		}
	}
	
	@Override
	public void foreignKey(String key, Definable definable){
		for(SQLClause clause : clauses){
			clause.foreignKey(key, definable);
			break;
		}
	}
	
	@Override
	public void foreignKey(Definable definable){
		for(SQLClause clause : clauses){
			clause.foreignKey(definable);
			break;
		}
	}
	
	public boolean match(JoinClause clause){
		try{
			Definable referece = clause.getDefinable();
			Definable definable = getDefinable();
			
			return definable.getClass().equals(referece.getClass());
		}
		catch(Exception ex){
			return false;
		}
	
	}
	
	public boolean match(Definable referece){
		try{
			Definable definable = getDefinable();
			
			return definable.getClass().equals(referece.getClass());
		}
		catch(Exception ex){
			return false;
		}
	
	}
	
	public String toString(){
		//TODO: make implementation more robust
		StringBuilder sb = new StringBuilder();
		if(valid()){
			Definable definable = getDefinable();
			SQLClause clause = clauses.get(0);
			sb.append(String.format("%s JOIN %s ON ", prefix().toUpperCase(), definable.tableLabel()));
			sb.append(clause);
		}
		return sb.toString();
	}
	
	public boolean valid(){
		// TODO: check validity of every clause
		if(definables.isEmpty())
			return false;
		
		if(clauses.isEmpty())
			return false;
		
		return true;
	}

}
