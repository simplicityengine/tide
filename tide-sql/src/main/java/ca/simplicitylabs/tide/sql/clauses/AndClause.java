package ca.simplicitylabs.tide.sql.clauses;

import static ca.simplicitylabs.tide.enumeration.LogicType.AND;

import ca.simplicitylabs.tide.definition.Definable;

public class AndClause extends LogicalClause {
	public AndClause(){
		super(AND);
	}
	
	public AndClause(Definable definable){
		super(AND);
		use(definable);
	}
	
	public AndClause(SQLClause ...clauses){
		super(AND);
		use(clauses);
	}
}
