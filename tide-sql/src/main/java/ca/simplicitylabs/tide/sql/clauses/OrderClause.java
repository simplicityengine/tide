package ca.simplicitylabs.tide.sql.clauses;

import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.sql.SQLOption;
import ca.simplicitylabs.tide.enumeration.SortType;
import static ca.simplicitylabs.tide.enumeration.SortType.ASCENDING;
import static ca.simplicitylabs.tide.enumeration.SortType.DESCENDING;

import java.util.Set;
public class OrderClause extends SQLClause {

	public OrderClause() {
		super();
	}

	public OrderClause(Definable definable) {
		super(definable);
	}

	public OrderClause(Definable definable, SQLOption option) {
		super(definable, option);
	}
	
	public void ascend(String key, Definable definable){
		String name = definable.columnName(key);
		avp(name, ASCENDING);
	}
	
	public void descend(String key, Definable definable){
		String name = definable.columnName(key);
		avp(name, DESCENDING);
	}
	
	@Override
	public boolean valid(){
		return !map.isEmpty();
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		if(!valid())
			return sb.toString();
		
		sb.append("ORDER BY ");
		
		Set<String> keys = map.keySet();
		String d = "";
		for(String key : keys){
			Object object = avp(key);
			String value = sorting(object);
			sb.append(d).append(key).append(" ").append(value);
			d = ", ";
		}
		
		return sb.toString();
	}
	
	private String sorting(Object value){
		if(value instanceof String){
			return (String) value;
		}
		
		else if(value instanceof SortType){
			SortType type = (SortType) value;
			switch(type){
				case ASCENDING: return "ASC";
				case DESCENDING: return "DESC";
			}
		}
		
		return "ASC";
	}
}
