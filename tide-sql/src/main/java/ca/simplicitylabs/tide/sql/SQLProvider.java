package ca.simplicitylabs.tide.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.configuration.SQLConfig;
import ca.simplicitylabs.tide.configuration.SQLPool;
import ca.simplicitylabs.tide.configuration.SQLTiming;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.sql.clauses.AlterTableClause;
import ca.simplicitylabs.tide.sql.clauses.DeleteClause;
import ca.simplicitylabs.tide.sql.clauses.InsertClause;
import ca.simplicitylabs.tide.sql.clauses.SelectClause;
import ca.simplicitylabs.tide.sql.clauses.UpdateClause;

public class SQLProvider extends SelfLogger {
	private ComboPooledDataSource source;
	private String driver;
	private String url;
	private String host;
	private Integer port;
	private String name;
	private String username;
	private String password;
	private Boolean ssl = true;
	private Boolean checkCertificate = false;
	// TODO: make configurable
	private int fetchSize = 4000;
	private boolean transparent = false;
	
	public SQLProvider(SQLConfig config) throws APIError{
		try{
			driver = config.driver();
			url = config.url();
			host = config.host();
			port = config.port();
			name = config.database();
			username = config.username();
			password = config.password();
			transparent = config.transparent();
			SQLPool pool = config.pool();
			SQLTiming timing = config.timing();
			
			source = new ComboPooledDataSource();
			source.setDriverClass(driver);
			source.setJdbcUrl(url());
			source.setUser(username);
			source.setPassword(password);	
			
			source.setMinPoolSize(pool.minimum());			
			source.setMaxPoolSize(pool.maximum());
			source.setInitialPoolSize(pool.initial());
			source.setAcquireIncrement(pool.increment());
			source.setIdleConnectionTestPeriod(timing.test());
			source.setMaxIdleTime(timing.idle());
			source.setMaxIdleTimeExcessConnections(timing.excess());
			source.setCheckoutTimeout(timing.checkout());
			source.setUnreturnedConnectionTimeout(timing.checkin());
			source.setLoginTimeout(timing.login());
		
		}
		catch (Exception e) {
			error(e);
			throw new Internal("An error was encounterd by the SQLProvider.  See logs for details");
		}
			
	}
	
	public SQLConnector connector() throws APIError {
		SQLConnector connector = new SQLConnector(connection());
		connector.setFetchSize(fetchSize);
		connector.setTransparency(transparent);
		return connector;
	}
	
	private Connection connection() throws APIError{
		try {
			return source.getConnection();
		} catch (SQLException e) {
			error(e);
			throw new Internal ("SQL Connection Error");
		}
	}
	
	private String url(){
		if(url != null){
			if(!url.trim().isEmpty())
				return url;
		}
		StringBuilder sb = new StringBuilder();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("useSSL", ssl);
		map.put("verifyServerCertificate", checkCertificate);
		sb.append("jdbc:mysql://%s:%d/%s");
		String delimiter = "?";
		
		Set<String> keys = map.keySet();
		for(String key : keys){
			Object value = map.get(key);
			sb.append(delimiter);
			sb.append(String.format("%s=%s", key, value));
			delimiter = "&";
		}
		
		return String.format(sb.toString(), host, port, name);
	}
	
	public int allConnections() throws APIError{
		try {
			return source.getNumConnections();
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public int idleConnections() throws APIError{
		try {
			return source.getNumIdleConnections();
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public int orphanedConnections() throws APIError{
		try {
			return source.getNumUnclosedOrphanedConnections();
		} catch (SQLException e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public int fetchSize(){
		return fetchSize;
	}
	
	public void fetchSize(int size){
		this.fetchSize = size;
	}
}
