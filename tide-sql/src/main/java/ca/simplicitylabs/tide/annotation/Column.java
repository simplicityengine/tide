package ca.simplicitylabs.tide.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
	String value() default "";
	boolean ignore() default false;
	/**
	 * Indicates that the column is an external reference to another table
	 * @return
	 */
	boolean external() default false;
	
	/**
	 * indicates that this column cannot be updated
	 * @return
	 */
	boolean readonly() default false;
	
	/**
	 * Used in tables that represent dual cardinalities of objects (plural and singular forms).<br/>
	 * When set to true, the corresponding row (object) will be considered the singular.<br/>
	 * When set to false, the corresponding rows (objects) will be considered plural.
	 * @return
	 */
	boolean principal() default false;
	
	// date & time methods
	/**
	 * Date formats
	 * @return
	 */
	String [] formats() default {};
	String timezone() default "";
	boolean epoch() default false;
	
	// json methods
	boolean toJson() default true;
	boolean fromJson() default true;
	boolean epochToJson() default false;
	boolean epochFromJson() default false;
	
	// hashing methods
	boolean md5() default false;
	
	// support to queries
	boolean expand() default true;
}
