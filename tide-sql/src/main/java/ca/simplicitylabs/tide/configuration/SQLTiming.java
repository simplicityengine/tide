package ca.simplicitylabs.tide.configuration;

public class SQLTiming {
	private int idle = 10;
	private int excess = 5;
	private int test = 1;
	private int login = 0;
	private int checkout = 0;
	private int checkin = 0;
	
	public SQLTiming() {
		// TODO Auto-generated constructor stub
	}
	
	public int idle(){
		return idle;
	}
	
	public int excess(){
		return excess;
	}
	
	public int test(){
		return test;
	}
	
	public int login(){
		return login;
	}
	
	public int checkout(){
		return checkout;
	}
	
	public int checkin(){
		return checkin;
	}
}
