package ca.simplicitylabs.tide.configuration;
import ca.simplicitylabs.tide.annotation.Reference;

@Reference("databases")
public class SQLConfig extends Config {
	private String driver = "com.mysql.jdbc.Driver";
	private String url;
	private String host = "127.0.0.1";
	private Integer port = 3306;
	private String database;
	private String username;
	private String password;
	private Boolean transparent = false;
	private SQLPool pool = new SQLPool();
	private SQLTiming timing = new SQLTiming();
	
	public SQLConfig() {
		super();
	}
	
	public String driver(){
		return driver;
	}
	
	public String url(){
		return url;
	}
	
	public String host(){
		return host;
	}
	
	public Integer port(){
		return port;
	}
	
	public String database(){
		return database;
	}
	
	public String username(){
		return username;
	}
	
	public String password(){
		return password;
	}
	
	public boolean transparent(){
		return transparent;
	}
	
	public SQLPool pool(){
		return pool;
	}
	
	public SQLTiming timing(){
		return timing;
	}
}
