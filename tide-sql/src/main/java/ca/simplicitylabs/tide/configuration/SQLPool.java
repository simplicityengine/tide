package ca.simplicitylabs.tide.configuration;

public class SQLPool {
	private Integer minimum = 0;
	private Integer maximim = 10;
	private Integer initial = 5;
	private Integer increment = 1;
	public SQLPool() {
		// TODO Auto-generated constructor stub
	}
	public Integer minimum() {
		return minimum.intValue();
	}
	public Integer maximum() {
		return maximim.intValue();
	}
	public Integer initial(){
		return initial.intValue();
	}
	public Integer increment(){
		return increment.intValue();
	}
	
	public String toString(){
		return String.format("DBPool[ minimum: %s, maximum: %s, initial: %s, increment: %s]", minimum(), maximum(), initial(), increment());
	}

}
