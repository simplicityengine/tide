package ca.simplicitylabs.tide.definition;

import static ca.simplicitylabs.tide.enumeration.SerialFormat.SQL;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.annotation.Ignore;
import ca.simplicitylabs.tide.annotation.Sequence;
import ca.simplicitylabs.tide.annotation.Table;
import ca.simplicitylabs.tide.enumeration.APIMethod;
import ca.simplicitylabs.tide.enumeration.SerialFormat;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.serialization.SQLSerializer;
import ca.simplicitylabs.tide.serialization.Serial;

public abstract class Definable extends SelfLogger implements Serial {
	@Sequence(0)
	protected Long id;
	protected transient boolean rendered = false;
	protected transient boolean read_only = false;
	protected transient String alias = "";
	protected transient String label = "";
	
	private transient static GsonBuilder builder = new GsonBuilder().setPrettyPrinting().serializeNulls().registerTypeHierarchyAdapter(Definable.class, new SQLSerializer());;
	private transient static Gson gson = builder.create();
	
	public Definable(){
		
	}
	
	public Definable(Long id){
		setId(id);
	}
	
	public boolean hasId(){
		return (id != null && id > 0L);
	}
	
	
	public boolean isId(Definable definable){
		return isId(definable.getId());
	}
	
	public boolean isId(Integer id){
		return isId(AppUtils.toLong(id));
	}
	
	public boolean isId(Long id){
		if(id == null || this.id == null)
			return false;
		
		return this.id.equals(id);
	}
	
	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	
	
	public void render(){
		rendered = true;
	}
	public void unrender(){
		rendered = false;
	}
	
	public boolean isRendered(){
		return rendered;
	}
	
	public String label(){
		return label;
	}
	
	public void label(String label){
		if(label == null)
			this.label = "";
		else
			this.label = label;
	}
	
	public boolean labeled(){
		if(label != null)
			return !label.trim().isEmpty();
		
		return false;
	}
	
	public String alias(){
		return alias;
	}
	
	public void alias(String value){
		if(value == null)
			this.alias = "";
		else
			this.alias = value;
	}
	
	public boolean aliased(){
		if(alias != null)
			return !alias.trim().isEmpty();
		
		return false;
	}
	
	@Override
	public <T> void serialized(SerialFormat type, T raw){

	}
	
	@Override
	public <T> void deserialized(SerialFormat type, T raw){
		
	}
	
	public void validate(APIMethod method ) throws APIError{
		
	}
	
	public Definable principalSignature(String... names) throws APIError{
		Definable definable = signature(names);
		Field principal = principalField();
		
		if(principal != null){
			try{
				Object value = principal.get(this);
				principal.set(definable, value);
				
			}
			catch(Exception ex){
				throw AppUtils.normalize(ex);
			}
		}
		return definable;
	}
	
	public Definable principalSignature(boolean value, String... names) throws APIError{
		Definable definable = signature(names);
		definable.principal(value);
		return definable;
	}
	
	public Definable signature(String... names) throws APIError {
		try {
			Definable definable = getClass().newInstance();
			definable.alias(alias());
			definable.label(label());
			for(String name : names){
				try{
					Field field = getField(name);
					field.setAccessible(true);
					Object value = field.get(this);
					field.set(definable, value);
				}
				catch(Exception ex){
					trace(ex);
				}
			}
				
			return definable;
		} catch (Exception e) {
			error(e);
		}
		
		return null;
	}
	
	public boolean hasValue(){
		List<Field> fields = getFields();
		
		for(Field field : fields){
			Object value = getValue(field);
			if(value != null)
				return true;
				
		}
		
		return false;
	}
	
	// TODO: support XML and YAML representations
	@Override
	public String toString(){
		return gson.toJson(this);
	}
	
	
	public void load(ResultSet rs){
		// TODO:  column names
		List<Field> fields = getFields();
		for(Field field : fields){
			try {
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					if(column.ignore())
						continue;
				}
				
				Class<?> type = field.getType();
				String single = AppUtils.singular(tableName());
				
				String key = field.getName();
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					String colName = column.value();
					if(colName != null){
						if(!colName.trim().isEmpty())
							key = colName;
					}
				}
				
				String label = String.format("%s_%s", single, key);

				if (AppUtils.isPrimitive(type)){
					primeResult(field, rs, label);
				}

				else if (Definable.class.isAssignableFrom(type)) {
					Definable definable = (Definable) type.newInstance();
									
					definable.load(rs);
					if( definable.hasId() ){
						field.set(this, definable);
					}
					else{
						label = String.format("%s_id", label);
						long id = rs.getLong(label);
						
						if( id > 0){
							definable.setId(id);
							field.set(this, definable);
						}	
					}					
				}
			} catch (Exception ex) {
				trace(ex);
			}
		
		}
		deserialized(SQL, rs);
	}
	
	public void expand(ResultSet rs){
		List<Field> fields = getFields();
		for(Field field : fields){
			try {
				String key = field.getName();
				boolean expand = true;
				boolean ignore = false;
				boolean external = false;
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					expand = column.expand();
					ignore = column.ignore();
					external = column.external();
					if(!column.value().trim().isEmpty())
						key = column.value();
				}
				
				if(ignore)
					continue;
				
				Class<?> type = field.getType();
				String single = AppUtils.singular(tableName());
				
				
				String label = String.format("%s_%s", single, key);

				if (AppUtils.isPrimitive(type)){
					primeResult(field, rs, label);
				}

				else if (Definable.class.isAssignableFrom(type)) {
					Definable child = (Definable) type.newInstance();
					label = String.format("%s_%s", single, AppUtils.plural(key));
					child.label(label);
					
					child.expand(rs);
					if( child.hasId() ){
						field.set(this, child);
					}
					else{
						label = String.format("%s_id", label);
						long id = rs.getLong(label);
						
						if( id > 0){
							child.setId(id);
							field.set(this, child);
						}	
					}					
				}
			} catch (Exception ex) {
				trace(ex);
			}
		
		}
		deserialized(SQL, rs);
	}
	
	private void primeResult(Field field, ResultSet rs, String... labels) throws IllegalArgumentException, IllegalAccessException, SQLException{
		ResultSetMetaData meta = rs.getMetaData();
		int size = meta.getColumnCount();
		
		if(labels.length > 1){
			for(String label : labels){
				for(int i = 1; i <= size; i++){
					if(label.equalsIgnoreCase(meta.getColumnLabel(i))){
						if(applyResult(field, rs, label))
							return;
					}
				}
			}
		}
		else if(labels.length == 1){
			applyResult(field, rs, labels[0]);
		}
		
				
	}
	
	private boolean applyResult(Field field, ResultSet rs, String label) throws IllegalArgumentException, IllegalAccessException, SQLException{
		Class<?> type = field.getType();
		if (AppUtils.isString(type)){
			field.set(this, rs.getString(label));
			return true;
		}
		else if (AppUtils.isLong(type)){
			long value = rs.getLong(label);
			field.set(this, (rs.wasNull() ? null : value) );				
			return true;
		}
		else if (AppUtils.isInteger(type)){
			int value = rs.getInt(label);
			field.set(this, (rs.wasNull() ? null : value) );				
			return true;
		}
		else if(AppUtils.isDouble(type)){
			Double value =  rs.getDouble(label);
			field.set(this, (rs.wasNull() ? null : value) );
			return true;
		}
		else if(AppUtils.isBoolean(type)){
			Boolean value = rs.getBoolean(label);
			field.set(this, (rs.wasNull() ? null : value) );			
			return true;
		}
		else if(AppUtils.isDate(type)){
			Timestamp time = rs.getTimestamp(label);
			Date date = null;
			if(time != null)
				date = new Date(time.getTime());
			
			field.set(this, date);
			return true;
		}
		
		return false;
	}
	
	public Object getValue(String key){
		try {
			Field field = getField(key);
			return getValue(field);
		} catch (NoSuchFieldException | SecurityException e) {
			warn(e);
			return null;
		}
		
	}
	
	public Object getValue(Field field){
		try {
			return field.get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			warn(e);
			return null;
		}
	}
	
	public List<Field> getFields(){
		List<Field> list = new ArrayList<Field>();
		getFields(getClass(), list);
		return list;
	}
	
	public Field getField(String key) throws NoSuchFieldException {
		List<Field> fields = getFields();
		for(Field field : fields){
			String name = field.getName();
			String columnName = "";
			if(field.isAnnotationPresent(Column.class)){
				Column column = field.getAnnotation(Column.class);
				columnName = column.value();
			}
			// TODO: do not condider columnName if it is empty string
			if(name.equalsIgnoreCase(key) || columnName.equalsIgnoreCase(key))
				return field;
		}
		throw new NoSuchFieldException();
	}
	
	public List<Field> getColumns(){
		return getColumns(false);
	}
	public List<Field> getColumns(boolean nillable){
		List<Field> list = getFields();
		int size = list.size();
		for(int i = size - 1; i >= 0; i--){
			try {
				Field field = list.get(i);
				Object object = field.get(this);
				Class<?> type = field.getType();
				if(object == null && !nillable){
					list.remove(i);
				}
				else if(Iterable.class.isAssignableFrom(type)){
					list.remove(i);
				}
				else if(field.isAnnotationPresent(Ignore.class))
					list.remove(i);
				
				else if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					if(column.ignore() || column.external())
						list.remove(i);
				}
				
				
				else if(!AppUtils.isPrimitive(type) && !Definable.class.isAssignableFrom(type))
					list.remove(i);
				
			} catch (IllegalArgumentException | IllegalAccessException e) {
				error(e);
			}
			
		}
		return list;
	}
	
	public String annotatedName(String key){
		List<Field> fields = getFields();
		
		for(Field field : fields){
			String name = field.getName();
			if(name.matches(key))
				return annotatedName(field);
		}
		
		return key;
	}
	
	public String annotatedName(Field field){
		String name = field.getName();
		Class<?> type = field.getType();
		if(field.isAnnotationPresent(Column.class)){
			Column column = field.getAnnotation(Column.class);
			String value = column.value();
			if(value != null){
				if(!value.trim().isEmpty())
					return value;
			}
		}
		
		if(Definable.class.isAssignableFrom(type)){
			name = String.format("%s_id", name);
		}
		
		return name;
	}
	
	public String columnName(Field field){
		String name = annotatedName(field);
		return String.format("%s.%s", tableName(), name);
	}
	
	public String columnName(String key){
		String name = annotatedName(key);
		return String.format("%s.%s", tableName(), name);
	}
	
	public String columnLabel(Field field){
		String name = annotatedName(field);
		return String.format("%s_%s", AppUtils.singular(table()), name);
	}
	
	public String columnLabel(String key){
		String name = annotatedName(key);
		return String.format("%s_%s", AppUtils.singular(table()), name);
	}
	
	public String simpleName(){
		return getClass().getSimpleName();
	}
	
	public String table(){
		Class<?> cls = getClass();
		if(cls.isAnnotationPresent(Table.class)){
			Table table = cls.getAnnotation(Table.class);
			if(!table.value().trim().isEmpty()){
				return table.value().toLowerCase().trim();
			}
		}
		
		return plural();
	}
	
	public String tableName(){
		if(labeled())
			return label();
		
		if(aliased())
			return alias();
		
		return table();
	}
	
	public String tableLabel(){
		if(labeled()){
			return String.format("%s `%s`", table(), label());
		}
		
		return tableName();
	}
	
	public boolean hasPrimitives(String... ignores){
		List<Field> fields = getFields();
		if(ignores == null)
			ignores = new String[]{};
		
		for(Field field : fields){
			try {
				Object value = field.get(this);
				String name = field.getName();
				
				if(AppUtils.isPrimitive(value)){
					boolean found = false;
					for(String ignore : ignores){
						if(name.equals(ignore))
							found = true;
					}
					if(!found)
						return true;
				}
					
				
			} catch (IllegalArgumentException | IllegalAccessException e) {
				error(e);
			}
		}
		
		return false;
	}
	
	public boolean hasColumns(String... ignores){
		List<Field> fields = getColumns(false);
		if(ignores == null)
			ignores = new String[]{};
		
		for(Field field : fields){
			try {
				String name = field.getName();
				String anno = annotatedName(field);
				
				boolean found = false;
				for(String ignore : ignores){
					if(name.equalsIgnoreCase(ignore) || anno.equalsIgnoreCase(ignore)){
						found = true;
						break;
					}
				}
				if(!found)
					return true;
					
				
			} catch (Exception e) {
				error(e);
			}
		}
		
		return false;
	}
	
	public boolean isNull(Field field){
		try {
			Object value = field.get(this);
			return value == null;
		} 
		catch (IllegalArgumentException | IllegalAccessException e) {
			warn(e);
			return false;
		}
	}
	
	public String plural(){
		String name = AppUtils.plural(singular());		
		return name;
	}
	
	public String singular(){
		Class<?> cls = getClass();
		String name = cls.getSimpleName().toLowerCase().trim();		
		return name;
	}

	private static void getFields(Class<?> cls, List<Field> list){
		AppUtils.fields(cls, list, Definable.class);
	}
	
	private Field principalField(){
		List<Field> fields = getFields();
		for(Field field: fields){
			try{
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					Class<?> type = field.getType();
					if(column.principal() && AppUtils.isBoolean(type)){
						return  field;
					}
				}
			}
			catch(Exception ex){
				
			}
		}
		return null;
	}
	
	public boolean principal(){
		try{
			Field field = principalField();
			if(field != null){
				return (Boolean) field.get(this);
			}
		}
		catch(Exception ex){
			warn(ex);
		}
		return false;
	}
	public void principal(boolean value){
		List<Field> fields = getFields();
		for(Field field: fields){
			try{
				if(field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					Class<?> type = field.getType();
					if(column.principal() && AppUtils.isBoolean(type)){
						field.set(this, value);
						break;
					}
				}
			}
			catch(Exception ex){
				
			}
		}
	}
	
	public boolean readonly(){
		return read_only;
	}
	public void readonly(boolean value){
		this.read_only = value;
	}
	public boolean readonly(String key){
		try {
			Field field = getField(key);
			return readonly(field);
		} catch (NoSuchFieldException e) {
			return false;
		}
		
	}
	public boolean readonly(Field field){
		if(field.isAnnotationPresent(Column.class)){
			Column column = field.getAnnotation(Column.class);
			return column.readonly();
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Definable> T external(Class<T> type) {
		List<Definable> list = externals();
		
		for(Definable item : list){
			if(type.equals(item.getClass()))
				return (T) item;
		}
		return null;
		
	}
	
	public List<Definable> externals(){
		return externals(false);
	}
	
	/**
	 * This method returns a List of SQLModel child objects that have been annotated with a Column that has external parameter set to true
	 * @param eject this parameter indicates if the identified SQLModel child objects should be removed from the SQLModel parent object
	 */
	public List<Definable> externals(boolean eject){
		List<Definable> list = new ArrayList<Definable>();
		List<Field> fields = getFields();
		
		for(Field field : fields){
			try{
				Class<?> type = field.getType();
				Object value = field.get(this);
				if(value != null && Definable.class.isAssignableFrom(type) && field.isAnnotationPresent(Column.class)){
					Column column = field.getAnnotation(Column.class);
					if(column.external() && !column.ignore()){
						Definable child = (Definable) value;
						child.principal(true);
						child.readonly(readonly(field));
						list.add(child);
						if(eject)
							field.set(this, null);
					}
				}
			}
			catch(Exception ex){
				
			}
		}
		return list;
	}
	public List<List<Definable>> lists(){
		return lists(false);
	}
	public List<List<Definable>> lists(boolean eject){
		List<List<Definable>> list = new ArrayList<List<Definable>>();
		List<Field> fields = getFields();
		
		for(Field field : fields){
			try{
				Class<?> type = field.getType();
				Object value = field.get(this);
				if(List.class.isAssignableFrom(type) && value != null){
					boolean ignore = false;
					ParameterizedType generic = (ParameterizedType) field.getGenericType();
					Class<?> genericType = (Class<?>) generic.getActualTypeArguments()[0];
					
					if(field.isAnnotationPresent(Column.class)){
						Column column = field.getAnnotation(Column.class);
						ignore = column.ignore();
					}
					
					if(Definable.class.isAssignableFrom(genericType) && !ignore){
						List<Definable> children = (List<Definable>) value;
						list.add(children);
						for(Definable child : children)
							child.principal(false);
						
						if(eject)
							field.set(this, null);
					}
				}
			}
			catch(Exception ex){
				
			}
		}
		return list;
	}
	
}
