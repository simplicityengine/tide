package ca.simplicitylabs.tide.definition;

import com.google.gson.JsonElement;

import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.annotation.Table;
import ca.simplicitylabs.tide.enumeration.SerialFormat;

@Table
public class County extends Geographical {
	@SuppressWarnings("unused")
	private Division division = new Division(this);
	private Country country;
	private Continent continent;
	
	@Column(value = "division_code", toJson = false)
	private String divisionCode;
	
	@Column(value = "division_name", toJson = false)
	private String divisionName;
	
	@Column(value = "division_friendly_name", toJson = false)
	private String divisionFriendly;

	public County(){
		super();
	}
	public County(Long id) {
		super(id);
	}

	public County(Long id, String name) {
		super(id, name);
	}
	
	public County(Long id, String name, String code) {
		super(id, name, code);
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	@Override
	public <JsonElement> void deserialized(SerialFormat type, JsonElement elem){
		switch(type){
		case JSON:
			division.push();
			break;
		case SQL:
			division.pull();
			break;
		default:
			break;
		}
		
	}
	
	@SuppressWarnings("unused")
	private static class Division{
		private String code;
		private String name;
		private String friendlyName;
		private transient County county;
		
		
		public Division(){
			
		}
		
		public Division(County county){
			this.county = county;
			setCode(county.getCode());
			
		}
		
		public void push(){
			county.setCode(code);
			county.setName(name);
			county.setFriendlyName(friendlyName);
		}
		
		public void pull(){
			setCode(county.code);
			setName(county.name);
			setFriendlyName(county.friendlyName);
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
			county.setCode(code);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
			county.setName(name);
		}

		public String getFriendlyName() {
			return friendlyName;
		}

		public void setFriendlyName(String friendlyName) {
			this.friendlyName = friendlyName;
			county.setFriendlyName(friendlyName);
		}
		
	}
}
