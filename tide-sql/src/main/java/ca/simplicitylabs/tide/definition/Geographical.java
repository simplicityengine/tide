package ca.simplicitylabs.tide.definition;

import java.lang.reflect.Field;
import java.util.List;

import ca.simplicitylabs.tide.annotation.Column;

public abstract class Geographical extends Enumerable {
	protected String code;
	
	@Column("friendly_name")
	protected String friendlyName;
	
	@Column(value = "geoname_id", toJson = false)
	protected Integer geoId;
	
	public Geographical() {
		// TODO Auto-generated constructor stub
	}

	public Geographical(Long id) {
		super(id);
	}

	public Geographical(Long id, String name) {
		super(id, name);
	}
	
	public Geographical(Long id, String name, String code) {
		super(id, name);
		setCode(code);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public Integer getGeoId() {
		return geoId;
	}

	public void setGeoId(Integer id) {
		this.geoId = id;
	}
	
	public void flatten(){
		flatten(this);
	}
	
	public static void flatten(Geographical geo){
		if(geo == null)
			return;
		
		List<Field> fields = geo.getFields();
		for(Field field : fields){
			try{
			Class<?> type = field.getType();
			
			if(Geographical.class.isAssignableFrom(type))
				field.set(geo, null);
			}
			catch( Exception ex){
				
			}
		}
	}

}
