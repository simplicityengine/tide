package ca.simplicitylabs.tide.definition;

import ca.simplicitylabs.tide.annotation.Sequence;

public abstract class Enumerable extends Definable {
	@Sequence
	protected String name;
	
	public Enumerable(){
		super();
	}
	
	public Enumerable(Long id){
		super(id);
	}
	
	public Enumerable(Long id, String name){
		super(id);
		setName(name);
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
}
