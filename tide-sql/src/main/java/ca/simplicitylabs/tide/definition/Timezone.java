package ca.simplicitylabs.tide.definition;

import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.annotation.Ignore;

public class Timezone extends Enumerable {
	@Ignore
	private Boolean successful;
	private String code;
	private String alternate;
	@Column("gmt_offset")
	private Double offset;
	
	@Column(expand=false)
	private Country country;
	
	@Column(expand=false)
	private Continent continent;
	
	
	public Timezone() {
		super();
	}

	public Timezone(Long id) {
		super(id);
	}

	public Timezone(Long id, String name) {
		super(id, name);
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAlternate() {
		return alternate;
	}

	public void setAlternate(String alternate) {
		this.alternate = alternate;
	}

	public Double getOffset() {
		return offset;
	}

	public void setOffset(Double offset) {
		this.offset = offset;
	}
	
	public void succeed(){
		successful = true;
	}
	
	public void fail(){
		successful = false;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	public void flatten(){
		Geographical[] geographicals = new Geographical[]{ country, continent};
		
		for(Geographical geo : geographicals){
			if(geo != null)
				geo.flatten();
		}
	}
}
