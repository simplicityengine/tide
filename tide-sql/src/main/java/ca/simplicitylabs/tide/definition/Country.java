package ca.simplicitylabs.tide.definition;

import ca.simplicitylabs.tide.annotation.Table;

@Table
public class Country extends Geographical {
	private Continent continent;
	public Country(){
		
	}
	public Country(Long id) {
		super(id);
	}

	public Country(Long id, String name) {
		super(id, name);
	}
	
	public Country(Long id, String name, String code) {
		super(id, name, code);
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}
}
