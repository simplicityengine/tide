package ca.simplicitylabs.tide.definition;

import ca.simplicitylabs.tide.annotation.Table;

@Table
public class Continent extends Geographical {
	public Continent(){
		
	}
	
	public Continent(Long id) {
		super(id);
	}

	public Continent(Long id, String name) {
		super(id, name);
	}
	
	public Continent(Long id, String name, String code) {
		super(id, name, code);
	}
}
