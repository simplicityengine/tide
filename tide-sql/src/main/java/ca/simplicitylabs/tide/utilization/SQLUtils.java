package ca.simplicitylabs.tide.utilization;

import ca.simplicitylabs.tide.historization.SelfLogger;

public class SQLUtils {
	private static SelfLogger log = new SelfLogger(SQLUtils.class);
	public SQLUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static String escape(String value){
		String regex = "(?<=[^\\']|^)\\'(?=[^\\']|$)";
		value = value.replaceAll(regex, "''");
		log.trace(value);
		return value;
	}

}
