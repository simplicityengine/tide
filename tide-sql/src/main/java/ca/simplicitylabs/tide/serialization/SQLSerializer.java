package ca.simplicitylabs.tide.serialization;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.TimeZone;

import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.enumeration.SerialType;

public class SQLSerializer extends GsonSerializer<Definable>{
	
	public SQLSerializer() {
		super(Definable.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean ok(Field field, Object value, boolean serialize){
		if(value == null){
			return allowNulls;
		}
		int mod = field.getModifiers();
		Column column = field.getAnnotation(Column.class);
		
		if(Modifier.isTransient(mod) || Modifier.isStatic(mod))
			return false;
		
		if(column != null){
			if(serialize)
				return column.toJson();
			else
				return column.fromJson();
		}
		
		return true;
		
	}
	
	@Override
	protected String format(Field field, SerialType type){
		if(field.isAnnotationPresent(Column.class)){
			Column column = field.getAnnotation(Column.class);
			String[] formats = column.formats();
			if(formats.length > 0)
				return formats[0];
		}
		return super.format(field, type);
	}
	
	@Override
	public boolean epoch(Field field, SerialType type){
		if(field.isAnnotationPresent(Column.class)){
			Column column = field.getAnnotation(Column.class);
			switch(type){
				case SERIALIZE: return column.epochToJson();
				case DESERIALIZE: return column.fromJson();
			}
		}
		return super.epoch(field, type);
	}
	@Override
	protected TimeZone timezone(Field field, SerialType type){
		//TODO: implement jsonTimezone annotation property.
		return super.timezone(field, type);
	}

	private boolean ok(String value) {
		return (value == null ? false : !value.trim().isEmpty());
	}
}
