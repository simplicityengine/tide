package ca.simplicitylabs.tide.orchestration;


import java.util.List;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.definition.Continent;
import ca.simplicitylabs.tide.definition.Country;
import ca.simplicitylabs.tide.definition.Geographical;
import ca.simplicitylabs.tide.definition.Timezone;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.sql.SQLConnector;
import ca.simplicitylabs.tide.sql.clauses.AndClause;
import ca.simplicitylabs.tide.sql.clauses.FromClause;
import ca.simplicitylabs.tide.sql.clauses.SelectClause;
import ca.simplicitylabs.tide.sql.clauses.WhereClause;

public abstract class TimezoneFlow extends SQLFlow {

	public TimezoneFlow() throws APIError {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void get() throws APIError {
		SQLConnector connector = null;
		try{
			connector = provider().connector();
			Timezone timezone = query(Timezone.class);
			Geographical geographical = geographical();
			
			AndClause andTimezone = new AndClause(timezone);
			AndClause andCountry = new AndClause(new Country());
			WhereClause where = new WhereClause(andTimezone, andCountry);
			FromClause from = new FromClause(timezone, new Country(), new Continent());
			SelectClause select = new SelectClause(where, from);
			
			if(geographical != null){
				andTimezone.foreignValue("id", geographical);
			}
			andTimezone.foreignKey("id", new Country());
			andTimezone.foreignKey("id", new Continent());
			andCountry.foreignKey("id", new Continent());
			
			trace(select);
			List<Timezone> list = connector.select(select, Timezone.class);
			
			respondJSON(200, list, Timezone.class);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
		finally{
			if(connector != null)
				connector.close();
		}
	}
	
	
	private Geographical geographical(){
		if(hasQuery("country")){
			return new Country(queryLong("country"));
		}
		
		else if(hasQuery("continent")){
			return new Continent(queryLong("continent"));
		}
		return null;
	}


}
