package ca.simplicitylabs.tide.orchestration;

import java.util.List;
import java.util.Map;

import ca.simplicitylabs.tide.definition.City;
import ca.simplicitylabs.tide.definition.Community;
import ca.simplicitylabs.tide.definition.Continent;
import ca.simplicitylabs.tide.definition.Country;
import ca.simplicitylabs.tide.definition.County;
import ca.simplicitylabs.tide.definition.Geographical;
import ca.simplicitylabs.tide.enumeration.ComparisonType;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.NotFound;
import ca.simplicitylabs.tide.sql.SQLConnector;
import ca.simplicitylabs.tide.sql.SQLOption;
import ca.simplicitylabs.tide.sql.SQLProvider;
import ca.simplicitylabs.tide.sql.clauses.AndClause;
import ca.simplicitylabs.tide.sql.clauses.FromClause;
import ca.simplicitylabs.tide.sql.clauses.JoinClause;
import ca.simplicitylabs.tide.sql.clauses.LeftJoinClause;
import ca.simplicitylabs.tide.sql.clauses.SelectClause;
import ca.simplicitylabs.tide.sql.clauses.WhereClause;
import io.undertow.server.HttpServerExchange;

public abstract class GeographicalFlow extends SQLFlow {
	private SQLConnector connector;
	public GeographicalFlow() throws APIError {
		super();
	}
	
	@Override
	public void handle(HttpServerExchange exchange, Map<String, String> resources) throws APIError{	
		connector = provider().connector();
		super.handle(exchange, resources);
	}
	
	@Override
	public void close(){
		try{
			if(connector != null)
				connector.close();
		}
		catch(Exception ex){
			error(ex);
		}
	}
	
	protected Class<? extends Geographical> getType() throws APIError{
		String type = resourceString("type");
		
		if(type == null)
			throw new NotFound();
		
		type = type.trim().toLowerCase();
		
		switch(type){
			case "continents": return Continent.class;
			case "countries": return Country.class;
			case "counties": return County.class;
			case "cities": return City.class;
			case "communities": return Community.class;
		}
		
		
		throw new NotFound(String.format("Geographical URI %s Not Found", type));
	}
	
	@Override
	public void get() throws APIError{
		Community community = new Community();
		City city = new City();
		County county = new County();
		Country country = new Country();
		Continent continent = new Continent();
		Geographical[] geographicals = new Geographical[]{community, city, county, country, continent};
		
		Class<? extends Geographical> cls = getType();
		Geographical search = query(cls);
		int limit = this.queryInteger("limit", 10);
		AndClause and = new AndClause(search);
		WhereClause where = new WhereClause(and);
		FromClause from = new FromClause(search);
		SelectClause select = new SelectClause(from, where);
		joinGeo(select, geographicals);
		
		and.configure("name", (new SQLOption()).compare(ComparisonType.STARTSWITH));
		select.limit(limit);
		List<? extends Geographical> list = connector.select(select, cls);
		
		respondJSON(200, list, cls);
		
	}
	
	private void joinGeo(SelectClause select, Geographical[] geographicals) throws APIError{
		Class<? extends Geographical> cls = getType();
		boolean pickup = false;
		for(int i = 0; i < geographicals.length; i++){
			Geographical geo = geographicals[i];
			if(pickup){
				Geographical prev = geographicals[i - 1];
				AndClause and = new AndClause(prev);
				JoinClause join = new LeftJoinClause(geo, and);
				and.foreignKey("id", geo);
				select.use(join);
			}
			
			if(cls.equals(geo.getClass()))
				pickup = true;
			
			if(pickup)
				select.selector(geo);
		}
		
	}

}
