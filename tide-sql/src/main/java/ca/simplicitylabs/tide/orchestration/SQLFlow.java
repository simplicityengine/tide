package ca.simplicitylabs.tide.orchestration;

import static ca.simplicitylabs.tide.enumeration.SerialFormat.JSON;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Column;
import ca.simplicitylabs.tide.definition.Definable;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.exception.NotAllowed;
import ca.simplicitylabs.tide.serialization.SQLSerializer;
import ca.simplicitylabs.tide.sql.SQLOption;
import ca.simplicitylabs.tide.sql.SQLProvider;
import ca.simplicitylabs.tide.sql.SQLResponse;
import ca.simplicitylabs.tide.sql.clauses.AlterTableClause;
import ca.simplicitylabs.tide.sql.clauses.AndClause;
import ca.simplicitylabs.tide.sql.clauses.DeleteClause;
import ca.simplicitylabs.tide.sql.clauses.FromClause;
import ca.simplicitylabs.tide.sql.clauses.InsertClause;
import ca.simplicitylabs.tide.sql.clauses.SelectClause;
import ca.simplicitylabs.tide.sql.clauses.UpdateClause;
import ca.simplicitylabs.tide.sql.clauses.WhereClause;
import io.undertow.io.Sender;

public abstract class SQLFlow extends APIFlow {
	private transient static GsonBuilder builder = new GsonBuilder()
			.setPrettyPrinting()
			.registerTypeHierarchyAdapter(Definable.class, new SQLSerializer());

	
	public SQLFlow () throws APIError{
		super();
	}
	
	public abstract SQLProvider provider();
	
	public Gson gson(){
		return builder.create();
	}

	
	public void modify(SQLOption option) throws APIError{
		throw new NotAllowed("Not Implemented");
	}
	
	@Override
	public void patch() throws APIError{
		SQLOption option = new SQLOption();
		modify(option);
	}
	
	@Override
	public void put() throws APIError{
		SQLOption option = new SQLOption().nullable(true);
		modify(option);
	}
	
	public <T extends Definable> T request(Class <T> cls) throws APIError{
		// TODO: add JSON/XML detection;
		// TODO: add List detection;
		try{
			Gson gson = gson();
			String payload = payload();
			JsonElement elem = gson.toJsonTree(payload);
			T definable = gson.fromJson(payload, cls);
			if(definable == null)
				definable = cls.newInstance();
			
			definable.deserialized(JSON, elem);
			return definable;
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex, BadRequest.class);
		}
	}
	
	public <T extends Definable> T payload(Class <T> cls) throws APIError{
		T definable = request(cls);
		if(definable != null)
			definable.setId(null);
		else{
			try {
				definable = cls.newInstance();
			} catch (Exception e) {
				throw AppUtils.normalize(e);
			}
		}
		return definable;
	}
	
	public <T extends Definable> T definable(Class <T> cls) throws APIError{
		T data = request(cls);
		T search = query(cls);
		if(data != null){
			if(search != null)
				data.setId(search.getId());
			else
				data.setId(null);
		}
		else{
			try {
				data = cls.newInstance();
			} catch (Exception e) {
				throw AppUtils.normalize(e);
			}
		}
		return data;
	}
	
	public <T extends Definable> T queryId(Class <T> cls, String... names) throws APIError{
		try {
			T definable = cls.newInstance();
			
			if(names.length <= 0){
				names = new String[]{definable.singular()};
			}
			
			for(String name : names){
				if(hasQuery(name, Long.class)){
					long id = queryLong(name, 0);
					definable.setId(id);
				}
			}
			
			return definable;
		} catch (Exception e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public <T extends Definable> T query(Class <T> cls) throws APIError{
		try {
			T definable = cls.newInstance();
			List<Field> fields = definable.getFields();
			Map<String, Deque<String>> query = exchange.getQueryParameters();
			
			for(Field field : fields){
				try{
					String key = field.getName();
					Class<?> type = field.getType();
					Deque<String> deque = query.get(key);
					
					if(deque != null){
						String value = deque.getFirst();
						if(String.class.isAssignableFrom(type))
							field.set(definable, value);
						
						else if(Number.class.isAssignableFrom(type) && AppUtils.isNumeric(value)){
							if(Long.class.isAssignableFrom(type))
								field.set(definable, Long.valueOf(value));
							
							else if(Integer.class.isAssignableFrom(type))
								field.set(definable, Integer.valueOf(value));
							// TODO: Double implementation
						}
							
						
						else if(Boolean.class.isAssignableFrom(type) && AppUtils.isBoolean(value))
							field.set(definable, Boolean.valueOf(value));
						
						else if(Definable.class.isAssignableFrom(type) && AppUtils.isLong(value)){
							try{
								Definable child = (Definable) type.newInstance();
								long id = Long.valueOf(value);
								child.setId(id);
								field.set(definable, child);
							}
							catch(Exception ex){
								warn(ex);
							}
						}
					}
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			
			return definable;
		} catch (Exception e) {
			throw AppUtils.normalize(e);
		}
	}
	
	public String query(String key){
		List<String> list = queryList(key);
		if(list.size() > 0 )
			return list.get(0);
		
		return null;
	}
	
	public List<String> queryList(String key){
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		List<String> list = new ArrayList<String>();
		if(query.containsKey(key)){
			Deque<String> value = query.get(key);
			Iterator<String> iter = value.iterator();
			while(iter.hasNext())
				list.add(iter.next());
		}
		return list;
	}
	

	
	protected <T extends Definable> void respondJSON(int code, Object payload, Class <T> cls) throws Internal{
		try {
			T definable = cls.newInstance();
			respondJSON(code, payload, definable.tableName());
		} catch (InstantiationException | IllegalAccessException e) {
			error(e);
			throw new Internal("Internal Class Error");
		}
		
	}
	
	protected <T extends Definable> void respondJSON(int code, Object payload, String plural) throws Internal{
		Gson gson = gson();
		header("Access-Control-Allow-Origin", "*");
		header("Content-Type", "application/json");
		
		if(payload instanceof Iterable){
			JsonObject json = new JsonObject();
			JsonElement array = gson.toJsonTree(payload);
			json.add(plural, array);
			respond(code, gson.toJson(json));
		}
		else{
			respond(code, gson.toJson(payload));
		}
		
	}
	
	@Override
	public void handleError(APIError ex) throws APIError{
		Gson gson = gson();
		header("Access-Control-Allow-Origin", "*");
		header("Content-type", "application/json");
		
		exchange.setStatusCode(ex.getCode());
		error(ex);
		Sender sender = exchange.getResponseSender();
		if(sender != null){
			
			JsonObject object = new JsonObject();
			object.addProperty("code", ex.getCode());
			object.addProperty("message", ex.getMessage());
			if(ex.getUUID() != null)
				object.addProperty("uuid", ex.getUUID().toString());
			
			sender.send(gson.toJson(object));
		}
	}
}
