package ca.simplicitylabs.tide;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ca.simplicitylabs.tide.annotation.Activate;
import ca.simplicitylabs.tide.annotation.DeActivate;
import ca.simplicitylabs.tide.annotation.Ignore;
import ca.simplicitylabs.tide.annotation.Implementation;
import ca.simplicitylabs.tide.annotation.Sequence;
import ca.simplicitylabs.tide.configuration.ConfigStore;
import ca.simplicitylabs.tide.configuration.DirectoryConfig;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class AppUtils {
	private static String DATEFORMAT = "yyyy-MM-dd hh:mm:ss";
	private static SelfLogger log;
	@SuppressWarnings("unused")
	private static Logger rca = LogManager.getLogger(AppUtils.class);
	final private static char[] hexArray = "0123456789abcdef".toCharArray();
	final private static MessageDigest md5 = DigestUtils.getMd5Digest();
	final private static Map<String, String> cardinalityNouns = new HashMap<String, String>();
	final private static ConfigStore store = AppUtils.instantiate(ConfigStore.class);
	
	
	private static DocumentBuilderFactory dfactory;
	private static DocumentBuilder dbuilder; 
	private static TransformerFactory tfactory;
	private static Transformer transformer;
	private static XPathFactory xfactory;
	private static XPath xpath;

	static {
		log = new SelfLogger(AppUtils.class);
		// https://www.thoughtco.com/irregular-plural-nouns-in-english-1692634
		Map<String, String> nouns = cardinalityNouns;
		nouns.put("aircraft", "aircraft");
		nouns.put("alumna", "alumnae");
		nouns.put("alumnus", "alumni");
		nouns.put("analysis", "analyses");
		nouns.put("antenna", "antennae");
		nouns.put("antithesis", "antitheses");
		nouns.put("apex", "apices");
		nouns.put("appendix", "appendices");
		nouns.put("axis", "axes");
		nouns.put("bacillus", "bacilli");
		nouns.put("basis", "bases");
		nouns.put("beau", "beaux");
		nouns.put("bison", "bison");
		nouns.put("bureau", "bureaux");
		nouns.put("cactus", "cacti");
		nouns.put("child", "children");
		nouns.put("codex", "codices");
		
		try {
			dfactory =  DocumentBuilderFactory.newInstance();
			dfactory.setNamespaceAware(true);
			dfactory.setIgnoringComments(true);
			dfactory.setIgnoringElementContentWhitespace(true);
			dbuilder = dfactory.newDocumentBuilder();
			
			tfactory = TransformerFactory.newInstance();
			transformer = tfactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "3");
			
			xfactory = XPathFactory.newInstance();
			xpath = xfactory.newXPath();
		} 
		catch (Exception e) {
			log.error(e);
		}
	}
	
	public static void activate(Class<?> cls) throws APIError{
		invoke(cls, Activate.class);
	}
	
	public static void deactivate(Class<?> cls) throws APIError{
		invoke(cls, DeActivate.class);
	}
	
	public static void invoke(Class<?> cls, Class<? extends Annotation> annotatedClass) throws APIError{
		try{
			Method[] methods = cls.getDeclaredMethods();
			Object object = cls.newInstance();
			for(Method method : methods){
				try{
					method.setAccessible(true);
					if(method.isAnnotationPresent(annotatedClass)){
						log.debug("invoking: %s.%s", cls.getCanonicalName(), method.getName());
						method.invoke(object);
					}
				}
				catch(Exception ex){
					log.warn("%s.%s: %s", cls.getCanonicalName(), method.getName(), ex);
				}
			}
		}
		catch(Exception ex){
			throw normalize(ex);
		}
	}
	
	public static Class<?> implementation(Class<?> type, boolean initialize, ClassLoader loader){
		
		if(type.isAnnotationPresent(Implementation.class)){
			if(type.isInterface())
				return type;
			
			Implementation implementation = type.getAnnotation(Implementation.class);
			String[] paths = implementation.value();
			
			for(String path : paths){
				log.debug(path);
				try { return Class.forName(path, initialize, loader);} 
				catch (ClassNotFoundException e) { log.warn(e);}
			}
			
			String path = String.format("%sImpl", type.getCanonicalName());
			log.debug(path);
			try { return Class.forName(path, initialize, loader);} 
			catch (ClassNotFoundException e) {}
		}
		
		return type;
	}
	
	public static boolean classExists(String classpath, ClassLoader loader){
		try {
			Class.forName(classpath, false, loader);
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}
	
	public static <T> T instantiate(Class<T> type){
		try {
			return instantiateWithErrors(type);
		} catch (APIError e) {
			log.error(e);
		}
		return null;
	}
	
	public static <T> T instantiate(Class<T> type, Object... args){
		try {
			return instantiateWithErrors(type, args);
		} catch (APIError e) {
			log.error(e);
		}
		return null;
	}
	
	public static Class<?> genericType(Field field){
		try{
			ParameterizedType generic = (ParameterizedType) field.getGenericType();
			Class<?> genericType = (Class<?>) generic.getActualTypeArguments()[0];
			return genericType;
		}
		catch(Exception ex){
			return null;
		}
	}
	
	public static Date now(){
		return new Date();
	}
	
	public static Date now(long timestamp){
		return new Date(timestamp*1000);
	}
	
	public static Date now(String zone){
		TimeZone timezone = TimeZone.getTimeZone(zone);
		Calendar calendar = Calendar.getInstance(timezone);
		calendar.setTime(new Date());
		return calendar.getTime();
	}
	
	public static Date zoned(Date date, String zone){
		TimeZone timezone = TimeZone.getTimeZone(zone);
		Calendar calendar = Calendar.getInstance(timezone);
		calendar.setTime(date);
		return calendar.getTime();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T instantiateWithErrors(Class<T> type) throws APIError{
		try {
			Class<?> cls = null;
			ClassLoader loader = activeLoader();
			if(type.isInterface()){
				// TODO: check for annotation
				String implementation = String.format("%sImpl", type.getCanonicalName());
				cls = loader.loadClass(implementation);
			}
			else
				cls = type;
			
			return (T) cls.newInstance();
		} catch (Exception e) {
			log.error(e);
			throw new Internal(e);
		}
	}
	@SuppressWarnings({ "unchecked"})
	public static <T> T instantiateWithErrors(Class<T> type, Object... args) throws APIError{
		try{
			Class<?> cls = null;
			ClassLoader loader = activeLoader();
			if(type.isInterface()){
				// TODO: check for annotation
				String implementation = String.format("%sImpl", type.getCanonicalName());
				cls = loader.loadClass(implementation);
			}
			else
				cls = type;
			
			if(args == null)
				return (T) cls.newInstance();
			
			if(args.length <= 0)
				return (T) cls.newInstance();
			
			// find all public constructors
			Constructor<?>[] builders = cls.getConstructors();
			
			// TODO: make a more graceful code block
			for(Constructor<?> builder: builders){
				try{
					return (T) builder.newInstance(args);
				}
				catch(IllegalArgumentException e){
				}
			}
			
			return (T) cls.newInstance();
		}
		catch(Exception e){
			log.error(e);
			throw new Internal(e);
		}
	}
	
	public static URI normalize(URI uri){
		try{
			String path = uri.normalize().toString();
			path = String.format("/%s", path);
			path = path.replaceAll("[\\/]{2,}", "/");
			path = path.replaceAll("\\/$", "");
		
			return new URI(path);
		}
		catch(Exception ex){
			return uri;
		}
	}
	
	public static APIError normalize (Exception ex){
		if(ex instanceof APIError)
			return (APIError) ex;
		
		return new Internal(ex);
	}
	
	public static APIError normalize (Exception ex, Class<? extends APIError> type){
		if(ex instanceof APIError)
			return (APIError) ex;
		
		return instantiate(type, ex);
	}
	
	public static StackTraceElement stack(int level, StackTraceElement[] stacks){
		if(stacks.length > level)
			return stacks[level];
		
		return new StackTraceElement("", "", "", 0);
	}
	
	public static StackTraceElement stack(String layer){
		StackTraceElement[] stacks = activeThread().getStackTrace();
		int level = 0;
		for(int i = 0; i < stacks.length; i++){
			StackTraceElement stack = stacks[i];
			String className = stack.getClassName();
			String methodName = stack.getMethodName();
			boolean check = !className.startsWith(layer);
			check = check && !(className.endsWith(AppUtils.class.getSimpleName()) && methodName.equals("stack"));
			check = check && !(className.endsWith(Thread.class.getSimpleName()) && methodName.equals("getStackTrace"));
			//rca.info("[rca]: {}.{}: {}", className, methodName, check);
			if(check){
				level = i;
				break;
			}
		}
		
		return stack(level, stacks);
	}
	
	public static void sleep(long milliseconds) throws APIError{
		try{
			Thread.sleep(milliseconds);
		}
		catch(Exception ex){
			throw normalize(ex);
		}
	}
	
	public static Thread activeThread(){
		return Thread.currentThread();
	}
	
	public static ClassLoader activeLoader() throws APIError{
		try{
			return activeThread().getContextClassLoader();
		}
		catch(Exception ex){
			throw new Internal(ex);
		}
	}
	
	public static List<DirectoryConfig> directories(Class<?> cls) throws APIError{
		return store.list(manifest(cls), DirectoryConfig.class);
	}
	
	
	
	public static List<JarFile> jars(Class<?> cls) throws APIError{
		ClassLoader loader = cls.getClassLoader();
		return jars(loader, cls);
	}
	
	public static List<JarFile> jars(ClassLoader loader) throws APIError{
		return jars(loader, "META-INF/MANIFEST.MF");
	}
	
	public static List<JarFile> jars(ClassLoader loader, Class<?> cls) throws APIError{
		String path = cls.getCanonicalName();
		path = path.replace(".", "/");
		path = String.format("%s.class", path);
		return jars(loader, path);
	}
	
	public static List<JarFile> jars(ClassLoader loader, String classPath) throws APIError{
		try{
			List<JarFile> list = new ArrayList<JarFile>();
			Enumeration<URL> resources = loader.getResources(classPath);
			while (resources.hasMoreElements()) {
				URL resource = resources.nextElement();
				String filePath = AppUtils.regexGroup(resource.toString(), "\\/(.*)\\!");
				if(filePath == null)
					filePath = resource.toString();
				
				JarFile file = new JarFile(filePath);
				list.add(file);
			}
			return list;
		}
		catch(Exception ex){
			throw normalize(ex);
		}
	}
	
	public static boolean has(String className, JarFile file){
		Enumeration<JarEntry> entries = file.entries();
		while(entries.hasMoreElements()){
			JarEntry entry = entries.nextElement();
			String name = entry.getName();
			if(name.endsWith(".class")){
				name = name.replace("/", ".").replace(".class", "");
				if(className.equals(name))
					return true;
			}
			
		}
		return false;
	}
	
	public static Manifest manifest() throws APIError{
		StackTraceElement[] stacks = activeThread().getStackTrace();
		StackTraceElement stack = stacks[2];
		return manifest(activeLoader(), stack.getClassName());
	}
	
	public static Manifest manifest(Class<?> cls) throws APIError{
		return manifest(cls.getClassLoader(), cls);
	}
	
	public static Manifest manifest(ClassLoader loader, Class<?> cls) throws APIError{
		return manifest(loader, cls.getCanonicalName());
	}
	
	public static Manifest manifest(ClassLoader loader, String className) throws APIError{
		try{
			String classPath = className.replace(".", "/");
			classPath = String.format("%s.class", classPath);
			List<JarFile> jars = jars(loader, classPath);
			log.trace("found %d jars that contain class: %s", jars.size(), className);
			for(JarFile jar : jars){
				log.trace("returning manifest of jar: %s", jar.getName());
				return jar.getManifest();
			}
			
			throw new Internal("Manifest File does not exist in JAR!");
		}
		catch(Exception ex){
			throw normalize(ex);
		}
	}
	
	public static String meta(String path) throws APIError{
		return meta(path, activeLoader());
	}
	
	public static String meta(String path, ClassLoader loader) throws APIError{
		try{
			path = String.format("META-INF/%s", path.replaceAll("^\\/", ""));
			log.debug(path);
			InputStream is = loader.getResourceAsStream(path);
			String content = IOUtils.toString(is, "UTF-8");
			return content;
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public static String simpleClassName(String packageName){
		String regex = "\\.([\\w_]+)$";
		String name = regexGroup(packageName, regex, 0);
		return (name != null ?  name :  packageName);
	}
	

	public static boolean isNumeric(Object value) {
		if (isString(value)) {
			try {
				String string = (String) value;
				Double.valueOf(string);
				return true;
			} catch (NumberFormatException ex) {
				return false;
			}
		}
		return value instanceof Number;
	}

	public static boolean isNumeric(Class<?> cls) {
		return Number.class.isAssignableFrom(cls);
	}

	public static boolean isDouble(Class<?> cls) {
		return Double.class.isAssignableFrom(cls);
	}

	public static boolean isInteger(Class<?> cls) {
		return Integer.class.isAssignableFrom(cls);
	}
	
	public static boolean isLong(Class<?> cls) {
		return Long.class.isAssignableFrom(cls);
	}

	public static boolean isInteger(Object value) {
		if (isString(value)) {
			try {
				String string = (String) value;
				Integer.valueOf(string);
				return true;
			} catch (NumberFormatException ex) {
				return false;
			}
		}
		return value instanceof Integer;
	}
	
	public static boolean isLong(Object value) {
		if (isString(value)) {
			try {
				String string = (String) value;
				Long.valueOf(string);
				return true;
			} catch (NumberFormatException ex) {
				return false;
			}
		}
		return value instanceof Long;
	}

	public static boolean isBoolean(Object value) {
		if (value instanceof String) {
			String string = (String) value;
			string = string.trim().toLowerCase();
			return string.matches("^(?:true|false)$");
		}
		return value instanceof Boolean;
	}

	public static boolean isBoolean(Class<?> cls) {
		return Boolean.class.isAssignableFrom(cls);
	}

	public static boolean isString(Object value) {
		return value instanceof String;
	}

	public static boolean isString(Class<?> cls) {
		return String.class.isAssignableFrom(cls);
	}

	public static boolean isDate(Class<?> cls) {
		return Date.class.isAssignableFrom(cls);
	}

	public static boolean isDate(Object value) {
		if (isString(value)) {
			try {
				String string = (String) value;
				SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
				sdf.parse(string);
				return true;
			} catch (ParseException e) {
				return false;
			}
		}
		return value instanceof Date;
	}
	
	public static boolean isUUID(Object value){
		if(value instanceof UUID)
			return true;
		
		if(value instanceof String){
			String string = (String) value;
			return string.matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
		}
		return false;
	}
	
	public static boolean isUUID(Class<?> cls){
		return UUID.class.equals(cls);
	}

	public static boolean isPrimitive(Class<?> cls) {
		return (isString(cls) || isBoolean(cls) || isNumeric(cls) || isDate(cls) || isUUID(cls));
	}

	public static boolean isPrimitive(Object value) {
		return (isString(value) || isBoolean(value) || isNumeric(value) || isDate(value) || isUUID(value));
	}
	
	public static boolean isIterable(Class<?> cls){
		if( Iterable.class.isAssignableFrom(cls) )
			return true;
		
		return false;
	}
	
	public static boolean isIterable(Object iterable){
		if(iterable instanceof Iterable)
			return true;
		
		return false;
	}

	public static long checksum(File file) {
		try {
			return FileUtils.checksumCRC32(file);

		} catch (IOException e) {
			log.error(e);
		}
		return -1;
	}
	
	public static String regexGroup(String content, String expression){
		return regexGroup(content, expression, 0);
	}

	public static String regexGroup(String content, String expression, int group) {
		List<String> results = regexGroups(content, expression);

		if (results.size() > group)
			return results.get(group);

		return null;
	}

	public static List<String> regexGroups(String content, String expression) {
		List<String> results = new ArrayList<String>();
		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(content);
		if (m.find()) {
			int count = m.groupCount();
			
			for (int i = 1; i <= count; i++){
				String group = m.group(i);
				log.trace("found: %s", group);
				results.add(group);
			}
		}

		return results;
	}
	
	public static List<Field> fields(Class<?> target){
		return fields(target, target);
	}
	
	public static List<Field> fields(Class<?> target, Class<?> base){
		List<Field> list = new ArrayList<Field>();
		fields(target, list, base);
		return list;
	}
	
	public static void fields(Class<?> cls, List<Field> list, Class<?> base){
		Class<?> parent = cls.getSuperclass();
		if(parent != null){
			if(base.isAssignableFrom(parent) ){
				fields(parent, list, base);
			}
		}
		
		Field[] array = cls.getDeclaredFields();
		
		for(Field field : array){
			field.setAccessible(true);
			int mod = field.getModifiers();
			boolean ignore = field.isAnnotationPresent(Ignore.class);
			boolean trans = Modifier.isTransient(mod);
			boolean stat = Modifier.isStatic(mod);
			// add or replace field
			add(field, list);
			
			if(trans || stat || ignore){
				drop(field, list);
			}
		}
	}
	
	private static void add(Field field, List<Field> list){
		int size = list.size();
		for(int i = size - 1; i >= 0; i--){
			Field item = list.get(i);
			item.setAccessible(true);
			if(item.getName().equals(field.getName())){
				list.remove(i);
				list.add(i, field);
				return;
			}
		}
		
		list.add(field);
	}
	
	private static void drop(Field field, List<Field> list){
		int size = list.size();
		for(int i = size - 1; i >= 0; i--){
			Field item = list.get(i);
			item.setAccessible(true);
			if(item.getName().equals(field.getName())){
				list.remove(i);
				break;
			}
		}
	}
	
	public static void copy(Object from, Object to, boolean includeNulls){
		try{
			List<Field> fields = fields(to.getClass(), Object.class);
			for(Field field : fields){
				String name = field.getName();
				Object value = field.get(from);
				Class<?> type = field.getType();
				
				// TODO: improve handling of interable
				if(isPrimitive(type) || isIterable(type)){
					field.set(to, value);
				}
				else{
					Object child = field.get(to);
					if(child == null){
						child = type.newInstance();
					}
					copy(value, child, includeNulls);
					field.set(to, child);

				}
			}
		}
		catch(Exception ex){
			log.warn(ex);
		}
	}

	public static String encodeBase64(String value) {
		return Base64.encodeBase64String(value.getBytes());
	}

	public static String decodeBase64(String value) {
		byte[] decoded = Base64.decodeBase64(value);

		return new String(decoded);
	}

	public static String encryptMD5(String message) throws APIError {
		try {
			byte[] bytes = md5.digest(message.getBytes("UTF-8"));
			return hexString(bytes);
		} catch (Exception ex) {
			log.error(ex);
			throw new Internal("Encoding Not Supported!");
		}
	}
	
	public static String hexString(String value) throws APIError {
		try{
			return hexString(value.getBytes("UTF-8"));
		}
		catch(Exception ex){
			log.error(ex);
			throw new Internal("Encoding Not Supported!");
		}
	}
	
	public static String hexString(byte[] bytes) throws APIError {
		try {

			char[] hexChars = new char[bytes.length * 2];
			for (int j = 0; j < bytes.length; j++) {
				int v = bytes[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
				hexChars[j * 2 + 1] = hexArray[v & 0x0F];
			}
			return new String(hexChars);
		} catch (Exception ex) {
			log.error(ex);
			throw new Internal("Encoding Not Supported!");
		}
	}
	public static boolean isXML(String content){
		try {
			xmlWithErrors(content);
			return true;
		} catch (APIError e) {
			return false;
		}
	}
	
	public static Document xml(String content){
		try {  
            return xmlWithErrors(content);
        }
		catch (Exception e) {  
            return null;
        } 
	}
	
	public static Document xmlWithErrors(String content) throws APIError{
		try {  
            Document doc = dbuilder.parse( new InputSource( new StringReader( content ) ) ); 
            return doc;
        }
		catch (Exception e) {  
            throw normalize(e, BadRequest.class);
        } 
	}

	public static String xml(Node node){
        try {
            return xmlWithErrors(node);
        } catch (Exception e) {
            return null;
        }
	}
	
	public static String xmlWithErrors(Node node) throws APIError{
        try {
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(node), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (Exception e) {
            throw normalize(e);
        }
	}
	
	public static List<Node> nodes(Document document, String path){
		List<Node> list = new ArrayList<Node>();
		try{
			XPathExpression expression = xpath.compile(path);
			NodeList nodes = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
			for(int i = 0; i < nodes.getLength(); i++){
				Node node = nodes.item(i);
				list.add(node);
			}
		}
		catch(Exception ex){
			log.warn(ex);
		}
		
		return list;
	}
	public static Double round(Double value) throws APIError{
		return round(value, 2);
	}
	public static Double round(Double value, int figures) throws APIError{
		try{
			Double r = Math.pow(10, figures);
			return Math.round(value*r)/r;
		}
		catch(Exception ex){
			throw normalize(ex);
		}
	}
	
	public static void ascend(List<Field> fields){
		sort(fields, false);
	}
	
	public static void descend(List<Field> fields){
		sort(fields, true);
	}
	
	private static void sort(List<Field> fields, boolean desc){
		int size = fields.size();
		
		for(int i = size - 1; i > 0; i--){
			for(int j = i - 1; j >= 0; j--){
				Field a = fields.get(i);
				Field b = fields.get(j);
				int x = magnitude(a);
				int y = magnitude(b);
				boolean change = (desc ? x > y: x < y);
				
				if(change){
					log.trace("replace [%s: %d] with [%s: %d]", a.getName(), x, b.getName(), y);
					fields.set(i, b);
					fields.set(j, a);					
				}
			}
			
		}
	}
	
	public static int magnitude(Field field){
		if(field.isAnnotationPresent(Sequence.class)){
			Sequence magnitude = field.getAnnotation(Sequence.class);
			return Math.max(magnitude.value(), 0);
		}
		return 10000;
	}

	public static String plural(String noun) {
		noun = noun.toLowerCase().trim();
		if (cardinalityNouns.containsKey(noun))
			return cardinalityNouns.get(noun);
		
		if(noun.endsWith("quy"))
			noun = noun.replaceAll("y$", "ies");
		
		else if(noun.matches(".*[aeiou]y$"))
			noun = String.format("%s%s", noun, "s");
		
		else if(noun.endsWith("um"))
			noun = noun.replaceAll("um$", "a");
		
		else if (noun.endsWith("y"))
			noun = noun.replaceAll("y$", "ies");
		
		else if(noun.endsWith("ix"))
			noun = noun.replaceAll("ix$", "ices");
		
		else if(noun.matches(".*[shx]$"))
			noun = String.format("%s%s", noun, "es");
		
		else
			noun = String.format("%s%s", noun, "s");
	
		return noun;

	}

	public static String singular(String noun) {
		noun = noun.toLowerCase().trim();

		if (cardinalityNouns.containsValue(noun)) {
			Set<Entry<String, String>> nouns = cardinalityNouns.entrySet();
			Iterator<Entry<String, String>> iter = nouns.iterator();
			while (iter.hasNext()) {
				Entry<String, String> entry = iter.next();
				String value = entry.getValue();
				String single = entry.getKey();
				if (noun.equalsIgnoreCase(value))
					return single;
			}
		}
		
		if (noun.endsWith("ices"))
			noun = noun.replaceAll("ices$", "ix");
		
		else if (noun.endsWith("ies"))
			noun = noun.replaceAll("ies$", "y");

		else if (noun.endsWith("ses") || noun.endsWith("hes") || noun.endsWith("xes"))
			noun = noun.replaceAll("es$", "");
		
		else if (noun.endsWith("a"))
			noun = noun.replaceAll("a$", "um");

		else
			noun = noun.replaceAll("s$", "");
		
		return noun;
	}
	
	public static String toString(Exception ex){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		ex.printStackTrace(pw);
		return sw.toString();
	}
	
	public static Integer toInteger(Long value){
		if(value != null)
			return value.intValue();
		return null;
	}
	
	public static Integer toInteger(Double value){
		if(value != null)
			return value.intValue();
		return null;
	}
	
	public static Long toLong(Integer value){
		if(value != null)
			return value.longValue();
		return null;
	}
	
	public static Long toLong(InetSocketAddress address){
		return toLong(address.getAddress());
	}
	
	public static Long toLong(InetAddress address){
		byte[] raw = address.getAddress();
		final long ipaddress =((raw [0] & 0xFFl) << (3*8)) + ((raw [1] & 0xFFl) << (2*8)) + ((raw [2] & 0xFFl) << (1*8)) + (raw [3] &  0xFFl);
		return ipaddress;
	}
	
	public static String host(InetSocketAddress address){
		return host(address.getAddress());
	}
	
	public static String host(InetAddress address){
		return address.getHostAddress();
	}
	
	public static String hostname(InetSocketAddress address){
		return hostname(address.getAddress());
	}
	
	public static String hostname(InetAddress address){
		return address.getHostName();
	}
	
	public static Integer port(InetSocketAddress address){
		return address.getPort();
	}
}
