package ca.simplicitylabs.tide.exception;

public class UnsupportedMediaType extends APIError {
	private static final long serialVersionUID = -1684553753842757784L;
	public UnsupportedMediaType(){
		super(415);
	}
	public UnsupportedMediaType(String message){
		this();
		setMessage(message);
	}
	public UnsupportedMediaType(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public UnsupportedMediaType(Exception ex){
		super(415, ex);
	}
}
