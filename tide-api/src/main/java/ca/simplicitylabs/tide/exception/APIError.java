package ca.simplicitylabs.tide.exception;

import java.util.UUID;

public abstract class APIError extends Exception {
	protected int code;
	protected String message = "";
	protected UUID uuid;
	public APIError(){
		super();
	}
	
	public APIError(int code){
		super();
		setCode(code);
	}
	
	public APIError(String message){
		super();
		setMessage(message);
	}
	
	
	public APIError(Exception ex){
		super();
		this.setStackTrace(ex.getStackTrace());
		String message = ex.getLocalizedMessage();
		
		if(!ok(message))
			message = ex.getMessage();
		
		if(!ok(message))
			message = ex.toString();
		
		setMessage(message);
	}
	
	public APIError(int code, Exception ex){
		this(ex);
		setCode(code);
	}
	
	public APIError(int code, String message){
		this(code);
		setMessage(message);
	}
	
	protected boolean ok(String value){
		return (value == null ? false : !value.trim().isEmpty());
	}
	
	
	public int getCode(){
		return code;
	}
	
	protected void setCode(int code){
		this.code = code;
	}
	@Override
	public String getMessage(){
		return message;
	}
	
	@Override
	public String getLocalizedMessage(){
		return message;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void setMessage(String template, Object ...args){
		this.message = String.format(template, args);
	}

	public UUID getUUID() {
		return uuid;
	}

	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}
	
	public void replace(String find, String alternative){
		try{
			if(ok(message) && ok(find) && alternative != null )
				message = message.replace(find, alternative);
		}
		catch(Exception ex){
			message = "Unknown";
		}
	}
	
	
}
