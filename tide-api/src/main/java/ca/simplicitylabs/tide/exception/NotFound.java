package ca.simplicitylabs.tide.exception;

public class NotFound extends APIError {
	private static final long serialVersionUID = -409684155831188459L;
	public NotFound(){
		super(404);
	}
	public NotFound(String message){
		this();
		setMessage(message);
	}
	public NotFound(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public NotFound(Exception ex){
		super(404, ex);
	}
}
