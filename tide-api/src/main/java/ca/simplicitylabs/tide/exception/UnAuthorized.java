package ca.simplicitylabs.tide.exception;

public class UnAuthorized extends APIError {
	private static final long serialVersionUID = 4950477267880770653L;
	public UnAuthorized(){
		super(401);
	}
	public UnAuthorized(String message){
		this();
		setMessage(message);
	}
	public UnAuthorized(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public UnAuthorized(Exception ex){
		super(401, ex);
	}
}
