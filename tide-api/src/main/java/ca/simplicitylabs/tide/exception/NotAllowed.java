package ca.simplicitylabs.tide.exception;

public class NotAllowed extends APIError {
	private static final long serialVersionUID = 338896266120549955L;
	public NotAllowed(){
		super(405);
	}
	public NotAllowed(String message){
		this();
		setMessage(message);
	}
	public NotAllowed(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public NotAllowed(Exception ex){
		super(405, ex);
	}
}
