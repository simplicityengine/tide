package ca.simplicitylabs.tide.exception;

public class TooManyRequests extends APIError {
	private static final long serialVersionUID = 4080488244923257525L;
	public TooManyRequests(){
		super(429);
	}
	public TooManyRequests(String message){
		this();
		setMessage(message);
	}
	public TooManyRequests(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public TooManyRequests(Exception ex){
		super(429, ex);
	}
}
