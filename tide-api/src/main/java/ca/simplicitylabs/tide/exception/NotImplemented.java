package ca.simplicitylabs.tide.exception;

public class NotImplemented extends APIError {
	private static final long serialVersionUID = -7476710887967548956L;
	public NotImplemented(){
		super(501);
	}
	public NotImplemented(String message){
		this();
		setMessage(message);
	}
	public NotImplemented(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public NotImplemented(Exception ex){
		super(501, ex);
	}
}
