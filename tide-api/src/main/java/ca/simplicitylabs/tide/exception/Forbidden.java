package ca.simplicitylabs.tide.exception;

public class Forbidden extends APIError {
	private static final long serialVersionUID = 202405423266424018L;
	public Forbidden(){
		super(403);
	}
	public Forbidden(String message){
		this();
		setMessage(message);
	}
	
	public Forbidden(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	
	public Forbidden(Exception ex){
		super(403, ex);
	}
}
