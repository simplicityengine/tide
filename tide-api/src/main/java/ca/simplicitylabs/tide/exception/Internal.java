package ca.simplicitylabs.tide.exception;

public class Internal extends APIError {
	private static final long serialVersionUID = -1684553753842757784L;
	public Internal(){
		super(500);
	}
	public Internal(String message){
		this();
		setMessage(message);
	}
	public Internal(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public Internal(Exception ex){
		super(500, ex);
	}
}
