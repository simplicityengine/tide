package ca.simplicitylabs.tide.exception;

public class PaymentRequired extends APIError {
	private static final long serialVersionUID = -664241769102026905L;
	public PaymentRequired(){
		super(402);
	}
	public PaymentRequired(String message){
		this();
		setMessage(message);
	}
	public PaymentRequired(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	public PaymentRequired(Exception ex){
		super(402, ex);
	}
}
