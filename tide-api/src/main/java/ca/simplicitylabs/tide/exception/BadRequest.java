package ca.simplicitylabs.tide.exception;

public class BadRequest extends APIError {
	private static final long serialVersionUID = 2700444605063957529L;
	public BadRequest(){
		super(400);
	}
	public BadRequest(String message){
		this();
		setMessage(message);
	}
	
	public BadRequest(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	
	public BadRequest(Exception ex){
		super(400, ex);
	}
}
