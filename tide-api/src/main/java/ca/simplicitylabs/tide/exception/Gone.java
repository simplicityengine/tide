package ca.simplicitylabs.tide.exception;

public class Gone extends APIError {
	private static final long serialVersionUID = 3112028665651366675L;

	public Gone(){
		super(410);
	}
	public Gone(String message){
		this();
		setMessage(message);
	}
	
	public Gone(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	
	public Gone(Exception ex){
		super(410, ex);
	}
}
