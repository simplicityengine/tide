package ca.simplicitylabs.tide.exception;

public class Conflict extends APIError {
	private static final long serialVersionUID = 202405423266424018L;
	public Conflict(){
		super(409);
	}
	public Conflict(String message){
		this();
		setMessage(message);
	}
	
	public Conflict(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	
	public Conflict(Exception ex){
		super(409, ex);
	}
}
