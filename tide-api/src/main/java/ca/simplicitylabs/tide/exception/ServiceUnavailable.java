package ca.simplicitylabs.tide.exception;

public class ServiceUnavailable extends APIError {
	private static final long serialVersionUID = 202405423266424018L;
	public ServiceUnavailable(){
		super(503);
	}
	public ServiceUnavailable(String message){
		this();
		setMessage(message);
	}
	
	public ServiceUnavailable(String message, Object ...args){
		this();
		setMessage(message, args);
	}
	
	public ServiceUnavailable(Exception ex){
		super(503, ex);
	}
}
