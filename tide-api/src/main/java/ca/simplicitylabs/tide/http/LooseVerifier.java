package ca.simplicitylabs.tide.http;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import ca.simplicitylabs.tide.historization.SelfLogger;

public class LooseVerifier extends SelfLogger implements HostnameVerifier {

	public LooseVerifier() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean verify(String hostname, SSLSession session) {
		info("hostname: %s", hostname);
		return true;
	}

}
