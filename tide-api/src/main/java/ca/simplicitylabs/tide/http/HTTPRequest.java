package ca.simplicitylabs.tide.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.enumeration.APIAuth;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class HTTPRequest extends SelfLogger {
	private static final SelfLogger log = new SelfLogger(HTTPRequest.class);
	private static final String REGEX_PREFIX = "(?i)(^https|^http)\\:\\/\\/.*";
	private static final String REGEX_HOSTNAME = "(?i)(?:https|^http)\\:\\/\\/([\\w\\.\\-]+[\\w]{1})(?:\\/|\\:).*";
	private static final String REGEX_PORT = "(?i)(?:https|^http)\\:\\/\\/(?:[\\w\\.\\-]+[\\w]{1}\\:)(\\d{2,4})(?:\\/.*|$)";
	private static final String REGEX_URI = "(?i)(?:https|^http)\\:\\/\\/(?:[\\w\\.\\-\\:]+)(\\/[\\w\\-\\.\\:\\/]+)$";
	
	private String hostname;
	private Integer port;
	private String uri;
	private String username;
	private String password;
	private String domain;
	private String bearer;
	private APIAuth auth = APIAuth.NONE;
	
	private Map<String, Object> queries = new HashMap<String, Object>();
	private Map<String, Object> headers = new HashMap<String, Object>();
	private Map<String, Object> resources = new HashMap<String, Object>();
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private HostnameVerifier verifier = new LooseVerifier();
	private boolean secure = false;
	
	static {
		System.setProperty("https.protocols", "SSLv3,TLSv1,TLSv1.1,TLSv1.2");
		
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(new KeyManager[]{new LooseKeyManager()}, new TrustManager[]{new LooseTrustManager()}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(new LooseVerifier());
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			log.error(e);
		}
		
	}
	
	public HTTPRequest(){
		addHeader("Connection", "keep-alive");
	}
	
	public HTTPRequest(String url) throws APIError{
		this();
		url(url);
	}
	
	public String hostname(){
		return hostname;
	}
	
	public void hostname(String value){
		this.hostname = value;
	}
	
	public Integer port(){
		return port;
	}
	
	public void port(Integer value){
		this.port = value;
	}
	
	public String uri(){
		return uri;
	}
	
	public void uri(String value){
		this.uri = value;
	}
	
	public boolean secure(){
		return secure;
	}
	
	public void secure(boolean value){
		this.secure = value;
	}
	
	public String username(){
		return username;
	}
	
	public void username(String value){
		this.username = value;
	}
	
	public String password(){
		return password;
	}
	
	public void password(String value){
		this.password = value;
	}
	
	public String domain(){
		return domain;
	}
	
	public void domain(String value){
		this.domain = value;
	}
	
	public String bearer(){
		return bearer;
	}
	
	public void bearer(String value){
		this.bearer = value;
	}
	
	public APIAuth auth(){
		return auth;
	}
	
	public void auth(APIAuth value){
		this.auth = value;
	}
	
	public void basicAuth(String username, String password){
		username(username);
		password(password);
		auth(APIAuth.BASIC);
	}
	
	public void basicAuth(String username, String password, String domain){
		domain(domain);
		basicAuth(username, password);
	}
	
	public void bearerAuth(String bearer){
		bearer(bearer);
		auth(APIAuth.BEARER);
	}
	
	public HTTPResponse post() throws APIError{
		return send("POST");
	}
	
	public HTTPResponse post(String value) throws APIError{
		return send("POST", value);
	}
	
	public HTTPResponse patch() throws APIError{
		return send("PATCH");
	}
	
	public HTTPResponse patch(String value) throws APIError{
		return send("PATCH", value);
	}
	
	public HTTPResponse put() throws APIError{
		return send("PATCH");
	}
	
	public HTTPResponse put(String value) throws APIError{
		return send("PATCH", value);
	}
	
	public HTTPResponse get() throws APIError{
		return send("GET");
	}
	
	public HTTPResponse delete() throws APIError{
		return send("DELETE");
	}
	
	public HTTPResponse delete(String value) throws APIError{
		return send("DELETE", value);
	}
	
	public void url(String value) throws APIError {
		String prefix = null;
		String h = null;
		String p = null;
		String u = null;
		
		if(value == null)
			throw new BadRequest("URL cannot be null");
		
		if(value.trim().isEmpty())
			throw new BadRequest("URL cannot be an empty string");
		
		
		if(!value.matches(REGEX_PREFIX))
			throw new BadRequest("URL needs the proper prefix!  (ex: http or https)");
		else
			prefix = AppUtils.regexGroup(value, REGEX_PREFIX);
		
		if(!value.matches(REGEX_HOSTNAME))
			throw new BadRequest("URL needs a hostname specified");
		else
			h = AppUtils.regexGroup(value, REGEX_HOSTNAME);
		
		if(value.matches(REGEX_PORT))
			p = AppUtils.regexGroup(value, REGEX_PORT);
		
		if(value.matches(REGEX_URI))
			u = AppUtils.regexGroup(value, REGEX_URI);
		
		debug("prefix: %s, host: %s, port: %s, uri: %s",prefix, h, p, u);
		this.secure = prefix.equalsIgnoreCase("https");
		this.hostname = h;
		this.port = (p != null ? Integer.valueOf(p) : null);
		this.uri = u;
	}
	
	public String url(){
		StringBuilder sb = new StringBuilder();
		sb.append((secure ? "https://" : "http://"));
		sb.append(hostname);
		String uri = this.uri;
		
		
		if(port != null)
			sb.append(":").append(port);
		
		if(uri != null){
			List<String> keys = new ArrayList<String>(resources.keySet());
			Collections.reverse(keys);
			for(String key : keys){
				try {
					String value = String.valueOf(resources.get(key));
					key = URLEncoder.encode(key, "UTF-8");
					value = URLEncoder.encode(value, "UTF-8");
					
					uri = uri.replace(String.format(":%s", key), value );
					uri = uri.replaceAll(String.format("\\{\\s*%s\\s*\\}", key), value);
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			sb.append(uri);
		}
		
		List<String> keys = new ArrayList<String>(queries.keySet());

		Collections.reverse(keys);
		
		String delimiter = "?";
		for(String key : keys){
			
			try {
				String value = String.valueOf(queries.get(key));
				trace("query[%s: %s]", key, value);
				key = URLEncoder.encode(key, "UTF-8");
				value = URLEncoder.encode(value, "UTF-8");
				sb.append(delimiter);
				sb.append(key);
				sb.append("=");
				sb.append(value);
				delimiter = "&";
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			
		}
		
		return sb.toString();
	}
	
	public void header(String key, String value){
		addHeader(key, value);
	}
	
	public void header(String key, Number value){
		addHeader(key, value);
	}
	
	public void header(String key, Boolean value){
		addHeader(key, value);
	}
	
	private void addHeader(String key, Object value){
		if(AppUtils.isString(value) || AppUtils.isBoolean(value) || AppUtils.isNumeric(value))
			headers.put(key, value);
	}
	
	public void query(String key, String value){
		addQuery(key, value);
	}
	
	public void query(String key, Number value){
		addQuery(key, value);
	}
	
	public void query(String key, Boolean value){
		addQuery(key, value);
	}
	
	private void addQuery(String key, Object value){
		if(AppUtils.isString(value) || AppUtils.isBoolean(value) || AppUtils.isNumeric(value))
			queries.put(key, value);
	}
	
	public void resource(String key, String value){
		addResource(key, value);
	}
	
	public void resource(String key, Number value){
		addResource(key, value);
	}
	
	public void resource(String key, Boolean value){
		addResource(key, value);
	}
	
	private void addResource(String key, Object value){
		if(AppUtils.isString(value) || AppUtils.isBoolean(value) || AppUtils.isNumeric(value))
			resources.put(key, value);
	}
	
	public void parameter(String key, String value){
		addParameter(key, value);
	}
	
	public void parameter(String key, Number value){
		addParameter(key, value);
	}
	
	public void parameter(String key, Boolean value){
		addParameter(key, value);
	}
	
	private void addParameter(String key, Object value){
		if(AppUtils.isString(value) || AppUtils.isBoolean(value) || AppUtils.isNumeric(value))
			parameters.put(key, value);
	}
	
	protected String basicAuthHeader(){
		String[] parts = new String[]{ username, password, domain };
		String d = "";
		StringBuilder builder = new StringBuilder();
		for(String part : parts){
			if(part != null){
				builder.append(d).append(part);
				d = ":";
			}
		}
		String encoded = AppUtils.encodeBase64(builder.toString());
		return String.format("Basic %s", encoded);
	}
	public String payload(){
		return payload(null);
	}
	protected String payload(String payload){
		if(!parameters.isEmpty()){
			StringBuilder sb = new StringBuilder();
			String d = "";
			List<String> keys = new ArrayList<String>(parameters.keySet());
			Collections.reverse(keys);
			
			for(String key : keys){
				try{
					String value = URLEncoder.encode(String.valueOf(parameters.get(key)), "UTF-8");
					key = URLEncoder.encode(key, "UTF-8");
					sb.append(d);
					sb.append(key);
					sb.append("=");
					sb.append(value);
					d="&";
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			
			payload = sb.toString();
		}
		
		return payload;
	}
	
	private HTTPResponse send(String method) throws APIError{
		return send(method, null);
	}
	
	private HTTPResponse send(String method, String payload) throws APIError{
		try {
			
			boolean push = method.matches("(?i)(?:^)post|patch|put|delete(?:$)");
			URL url = new URL(url());
			
			//info("url: %s, push: %s, secure: %s", url, push, secure);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			if(secure){
				HttpsURLConnection https = (HttpsURLConnection) http;
				https.setHostnameVerifier(verifier);
			}
			
			switch(auth){
				case BASIC:
					header("Authorization", basicAuthHeader());
					break;
					
				case BEARER:
					header("Authorization", String.format("Bearer %s", bearer));
					break;
				default:
					break;	
			}
			
			List<String> keys = new ArrayList<String>(headers.keySet());
			Collections.reverse(keys);
			for(String key : keys){
				String value = String.valueOf(headers.get(key));
				http.setRequestProperty(key, value);
				trace("header[%s: %s]", key, value);
			}
			
			http.setRequestMethod(method);
			
			if(push){
				payload = payload(payload);
				if(payload != null){
					http.setDoOutput(true);
					OutputStream os = http.getOutputStream();
					os.write(payload.getBytes());
					os.flush();
				}
			}
			
			http.connect();
			
			HTTPResponse response = new HTTPResponse(http);
			
			http.disconnect();
			
			return response;
		} 
		
		catch (MalformedURLException e) {
			throw AppUtils.normalize(e, BadRequest.class);
		} 
		
		catch (IOException e) {
			throw AppUtils.normalize(e);
		} 
	}
	
}
