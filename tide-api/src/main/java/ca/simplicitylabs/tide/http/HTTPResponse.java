package ca.simplicitylabs.tide.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class HTTPResponse extends SelfLogger {
	private transient GsonBuilder gbuilder = new GsonBuilder().setPrettyPrinting();
	private transient Gson gson = gbuilder.create();
	private int code = 200;
	private String status;
	private String text;
	
	private Map<String, Object> headers = new HashMap<String, Object>();
	
	public HTTPResponse(){
		
	}
	
	public HTTPResponse(int code){
		code(code);
	}
	
	public HTTPResponse(int code, String text){
		this(code);
		text(text);
	}
	
	public HTTPResponse(int code, String text, String status){
		this(code, text);
		status(status);
	}
	
	public HTTPResponse(HttpURLConnection http) {
		try {
			code(http.getResponseCode());
			status(http.getResponseMessage());
			
			Map<String, List<String>> fields = http.getHeaderFields();
			Set<String> keys = fields.keySet();
			for(String key : keys){
				List<String> list = fields.get(key);
				for(String item : list){
					headers.put(key, item);
					break;
				}
			}
			
			String text = null;

			try{
				InputStream is = http.getErrorStream();
				
				if(is == null)
					is = http.getInputStream();
				
				text = IOUtils.toString(is, "UTF-8");
			}
			catch(Exception ex){
				warn(ex);
			}
			
			text(text);

		} catch (IOException e) {
			warn(e);
		}
	}

	public Gson gson(){
		return gson;
	}
	
	public boolean ok(){
		return (code >= 200 && code <= 299);
	}
	
	public int code(){
		return code;
	}
	
	public void code(int value){
		this.code = value;
	}
	
	public String status(){
		return status;
	}
	
	public void status(String value){
		this.status = value;
	}
	
	public String text(){
		return text;
	}
	
	public void text(String value){
		this.text = value;
	}
	
	public void text(JsonElement element){
		this.text = gson().toJson(element);
	}
	
	public void text(Node node){
		this.text = AppUtils.xml(node);
	}
	
	public JsonElement json() throws APIError{
		try{
			return gson().fromJson(text, JsonElement.class);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex, BadRequest.class);
		}
	}
	
	public Document xml() throws APIError {
		return AppUtils.xmlWithErrors(text);
	}
	
	public List<Node> xml(String path) throws APIError{
		return AppUtils.nodes(xml(), path);
	}
}
