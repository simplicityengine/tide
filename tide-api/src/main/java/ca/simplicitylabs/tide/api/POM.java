package ca.simplicitylabs.tide.api;

public interface POM {
	public String groupId();
	public String artifactId();
	public String version();
	public String domain();
}
