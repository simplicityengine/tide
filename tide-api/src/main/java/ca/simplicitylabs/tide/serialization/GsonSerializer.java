package ca.simplicitylabs.tide.serialization;

import static ca.simplicitylabs.tide.enumeration.SerialFormat.JSON;
import static ca.simplicitylabs.tide.enumeration.SerialType.SERIALIZE;
import static ca.simplicitylabs.tide.enumeration.SerialType.DESERIALIZE;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.enumeration.SerialType;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class GsonSerializer <T> extends SelfLogger implements JsonSerializer<T>, JsonDeserializer<T> {
	protected boolean allowNulls = false;
	protected Class<?> superClass;
	public GsonSerializer(Class<T> superClass) {
		this.superClass = superClass;
	}

	@Override
	public JsonElement serialize(T object, Type cls, JsonSerializationContext context) {
		try{
			JsonObject json = new JsonObject();
			
			List<Field> fields = new ArrayList<Field>();
			AppUtils.fields((Class<?>) cls, fields, Serial.class);
			AppUtils.ascend(fields);
			for(Field field : fields){
				try{
					String name = annotatedName(field);
					Object value = field.get(object);
					Class<?> type = field.getType();
					
					if(ok(field, value)){
						JsonElement element = serialize(context, value, type, field);
						json.add(name, element);
					}
					
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			serialized(object, json);
			return json;
		}
		catch(APIError e){
			error(e);
			return null;
		}
	}

	

	@Override
	public T deserialize(JsonElement element, Type cls, JsonDeserializationContext context) throws JsonParseException {
		try{
			@SuppressWarnings("unchecked")
			T object = AppUtils.instantiate((Class<T>) cls);
			
			JsonObject json = element.getAsJsonObject();
			List<Field> fields = new ArrayList<Field>();
			AppUtils.fields((Class<?>) cls, fields, Serial.class);
			for(Field field : fields){
				try{
					String name = annotatedName(field);
					Class<?> type = (Class<?>) field.getType();
					
					if(json.has(name)){
						JsonElement elem = json.get(name);
						Object value = deserialize(context, elem, type, field);
						
						if(ok(field, value, false)){
							if(elem.isJsonArray() && Iterable.class.isAssignableFrom(type)){
								ParameterizedType generic = (ParameterizedType) field.getGenericType();
								JsonArray array = elem.getAsJsonArray();
								Class<?> itemCls = (Class<?>) generic.getActualTypeArguments()[0];
								
								if(List.class.isAssignableFrom(type)){
									List list = new ArrayList();
									for(JsonElement child : array){
										Object item = deserialize(context, child, itemCls, field);
										deserialized(item, child);
										list.add(item);
									}
									field.set(object, list);
								}
							}
							else{
								deserialized(value, elem);
								field.set(object, value);
							}
						}
					}
						
					
				}
				catch(Exception ex){
					warn(ex);
				}
			}
			deserialized(object, element);
			return (T) object;
		}
		catch(Exception e){
			throw new JsonParseException(e);
		}
	}
	protected JsonElement serialize(JsonSerializationContext context, Object value, Class<?> type, Field field) throws Exception {
		if(Date.class.isAssignableFrom(type) && value != null){
			Date date = (Date) value;
			if(epoch(field, SERIALIZE)){
				return new JsonPrimitive(date.getTime());
			}
			else{				
				String format = format(field, SERIALIZE);
				TimeZone timezone = timezone(field, SERIALIZE);
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				if(timezone != null)
					sdf.setTimeZone(timezone);
				
				return new JsonPrimitive(sdf.format(date));
			}
			
		}
		return context.serialize(value, type);
	}
	
	protected Object deserialize(JsonDeserializationContext context, JsonElement elem, Class<?> type, Field field) {
		if(Date.class.isAssignableFrom(type) && elem.isJsonPrimitive()){
			JsonPrimitive prime = elem.getAsJsonPrimitive();
			if(prime.isString()){
				// TODO: check for numeric strings
				String value = prime.getAsString();
				String format = format(field, DESERIALIZE);
				TimeZone timezone = timezone(field, DESERIALIZE);
				try {
					SimpleDateFormat sdf = new SimpleDateFormat(format);
					if(timezone != null)
						sdf.setTimeZone(timezone);
					return sdf.parse(value);
				} catch (ParseException e) {
					trace(e);
				}
			}
			else if(prime.isNumber() && epoch(field, DESERIALIZE)){
				long value = prime.getAsLong();
				return new Date(value);
			}
		}
		return context.deserialize(elem, type);
	}

	private void serialized(Object object, JsonElement elem) throws APIError{
		if(object instanceof Serial){
			((Serial) object).serialized(JSON, elem);
		}
	}
	
	
	private void deserialized(Object object, JsonElement elem) throws APIError{
		if(object instanceof Serial){
			((Serial) object).deserialized(JSON, elem);
		}
	}
	private String annotatedName(Field field){
		String name = field.getName();
		if(field.isAnnotationPresent(SerializedName.class)){
			SerializedName serial = field.getAnnotation(SerializedName.class);
			name = serial.value();
		}
		
		return name;
	}
	
	protected boolean ok(Field field, Object value){
		return ok(field, value, true);
	}
	//TODO: add Ignore annotation
	protected boolean ok(Field field, Object value, boolean serialize){
		if(value == null){
			return allowNulls;
		}
		int mod = field.getModifiers();

		if(Modifier.isTransient(mod) || Modifier.isStatic(mod))
			return false;
		
		return true;
	}
	
	protected String format(Field field, SerialType type){
		return "yyyy-MM-dd HH:mm:ss";
	}
	
	public boolean epoch(Field field, SerialType type){
		return false;
	}
	
	protected TimeZone timezone(Field field, SerialType type){
		return null;
	}

}
