package ca.simplicitylabs.tide.serialization;

import ca.simplicitylabs.tide.enumeration.SerialFormat;
import ca.simplicitylabs.tide.exception.APIError;

public interface Serial {
	public <T> void serialized(SerialFormat type, T raw) throws APIError;
	public <T> void deserialized(SerialFormat type, T raw) throws APIError;
}
