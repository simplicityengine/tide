package ca.simplicitylabs.tide.configuration;
import java.util.List;
import java.util.jar.Manifest;

import com.google.gson.GsonBuilder;

import ca.simplicitylabs.tide.exception.APIError;

public interface ConfigStore {
	public String getPath();
	public void setPath(String path) throws APIError;
	public void register(Class<? extends Config> type) throws APIError;
	public void unregister(Class<? extends Config> type) throws APIError;
	public boolean has(Config config) throws APIError;
	public <T extends Config> List<T> list(Manifest manifest, Class<T> type) throws APIError;
	public <T extends Config> List<T> list(Class<T> type) throws APIError;
	public void add(Config config) throws APIError;
	public void drop(Config config) throws APIError;
	public void replace(Config config) throws APIError;
	public void modify(Config config) throws APIError;
	public List <Class<? extends Config>> types();
	public GsonBuilder builder();
}
