package ca.simplicitylabs.tide.configuration;

import java.lang.reflect.Type;
import java.util.UUID;
import java.util.jar.Manifest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.enumeration.SerialFormat;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.serialization.GsonSerializer;
import ca.simplicitylabs.tide.serialization.Serial;

public abstract class Config extends SelfLogger implements Comparable<Object>, Serial {
	private static GsonBuilder builder = new GsonBuilder()
			.setPrettyPrinting();
	protected UUID uuid;
	protected String type;
	protected String name;
	protected String groupId;
	protected String artifactId;
	protected String version;
	protected String domain;
	
	public Config() {
		type = getClass().getCanonicalName();
	}
	public void generateUUID(){
		if(uuid == null)
			uuid = UUID.randomUUID();
	}
	
	public boolean hasUUID(){
		return uuid != null;
	}
	
	public UUID uuid(){
		return uuid;
	}
	
	public void uuid(String uuid){
		this.uuid = UUID.fromString(uuid);
	}
	
	public void uuid(UUID uuid){
		this.uuid = uuid;
	}
	
	public String type(){
		return type;
	}
	
	public void type(String type){
		this.type = type;
	}
	
	public String name(){
		return name;
	}
	
	public String reference(){
		Class<?> cls = getClass();
		if(cls.isAnnotationPresent(Reference.class)){
			Reference configuration = cls.getAnnotation(Reference.class);
			return configuration.value();
		}
		return null;
	}

	public String groupId() {
		return groupId;
	}

	public String artifactId() {
		return artifactId;
	}

	public String version() {
		return version;
	}

	public String domain() {
		return domain;
	}
	
	/**
	 * @author Joseph Eniojukan
	 * This method is called whenever the configuration is added to the App
	 */
	public void added(){
		
	}
	
	/**
	 * @author Joseph Eniojukan
	 * This method is called whenever the configuration is removed from the App
	 */
	public void dropped(){
		
	}
	
	public boolean valid(){
		if(!ok(uuid))
			return false;
		
		if(!ok(type))
			return false;
		
		if(!ok(name))
			return false;
		
		if(!ok(groupId))
			return false;
		
		if(!ok(artifactId))
			return false;
		
		// TODO: add check for domain
		
		return true;
	}
	
	@Override
	public int compareTo(Object object){
		if(object instanceof UUID){
			UUID value = (UUID) object;
			if(hasUUID()){
				return uuid.compareTo(value);
			}
		}
		return -1;
	}
	
	public boolean equals(Config config){
		if(uuid == null)
			return false;
		
		if(config.uuid() == null)
			return false;
		
		return uuid.equals(config.uuid());
	}
	
	public boolean equals(Manifest manifest) {
		try {
			String groupId = manifest.getMainAttributes().getValue("groupId");
			String artifactId = manifest.getMainAttributes().getValue("artifactId");

			if (!this.groupId.equalsIgnoreCase(groupId))
				return false;

			if (!this.artifactId.equalsIgnoreCase(artifactId))
				return false;

			return true;
		} catch (Exception ex) {
			error(ex);
			return false;
		}
	}
	
	@Override
	public <T> void serialized(SerialFormat type, T raw){
		
	}
	
	@Override
	public <T> void deserialized(SerialFormat type, T raw){
		
	}
	
	@Override
	public String toString(){
		return gson().toJson(this);
	}
	public static void registerTypeAdapter(Type type, Object adapter){
		builder.registerTypeAdapter(type, adapter);
	}
	private static Gson gson(){
		return builder.create();
	}
	private static boolean ok(Object value){
		if(value == null)
			return false;
		
		if(value instanceof String){
			return !((String) value).trim().isEmpty();
		}
		
		return true;
	}

}
