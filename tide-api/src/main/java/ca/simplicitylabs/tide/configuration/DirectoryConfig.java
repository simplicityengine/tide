package ca.simplicitylabs.tide.configuration;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.NotFound;

@Reference("directories")
public class DirectoryConfig extends Config {
	private String path;
	private Integer maxFiles;
	private Integer maxFileSize;
	private Integer maxSize;
	
	public DirectoryConfig() {
		super();
	}
	
	public String path(){
		return path;
	}
	
	public Integer maxFiles(){
		return maxFiles;
	}
	
	public Integer maxFileSize(){
		return maxFileSize;
	}
	
	public Integer maxSize(){
		return maxSize;
	}
	
	private URI uri(String filePath) throws APIError{
		try{
			URI uri = new URI(String.format("%s/%s", path, filePath));
			uri = AppUtils.normalize(uri);
			return new URI(String.format("file://%s", uri));
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}

	}
	
	public void write(String filePath, String content) throws APIError{
		write(filePath, content.getBytes());
	}
	
	public void write(String filePath, byte[] content) throws APIError{
		try{
			URI uri = uri(filePath);
			debug("uri: %s", uri);
			Path path = Paths.get(uri);
			File file = path.toFile();
			file.createNewFile();
			file.setExecutable(false);
			file.setWritable(true);
			file.setReadable(true);
			
			Files.write(path, content, StandardOpenOption.WRITE);

		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public InputStream read(String filePath) throws APIError{
		try{
			URI uri = uri(filePath);
			debug("uri: %s", uri);
			Path path = Paths.get(uri);
			File file = path.toFile();
			
			if(!file.exists())
				throw new NotFound("%s not found on the filesystem!", filePath);
			
			return Files.newInputStream(path, StandardOpenOption.READ);

		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public boolean delete(String filePath) throws APIError {
		try{
			URI uri = uri(filePath);
			Path path = Paths.get(uri);
			File file = path.toFile();
			return file.delete();
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

}
