package ca.simplicitylabs.tide.collection;

public interface Item {
	public void added();
	public void dropped();
	public void modified();
	public void retrieved();
}
