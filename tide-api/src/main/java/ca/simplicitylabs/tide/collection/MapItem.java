package ca.simplicitylabs.tide.collection;

import java.util.Map;

public interface MapItem<K, V> extends Item {
	public Map<K, V> getMap();
}
