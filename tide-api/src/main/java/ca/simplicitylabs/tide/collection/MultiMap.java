package ca.simplicitylabs.tide.collection;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class MultiMap <K, V> implements Map<K, V> {
	Map<K, List<V>> map = new HashMap<K, List<V>>();

	public MultiMap() {
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public boolean containsKey(Object key, Class<V> cls) {
		if (map.containsKey(key)) {
			List<V> values = map.get(key);
			for (V value : values) {
				if (cls.equals(value.getClass())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean containsKey(Object key, Enum<?> enumerable) {
		return get(key, enumerable) != null;
	}
	
	public boolean containsKey(Object key, Serializable serializable) {
		return get(key, serializable) != null;
	}

	@Override
	public boolean containsValue(Object value) {
		Iterator<java.util.Map.Entry<K, List<V>>> entries = map.entrySet().iterator();

		while (entries.hasNext()) {
			Entry<K, List<V>> entry = entries.next();
			List<V> list = entry.getValue();

			for (V item : list) {
				if (item.equals(value))
					return true;
			}
		}
		return false;
	}

	@Override
	public V get(Object key) {
		List<V> values = list(key);
		if (!values.isEmpty())
			return values.get(0);
		return null;
	}
	
	public V get(Object key, Class<V> cls) {
		List<V> values = list(key);
		for (V value : values) {
			if (cls.equals(value.getClass())) {
				return value;
			}
		}
		return null;
	}
	
	public V get(Object key, Enum<?> enumeration){
		List<V> values = list(key);
		for (V value : values) {
			if (enumeration.equals(value)) {
				return value;
			}
		}
		return null;
	}
	
	public V get(Object key, Serializable serializable){
		List<V> values = list(key);
		for (V value : values) {
			if (serializable.toString().equals(value.toString())) {
				return value;
			}
		}
		return null;	
	}
	
	public List<V> list(Object key) {
		if (map.containsKey(key)) {
			List<V> values = map.get(key);
			return values;
		}
		return new ArrayList<V>();
	}

	@Override
	public V put(K key, V value) {
		if (containsKey(key)) {
			List<V> values = map.get(key);
			boolean found = false;
			for(V reference : values){
				if(reference.equals(value))
					found = true;
			}
			if(!found)
				values.add(value);
		}
		
		else{
			List<V> values = new ArrayList<V>();
			values.add(value);
			map.put(key, values);
		}
		
		return value;
	}

	@Override
	public V remove(Object key) {
		if(containsKey(key)){
			V value = get(key);
			map.remove(key);
			return value;
		}
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		// TODO Auto-generated method stub

	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<K> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}
}
