package ca.simplicitylabs.tide.collection;
import java.util.List;

public interface ListItem<T> extends Item {
	public List<T> getList();
}
