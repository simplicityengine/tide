package ca.simplicitylabs.tide.encryption;

import ca.simplicitylabs.tide.exception.APIError;

public interface AESEncryption {

	public void setSecret(String value) throws APIError;

	public String encrypt(String value) throws APIError;

}
