package ca.simplicitylabs.tide.enumeration;

public enum APIAuth {
	NONE, BASIC, DIGEST, BEARER
}
