package ca.simplicitylabs.tide.enumeration;

public enum LogicType {
	AND, OR
}
