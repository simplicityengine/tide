package ca.simplicitylabs.tide.enumeration;

public enum LogLevel {
	TRACE("TRACE", 0), DEBUG("DEBUG", 1), INFO("INFO", 2), 
	WARN("WARN", 3), ERROR("ERROR", 4), OFF("OFF", 5);
	
	private String _key;
	private int _value;
	LogLevel(String key, int value){
		this._key = key;
		this._value = value;
	}
	
	public String key(){
		return _key;
	}
	public int value(){
		return _value;
	}
	
	public String toString(){
		return key();
	}
	
	public boolean loggable(LogLevel level){
		return level.value() >= value();
	}
}
