package ca.simplicitylabs.tide.enumeration;

public enum SortType {
	ASCENDING, DESCENDING
}
