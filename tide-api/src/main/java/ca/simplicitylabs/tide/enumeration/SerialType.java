package ca.simplicitylabs.tide.enumeration;

public enum SerialType {
	SERIALIZE, DESERIALIZE
}
