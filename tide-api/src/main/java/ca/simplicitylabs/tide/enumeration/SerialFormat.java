package ca.simplicitylabs.tide.enumeration;

public enum SerialFormat {
	SQL, JSON, XML
}
