package ca.simplicitylabs.tide.enumeration;

public enum LifeCycle {
	DEPLOY("TRACE", 0), START("DEBUG", 1), STOP("ERROR", 2), UNDEPLOY("OFF", 3);
	
	private String _key;
	private int _value;
	LifeCycle(String key, int value){
		this._key = key;
		this._value = value;
	}
	
	public String key(){
		return _key;
	}
	public int value(){
		return _value;
	}
	
	public String toString(){
		return key();
	}
}
