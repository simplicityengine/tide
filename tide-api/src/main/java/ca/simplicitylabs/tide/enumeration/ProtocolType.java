package ca.simplicitylabs.tide.enumeration;

public enum ProtocolType {
	HTTP, HTTPS, WS, WSS
}
