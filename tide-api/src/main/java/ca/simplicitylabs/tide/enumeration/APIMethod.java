package ca.simplicitylabs.tide.enumeration;

public enum APIMethod {
	POST, GET, PATCH, PUT, DELETE, OPTIONS, TRACE, HEAD
}
