package ca.simplicitylabs.tide.orchestration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.API;
import ca.simplicitylabs.tide.enumeration.APIMethod;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.exception.NotAllowed;
import ca.simplicitylabs.tide.historization.SelfLogger;
import io.undertow.io.Sender;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.MultiPartParserDefinition;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;

public abstract class APIFlow extends SelfLogger {
	private static GsonBuilder jsonBuilder = new GsonBuilder().setPrettyPrinting();
	protected transient HttpServerExchange exchange;
	protected transient HeaderMap responseHeaders;
	protected transient Map<String, String> resources = new HashMap<String, String>();
	private transient long startTime = 0;
	private transient long stopTime = 0;
	
	public APIFlow(){
		startTime();
	}

	
	public void close(){
		
	}
	
	protected Gson gson(){
		return jsonBuilder.create();
	}
	
	public Date startDate(){
		return new Date(startTime);
	}
	
	public void startTime(){
		startTime = new Date().getTime();
	}
	
	public Date stopDate(){
		return new Date(stopTime);
	}
	
	public void stopTime(){
		stopTime = new Date().getTime();
	}
	
	public Long executionMills(){
		return new Long(stopTime - startTime);
	}
	
	public Double executionSeconds(){
		return executionMills().doubleValue()/1000;
	}
	
	protected boolean timestamped(){
		Class<?> cls = getClass();
		if(cls.isAnnotationPresent(API.class)){
			API api = cls.getAnnotation(API.class);
			return api.timestamped();
		}
		return false;
	}
	public void handleError(APIError error) throws APIError {
		header("Access-Control-Allow-Origin", "*");
		exchange.setStatusCode(error.getCode());
		Sender sender = exchange.getResponseSender();
		error(error);
		if(sender != null)
			sender.send(error.getMessage());
		stopTime();
	}
	
	public void handleJsonError(APIError error) throws APIError{
		header("Access-Control-Allow-Origin", "*");
		header("Content-type", "application/json");
		exchange.setStatusCode(error.getCode());
		Sender sender = exchange.getResponseSender();
		error(error);
		if(sender != null){
			
			JsonObject object = new JsonObject();
			object.addProperty("code", error.getCode());
			object.addProperty("message", error.getMessage());
			if(error.getUUID() != null)
				object.addProperty("uuid", error.getUUID().toString());
			sender.send(gson().toJson(object));
		}
		stopTime();
	}
	public void handle(HttpServerExchange exchange, Map<String, String> resources) throws APIError{
		setResources(resources);
		setExchange(exchange);
	}
	private void setResources(Map<String, String> resources){
		this.resources = resources;
	}
	private void setExchange(HttpServerExchange exchange) throws APIError{
		try{
			this.exchange = exchange;
			this.responseHeaders = exchange.getResponseHeaders();
			APIMethod method = getMethod();
			switch(method){
				case POST: post(); return;
				case GET: get(); return;
				case PATCH: patch(); return;
				case PUT: put(); return;
				case DELETE: delete(); return;
				case OPTIONS: options(); return;
				case HEAD: head(); return;
				case TRACE: trace(); return;
			}
		}
		catch(APIError error){
			handleError(error);
		}
		catch(Exception ex){
			handleError(new Internal(ex));
		}
	}
	
	public void post() throws APIError {
		throw new NotAllowed("POST Not Implemented");
	}
	
	public void get() throws APIError {
		throw new NotAllowed("GET Not Implemented");
	}
	
	public void put() throws APIError {
		throw new NotAllowed("PUT Not Implemented");
	}
	
	public void patch() throws APIError {
		throw new NotAllowed("PATCH Not Implemented");
	}
	
	public void delete() throws APIError {
		throw new NotAllowed("DELETE Not Implemented");
	}
	
	public void options() throws APIError {
		//TODO: make CORS configurable
		header("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, DELETE, HEAD, TRACE, OPTIONS");
		header("Access-Control-Allow-Headers", "Content-Type, Authorization");
		header("Access-Control-Allow-Origin", "*");
		respond(200);
	}
	
	public void head() throws APIError {
		respond(200);
	}
	public void trace() throws APIError {
		respond(200);
	}
	
	public APIMethod getMethod(){
		String method = exchange.getRequestMethod().toString().toUpperCase();
		return APIMethod.valueOf(method);
	}
	
	public void setCode(int code){
		exchange.setStatusCode(code);
	}
		
	public void respond(int code){
		setCode(code);
		respond();
	}
	
	public void respond(int code, String message){
		setCode(code);
		respond(message);
	}
	
	public void respond(){
		respond(null);
	}
	
	public void respond(String data){
		stopTime();
		// TODO: make CORS configurable
		header("Access-Control-Allow-Origin", "*");
		if(timestamped()){
			header("execution-time-milliseconds", executionMills());
			header("execution-time-seconds", executionSeconds());
		}
		if(data != null)
			exchange.getResponseSender().send(data);
		
		exchange.getResponseSender().close();
	}
	
	public FormData form(String path) throws APIError{
		try{
			MultiPartParserDefinition multi = new MultiPartParserDefinition(Paths.get(path));
			FormDataParser parser = multi.create(exchange);
			FormData form = parser.parseBlocking();
			return form;
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}
	
	public void download(InputStream is, String contentType) throws APIError{
		header("Content-Type", contentType);
		download(is);
	}
	
	public void download(InputStream is) throws APIError{
		download(200, is);
	}
	
	public void download(int code, InputStream is, String contentType) throws APIError{
		header("Content-Type", contentType);
		download(code, is);
	}
	
	public void download(int code, InputStream is) throws APIError{
		try {
			header("Access-Control-Allow-Origin", "*");
			setCode(code);
			OutputStream os = exchange.getOutputStream();
			IOUtils.copy(is, os);
			os.close();			
		} 
		catch (IOException ex) {
			throw AppUtils.normalize(ex);
		}
	}
	
	public void header(String key, String value){
		if(responseHeaders.contains(key))
			responseHeaders.remove(key);
		
		responseHeaders.add(new HttpString(key), value);
	}
	public void header(String key, long value){
		if(responseHeaders.contains(key))
			responseHeaders.remove(key);
		
		responseHeaders.add(new HttpString(key), value);
		
	}
	public void header(String key, double value){
		if(responseHeaders.contains(key))
			responseHeaders.remove(key);
		
		responseHeaders.add(new HttpString(key), String.valueOf(value));
	}
	
	public String payload() throws APIError{
		try {
			return IOUtils.toString(exchange.getInputStream(), "UTF-8");
		} catch (IOException e) {
			fault(e);
		}
		return null;
	}
	
	public String query(String key){
		return query(key, String.class);
	}
	
	public String query(String key, String value){
		return query(key, String.class, value);
	}
	
	public <T> T query(Class <T> targetClass, Class<?> baseClass){
		try {
			T object = targetClass.newInstance();
			List<Field> fields = AppUtils.fields(targetClass, baseClass);
			Map<String, Deque<String>> query = exchange.getQueryParameters();
			
			for(Field field : fields){
				String key = field.getName();
				Class<?> type = field.getType();
				Deque<String> deque = query.get(key);
				
				
				if(deque != null){
					String value = deque.getFirst();
					
					if(AppUtils.isUUID(type) && AppUtils.isUUID(value)){
						UUID uuid = UUID.fromString(value);
						field.set(object, uuid);
					}
					
					else if(String.class.isAssignableFrom(type))
						field.set(object, value);
					
					else if(Number.class.isAssignableFrom(type) && AppUtils.isNumeric(value)){
						if(Integer.class.isAssignableFrom(type))
							field.set(object, Integer.valueOf(value));
						// TODO: Double implementation
					}
					
					else if(Boolean.class.isAssignableFrom(type) && AppUtils.isBoolean(value))
						field.set(object, Boolean.valueOf(value));
					
				}
			}
			
			return object;
		} catch (InstantiationException | IllegalAccessException e) {
			warn(e);
		}
		return null;
	}
	
	public Long queryLong(String key){
		return query(key, Long.class);
	}
	
	public Long queryLong(String key, long value){
		return query(key, Long.class, value);
	}
	
	public Integer queryInteger(String key){
		return query(key, Integer.class);
	}
	
	public Integer queryInteger(String key, int value){
		return query(key, Integer.class, value);
	}
	
	public Double queryDouble(String key){
		return query(key, Double.class);
	}
	
	public Double queryDouble(String key, double value){
		return query(key, Double.class, value);
	}
	
	public Boolean queryBoolean(String key){
		return query(key, Boolean.class);
	}
	
	public Boolean queryBoolean(String key, boolean value){
		return query(key, Boolean.class, value);
	}
	
	public <T> T query(String key, Class<T> cls){
		return query(key, cls, null);
	}
	
	public boolean hasQuery(String key){
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		return query.containsKey(key);
	}
	
	public boolean hasQuery(String key, Class<?> cls){
		Object value = query(key, cls);
		return value != null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T query(String key, Class<T> cls, T defaultValue){
		Map<String, Deque<String>> map = exchange.getQueryParameters();
		if(map.containsKey(key)){
			Deque<String> deque = map.get(key);
			String value = deque.getFirst();
			if(AppUtils.isString(cls))
				return (T) value;
			
			if(AppUtils.isBoolean(cls) && AppUtils.isBoolean(value)){
				return (T) Boolean.valueOf(value);
			}
			
			if(AppUtils.isLong(cls) && AppUtils.isLong(value)){
				return (T) Long.valueOf(value);
			}
			
			if(AppUtils.isInteger(cls) && AppUtils.isInteger(value)){
				return (T) Integer.valueOf(value);
			}
			
			if(AppUtils.isDouble(cls) && AppUtils.isNumeric(value)){
				return (T) Double.valueOf(value);
			}
		}
		return defaultValue;
	}
	
	public String resourceString(String key){
		return resource(key, String.class);
	}
	
	public Integer resourceInteger(String key){
		return resource(key, Integer.class);
	}
	
	public Double resourceDouble(String key){
		return resource(key, Double.class);
	}
	
	public Boolean resourceBoolean(String key){
		return resource(key, Boolean.class);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T resource(String key, Class<T> cls){

		if(resources.containsKey(key)){
			String value = resources.get(key);
			if(AppUtils.isString(cls))
				return (T) value;
			
			if(AppUtils.isBoolean(cls) && AppUtils.isBoolean(value)){
				return (T) Boolean.valueOf(value);
			}
			
			if(AppUtils.isLong(cls) && AppUtils.isLong(value)){
				return (T) Long.valueOf(value);
			}
			
			if(AppUtils.isInteger(cls) && AppUtils.isInteger(value)){
				return (T) Integer.valueOf(value);
			}
			
			if(AppUtils.isDouble(cls) && AppUtils.isNumeric(value)){
				return (T) Double.valueOf(value);
			}
		}
		return null;
	}
	
	public String url(){
		return exchange.getRequestURL();
	}
	
	public String uri(){
		return exchange.getRequestURI();
	}
	
	public InetSocketAddress address(){
		return exchange.getSourceAddress();
	}
	
	private void fault(Exception ex) throws APIError{
		error(ex);
		throw new Internal(ex);
	}
}
