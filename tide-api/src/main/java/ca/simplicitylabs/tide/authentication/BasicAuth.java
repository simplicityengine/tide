package ca.simplicitylabs.tide.authentication;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.UnAuthorized;
import io.undertow.server.HttpServerExchange;

public class BasicAuth extends APICredential {

	public BasicAuth() {
		super();
	}

	public BasicAuth(String username) {
		super(username);
	}

	public BasicAuth(String username, String password) throws APIError {
		super(username, password);
	}

	public BasicAuth(String username, String password, String domain) throws APIError {
		super(username, password, domain);
	}
	
	public BasicAuth(HttpServerExchange exchange) throws APIError{
		super(exchange);
	}

	@Override
	public void authenticate(HttpServerExchange exchange) throws APIError {
		if(!isAuth(exchange))
			throw new UnAuthorized("Basic Authentication Required!");
		
		String header = exchange.getRequestHeaders().get("Authorization", 0);
		String base = AppUtils.regexGroup(header, "^Basic\\s([\\w\\=]+)$", 0);
		if(base == null)
			base = header;
		
		String decoded = AppUtils.decodeBase64(base);
		String[] segments = decoded.split(":");
		
		
		if(segments.length < 2)
			throw new UnAuthorized("Username and Password where not encoded in Base64");
		
		setUsername(segments[0]);
		setPassword(segments[1]);
		
		if(segments.length > 2)
			setDomain(segments[2]);
	}

	@Override
	public boolean isAuth(HttpServerExchange exchange) {
		String header = exchange.getRequestHeaders().get("Authorization", 0);
		
		if(header != null){
			return header.matches("^Basic\\s(?:[\\w\\=]+)$");
		}
		return false;
	}

}
