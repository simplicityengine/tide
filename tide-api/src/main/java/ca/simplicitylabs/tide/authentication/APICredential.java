package ca.simplicitylabs.tide.authentication;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import io.undertow.server.HttpServerExchange;

public abstract class APICredential extends SelfLogger {
	private String username;
	private String password;
	private String domain;
	private String md5Password;
	
	public APICredential(){
		super();
	}
	
	public APICredential(String username){
		this();
		setUsername(username);
	}
	
	public APICredential(String username, String password) throws APIError{
		this(username);
		setPassword(password);
	}
	
	public APICredential(String username, String password, String domain) throws APIError{
		this(username, password);
		setDomain(domain);
	}
	
	public APICredential(HttpServerExchange exchange) throws APIError{
		this();
		authenticate(exchange);
	}
	
	public abstract void authenticate(HttpServerExchange exchange) throws APIError;
	public abstract boolean isAuth(HttpServerExchange exchange);

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) throws APIError {
		this.password = password;
		this.md5Password = AppUtils.encryptMD5(password);
	}

	public String getMd5Password() {
		return md5Password;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public String toString(){
		return String.format("credentials [username: %s, password: %s, domain: %s]", username, password.replaceAll(".", "*"), domain);
	}
}
