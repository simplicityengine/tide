package ca.simplicitylabs.tide.historization;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;

public class SelfLogger implements Logging {
	private static List<Class<? extends Logging>> types = new ArrayList<Class<? extends Logging>>();
	private transient List<Logging> logs = new ArrayList<Logging>();
	private transient Class<?> cls;
	public SelfLogger(){
		cls = getClass();
		load();
	}
	public SelfLogger(Class<?> cls){
		this.cls = cls;
		load();
	}
	
	private void load(){
		for(Class<? extends Logging> type : types){
			try {
				Constructor<? extends Logging> builder = type.getConstructor(Class.class);
				logs.add(builder.newInstance(cls));
			} catch (Exception e) {
			}
		}
	}
	
	public static void addType(Class<? extends Logging> type){
		if(!SelfLogger.class.isAssignableFrom(type)){
			types.add(type);
		}
	}
	
	public void print(String format, Object... args){
		System.out.println(String.format(format, args));
	}
	
	public void print(Object object){
		System.out.println(object);
	}
	
	public UUID randomLogUUID(){
		return UUID.randomUUID();
	}
	
	public UUID trace(Object object){
		UUID id = randomLogUUID();
		trace(id, object);
		return id;
	}
	
	public UUID trace(String template, Object ...args){
		UUID id = randomLogUUID();
		trace(id, template, args);
		return id;
	}
	
	public UUID debug(Object object){
		UUID id = randomLogUUID();
		debug(id, object);
		return id;
	}
	
	public UUID debug(String template, Object ...args){
		UUID id = randomLogUUID();
		debug(id, template, args);
		return id;
	}
	
	public UUID info(Object object){
		UUID id = randomLogUUID();
		info(id, object);
		return id;
	}
	
	public UUID info(String template, Object ...args){
		UUID id = randomLogUUID();
		info(id, template, args);
		return id;
	}
	
	public UUID warn(Object object){
		UUID id = randomLogUUID();
		warn(id, object);
		return id;
	}
	
	public UUID warn(String template, Object ...args){
		UUID id = randomLogUUID();
		warn(id, template, args);
		return id;
	}
	
	public UUID warn(Exception ex){
		UUID id = randomLogUUID();
		warn(id, ex);
		return id;
	}
	
	public UUID error(Object object){
		UUID id = randomLogUUID();
		error(id, object);
		return id;
	}
	
	public UUID error(String template, Object ...args){
		UUID id = randomLogUUID();
		error(id, template, args);
		return id;
	}
	
	public UUID error(Exception ex){
		UUID id = randomLogUUID();
		error(id, ex);
		return id;
	}
	
	public void alert(Exception ex) throws APIError{
		UUID id = randomLogUUID();
		APIError aex = AppUtils.normalize(ex);
		error(id, aex);
		throw aex;
	}
	
	public void alert(Exception ex, Class<? extends APIError> type) throws APIError{
		UUID id = randomLogUUID();
		APIError aex = AppUtils.normalize(ex, type);
		error(id, aex);
		throw aex;
	}
	
	@Override
	public void trace(UUID id, Object object){
		for(Logging log: logs)
			log.trace(id, object);
	}
	
	@Override
	public void trace(UUID id, String template, Object ...args){
		for(Logging log: logs)
			log.trace(id, template, args);
	}
	
	@Override
	public void debug(UUID id, Object object){
		for(Logging log: logs)
			log.debug(id, object);
	}
	
	@Override
	public void debug(UUID id, String template, Object ...args){
		for(Logging log: logs)
			log.debug(id, template, args);
	}
	
	@Override
	public void info(UUID id, Object object){
		for(Logging log: logs)
			log.info(id, object);
	}
	
	@Override
	public void info(UUID id, String template, Object ...args){
		for(Logging log: logs)
			log.info(id, template, args);
	}
	
	@Override
	public void warn(UUID id, Object object){
		for(Logging log: logs)
			log.warn(id, object);
	}
	
	@Override
	public void warn(UUID id, String template, Object ...args){
		for(Logging log: logs)
			log.warn(id, template, args);
	}
	
	@Override
	public void warn(UUID id, Exception ex){
		for(Logging log: logs)
			log.warn(id, ex);
	}
	
	@Override
	public void error(UUID id, Object object){
		for(Logging log: logs)
			log.error(id, object);
	}
	
	@Override
	public void error(UUID id, String template, Object ...args){
		for(Logging log: logs)
			log.error(id, template, args);
	}
	
	@Override
	public void error(UUID id, Exception ex){
		APIError aex = AppUtils.normalize(ex);
		aex.setUUID(id);
		for(Logging log: logs)
			log.error(id, aex);
	}
	
}
