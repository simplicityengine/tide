package ca.simplicitylabs.tide.historization;

import java.util.UUID;

public interface Logging {	
	public void trace(UUID id, Object object);
	public void trace(UUID id, String template, Object... args);
	public void debug(UUID id, Object object);
	public void debug(UUID id, String template, Object... args);
	public void info(UUID id, Object object);
	public void info(UUID id, String template, Object... args);
	public void warn(UUID id, Object object);
	public void warn(UUID id, String template, Object... args);
	public void warn(UUID id, Exception ex);
	public void error(UUID id, Object object);
	public void error(UUID id, String template, Object... args);
	public void error(UUID id, Exception ex);
}
