package ca.simplicitylabs.tide.historization;
import static ca.simplicitylabs.tide.enumeration.LogLevel.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.enumeration.LogLevel;
import ca.simplicitylabs.tide.exception.APIError;

public class FileLogger implements Logging {
	private transient Logger log;
	private transient LogLevel level = TRACE;
	
	public FileLogger(){
		log = LogManager.getLogger(getClass().getName());
	}
	public FileLogger(Class<?> cls){
		log = LogManager.getLogger(cls.getName());
	}
	
	@Override
	public void trace(UUID id, Object object){
		if(level.loggable(TRACE))
			log.trace(write(TRACE, toString(object)));
	}
	
	@Override
	public void trace(UUID id, String template, Object ...args){
		if(level.loggable(TRACE))
			log.trace(write(TRACE, template, args));
	}
	
	@Override
	public void debug(UUID id, Object object){
		if(level.loggable(DEBUG))
			log.debug(write(DEBUG, toString(object)));
	}
	
	@Override
	public void debug(UUID id, String template, Object ...args){
		if(level.loggable(DEBUG))
			log.debug(write(DEBUG, template, args));
	}
	
	@Override
	public void info(UUID id, Object object){
		if(level.loggable(INFO))
			log.info(write(INFO, toString(object)));
	}
	
	@Override
	public void info(UUID id, String template, Object ...args){
		if(level.loggable(INFO))
			log.info(write(INFO, template, args));
	}
	
	@Override
	public void warn(UUID id, Object object){
		if(level.loggable(WARN))
			log.warn(write(WARN, toString(object)));
	}
	
	@Override
	public void warn(UUID id, String template, Object ...args){
		if(level.loggable(WARN))
			log.warn(write(WARN, template, args));
	}
	
	@Override
	public void warn(UUID id, Exception ex){
		if(level.loggable(WARN))
			log.warn(write(WARN, ex.toString()));
	}
	
	@Override
	public void error(UUID id, Object object){
		if(level.loggable(ERROR))
			log.error(write(ERROR, toString(object)));
	}
	
	@Override
	public void error(UUID id, String template, Object ...args){
		if(level.loggable(ERROR))
			log.error(write(ERROR, template, args));
	}
	
	@Override
	public void error(UUID id, Exception ex){
		if(level.loggable(ERROR)){
			if(ex instanceof APIError){
				APIError aex = (APIError) ex;
				log.error(write(ERROR, "[%s] %s\n%s", aex.getUUID(), aex.getMessage(), toString(aex)));
			}
			else
				log.error(write(ERROR, "\n%s", toString(ex)));
		}
	}
	
	private String write(LogLevel level, String template, Object ...args){
		StackTraceElement element = AppUtils.stack("ca.simplicitylabs.tide.historization");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd hh:mm:ss.SSS");
		//template = template.replaceAll("\\%(?![\\w\\-])", "%%");
		template = template.replaceAll("(?<=LIKE\\s\\')(?<=[^\\%])\\%(?=[^\\%])|(?<=[^\\%])\\%(?![\\w\\-\\%])", "%%");
		template = String.format(template, args);
		String method = element.getMethodName();
		String cls = classname(element.getClassName());
		int line = element.getLineNumber();
		String date = sdf.format(new Date());
		
		Thread thread = AppUtils.activeThread();
		Long threadId = thread.getId();
		String threadName = AppUtils.activeThread().getName();
		
		template = String.format("[%-5s %s] [%s.%s:%s] [%s] %s", level, date, cls, method, line, threadName, template);
		return template;
	}
	
	private String classname(String name){
		String[] names = name.split("\\.");
		
		if(names != null){
			if(names.length > 0)
				return names[names.length - 1];
		}
		
		return name;
	}
	
	private String toString(Exception ex){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		return sw.toString();
	}
	
	private String toString(Object obj){
		if(obj == null)
			return "";
		
		return obj.toString();
	}
	
}
