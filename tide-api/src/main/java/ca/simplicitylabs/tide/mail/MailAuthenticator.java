package ca.simplicitylabs.tide.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MailAuthenticator extends Authenticator {
	private String username;
	private String password;

	public MailAuthenticator() {
		super();
	}

	public MailAuthenticator(String username, String password) {
		username(username);
		password(password);
	}
	
	public String username(){
		return username;
	}
	
	public void username(String username){
		this.username = username;
	}
	
	public String password(){
		return password;
	}
	
	public void password(String password){
		this.password = password;
	}
	
	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}
