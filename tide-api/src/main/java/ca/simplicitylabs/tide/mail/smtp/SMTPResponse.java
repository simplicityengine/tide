package ca.simplicitylabs.tide.mail.smtp;

import java.util.ArrayList;
import java.util.List;

import javax.mail.Address;
import javax.mail.SendFailedException;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.exception.BadRequest;
import ca.simplicitylabs.tide.exception.Internal;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class SMTPResponse extends SelfLogger {
	private boolean success = true;
	private Exception exception;
	private Address[] addresses;
	public SMTPResponse() {
		super();
	}
	
	public SMTPResponse(Exception ex) {
		this();
		fail(ex);
	}
	
	public boolean successful(){
		return success;
	}
	
	public void succeed(){
		this.success = true;
	}
	
	public void fail(){
		this.success = false;
	}
	
	public void fail(Exception ex){
		if(ex instanceof SendFailedException){
			SendFailedException sfe = (SendFailedException) ex;
			addresses = sfe.getInvalidAddresses();
		}
		this.success = false;
		this.exception = ex;
	}
	
	public Exception exception(){
		return exception;
	}
	
	public List<String> undelivered(){
		List<String> list = new ArrayList<String>();
		if(addresses != null){
			for(Address address : addresses)
				list.add(address.toString());
		}
		return list;
	}
	
	public void validate() throws APIError{
		if(!successful()){
			if(exception == null)
				throw new Internal();
			
			if(exception instanceof SendFailedException){
				throw AppUtils.normalize(exception, BadRequest.class);
			}
			
			throw AppUtils.normalize(exception);
		}
	}
}
