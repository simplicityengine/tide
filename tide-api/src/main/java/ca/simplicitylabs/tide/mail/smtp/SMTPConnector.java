package ca.simplicitylabs.tide.mail.smtp;

import static javax.mail.Message.RecipientType.BCC;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ca.simplicitylabs.tide.AppUtils;
import ca.simplicitylabs.tide.annotation.Reference;
import ca.simplicitylabs.tide.exception.APIError;
import ca.simplicitylabs.tide.historization.SelfLogger;
import ca.simplicitylabs.tide.mail.Mail;
import ca.simplicitylabs.tide.mail.MailAuthenticator;

public class SMTPConnector extends SelfLogger {
	@Reference("mail.smtp.host")
	private String host;
	
	@Reference("mail.smtp.port")
	private Integer port;	
	
	@Reference("mail.smtp.user")
	private String user;
	
	private String password;
	
	@Reference("mail.smtp.auth")
	private Boolean auth;
	
	@Reference("mail.smtp.starttls.enable")
	private Boolean startTLS;
	
	@Reference("mail.smtp.starttls.required")
	private Boolean requireTLS;
	
	@Reference("mail.smtp.connectiontimeout")
	private Integer connectionTimeout;
	
	@Reference("mail.smtp.timeout")
	private Integer timeout;
	
	@Reference("mail.smtp.from")
	private String from;
	
	private Boolean debug = false;
	private Session session = Session.getDefaultInstance(properties());
	
	public SMTPConnector(){
		super();
	}
	

	public String host() {
		return host;
	}


	public void host(String hostname) {
		if(this.host != null){
			if(this.host.equalsIgnoreCase(hostname))
				return;
		}
		this.host = hostname;
	}


	public int port() {
		return port;
	}

	public void port(Integer port) {
		if(this.port != null){
			if(this.port == port)
				return;
		}
		this.port = port;
	}
	
	public void port(String port) throws APIError{
		try{
			if(this.port != null){
				if(this.port == Integer.valueOf(port))
					return;
			}
			this.port = Integer.valueOf(port);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public String user() {
		return user;
	}

	public void user(String user) {
		if(this.user != null){
			if(this.user.equalsIgnoreCase(user))
				return;
		}
		this.user = user;
	}

	public String password() {
		return password;
	}

	public void password(String password) {
		if(this.password != null){
			if(this.password.equalsIgnoreCase(password))
				return;
		}
		this.password = password;
	}

	public boolean auth() {
		return auth;
	}

	public void auth(Boolean authentication) {
		if(this.auth != null){
			if(this.auth == authentication)
				return;
		}
		this.auth = authentication;
	}
	
	public void auth(String authentication) throws APIError{
		try{
			if(this.auth != null){
				if( this.auth == Boolean.valueOf(authentication) )
					return;
			}
			this.auth = Boolean.valueOf(authentication);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public boolean startTLS() {
		return startTLS;
	}

	public void startTLS(Boolean tls) {
		if(this.startTLS != null){
			if(this.startTLS == tls)
				return;
		}
		this.startTLS = tls;
	}
	
	public void startTLS(String tls) throws APIError{
		try{
			if(this.startTLS != null){
				if( this.startTLS == Boolean.valueOf(tls) )
					return;
			}
			this.startTLS = Boolean.valueOf(tls);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public boolean requireTLS() {
		return requireTLS;
	}

	public void requireTLS(Boolean tls) {
		if(this.requireTLS != null){
			if(this.requireTLS == tls)
				return;
		}
		this.requireTLS = tls;
	}
	
	public void requireTLS(String tls) throws APIError{
		try{
			if(this.requireTLS != null){
				if( this.requireTLS == Boolean.valueOf(tls) )
					return;
			}
			this.requireTLS = Boolean.valueOf(tls);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public Integer connectionTimeout() {
		return connectionTimeout;
	}

	public void connectionTimeout(Integer timeout) {
		if(this.connectionTimeout != null){
			if(this.connectionTimeout == timeout)
				return;
		}
		this.connectionTimeout = timeout;
	}
	
	public void connectionTimeout(String timeout) throws APIError{
		try{
			if(this.connectionTimeout != null){
				if(this.connectionTimeout == Integer.valueOf(timeout))
					return;
			}
			this.connectionTimeout = Integer.valueOf(timeout);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public Integer timeout() {
		return timeout;
	}

	public void timeout(Integer timeout) {
		if(this.timeout != null){
			if(this.timeout == timeout)
				return;
		}
		this.timeout = timeout;
	}
	
	public void timeout(String timeout) throws APIError{
		try{
			if(this.timeout != null){
				if(this.timeout == Integer.valueOf(timeout))
					return;
			}
			this.timeout = Integer.valueOf(timeout);
		}
		catch(Exception ex){
			throw AppUtils.normalize(ex);
		}
	}

	public String from() {
		return from;
	}

	public void from(String address) {
		if(this.from != null){
			if(this.from.equalsIgnoreCase(address))
				return;
		}
		this.from = address;
	}
	
	public SMTPResponse send(Mail mail){
		SMTPResponse response = new SMTPResponse();
		try {
			connect();
			MimeMessage message = new MimeMessage(session);
			List<String> tos = mail.to();
			List<String> ccs = mail.cc();
			List<String> bccs = mail.bcc();
			
			String from = mail.from();
			
			if(from == null)
				from = from();
			
			message.setFrom(new InternetAddress(from));
			
			for(String to : tos)
				message.setRecipient(TO, new InternetAddress(to));
			
			for(String cc : ccs)
				message.setRecipient(CC, new InternetAddress(cc));
			
			for(String bcc : bccs)
				message.setRecipient(BCC, new InternetAddress(bcc));
			
			message.setSubject(mail.subject());
		
			if(mail.html())
				message.setText(mail.body(), "utf-8", "html");
			else
				message.setText(mail.body());
			
			Transport.send(message);
			response.succeed();
		} 
		catch (Exception e) {
			response.fail(e);
		}
		
		return response;
	}
	
	public Boolean debug(){
		return debug;
	}
	
	public void debug(Boolean debug){
		this.debug = debug;
	}
	
	public void connect(){
		session = null;
		session = Session.getInstance(properties(), new MailAuthenticator(user, password));
		session.setDebug(debug);
	}

	
	public Properties properties(){
		Properties properties = new Properties();
		List<Field> fields = AppUtils.fields(getClass());
		
		for(Field field : fields){
			try{
				if(!field.isAnnotationPresent(Reference.class))
					continue;
				
				Object value = field.get(this);
				Reference reference = field.getAnnotation(Reference.class);
				String key = reference.value();
				
				if(value != null)
					properties.put(key, value);
			}
			catch(Exception ex){
				
			}
		}
		
		return properties;
	}

}
