package ca.simplicitylabs.tide.mail;

import static javax.mail.Message.RecipientType.BCC;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.mail.Message.RecipientType;

import ca.simplicitylabs.tide.collection.MultiMap;
import ca.simplicitylabs.tide.historization.SelfLogger;

public class Mail extends SelfLogger {
	private String from;
	private String subject;
	private String body;
	private boolean html = false;
	private MultiMap<String, RecipientType> addresses = new MultiMap<String, RecipientType>();
	
	public Mail() {
		// TODO Auto-generated constructor stub
	}

	public Mail(Class<?> cls) {
		super(cls);
	}
	
	public String from() {
		return from;
	}

	public void from(String address) {
		this.from = address;
	}
	
	public String subject(){
		return subject;
	}
	
	public void subject(String subject){
		this.subject = subject;
	}

	public String body() {
		return body;
	}

	public void body(String content) {
		this.body = content;
	}

	public boolean html() {
		return html;
	}

	public void html(boolean html) {
		this.html = html;
	}
	
	public void html(String content){
		html(true);
		body(content);
	}
	
	public void text(String content){
		html(false);
		body(content);
	}

	public List<String> to(){
		return getAddress(TO);
	}
	
	public void to(String address){
		addAddress(address, TO);
	}
	
	public List<String> cc(){
		return getAddress(CC);
	}
	
	public void cc(String address){
		addAddress(address, CC);
	}
	
	public List<String> bcc(){
		return getAddress(BCC);
	}
	
	public void bcc(String address){
		addAddress(address, BCC);
	}
	
	private void addAddress(String address, RecipientType type){
		if(address == null)
			return;
		
		if(address.trim().isEmpty())
			return;
		
		address = address.toLowerCase();

		if(!addresses.containsKey(address, type)){
			addresses.put(address, type);
		}
	}
	
	public List<String> getAddress(RecipientType type){
		Set<String> keys = addresses.keySet();
		List<String> list = new ArrayList<String>();
		
		for(String key : keys){
			if(addresses.containsKey(key, type))
				list.add(key);
		}
		
		return list;
	}

}
